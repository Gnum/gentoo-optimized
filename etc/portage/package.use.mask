# Copyright 1999-2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# Rick Farina <zerochaos@gentoo.org> (2017-03-04)
# requires libusb[static-libs] which requires libudev[static-libs], all masked
net-wireless/ubertooth static-libs

# Rick Farina <zerochaos@gentoo.org> (2016-02-03)
# dev-libs/libusb[static-libs] requires libudev[static-libs] which is masked below
dev-libs/libusb static-libs

# 'static-libs' support on sys-apps/systemd is not provided
virtual/libgudev static-libs
virtual/libudev static-libs
sys-fs/cryptsetup static static-libs
sys-fs/lvm2 static static-libs
sys-fs/dmraid static
sys-fs/zfs static-libs
dev-libs/libgudev static-libs
# These are actually possible, but only if you USE=-udev when building pciutils.
# But package.use.mask doesn't have a way to express that, so this profile loses.
sys-apps/flashrom static
sys-apps/pciutils static-libs
# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# Alexey Shvetsov <alexxy@gentoo.org> (2019-12-24)
# Gromacs python module install br0ken
sci-chemistry/gromacs python

# Peter Levine <plevine457@gmail.com> (2019-10-01)
# Native inotify support is preferred on linux.
# https://bugs.gentoo.org/697476
net-fs/samba fam

# Patrick McLean <chutzpah@gentoo.org> (2019-09-17)
# Masked due to upstream build failures (bug #693018)
# One example: https://tracker.ceph.com/issues/41523
>=sys-cluster/ceph-14.2 dpdk

# Jason Zaman <perfinion@gentoo.org> (2015-09-06)
# This is masked in base/package.use.mask as Linux only.
sys-auth/consolekit -cgroups

# Michael Palimaka <kensington@gentoo.org> (2015-09-03)
# Native inotify support is preferred on linux.
kde-frameworks/kcoreaddons fam

# Diego Elio Pettenò (2012-08-27)
# The libpci access is only used for linux.
net-analyzer/net-snmp -pci

# Richard Yao <ryao@gentoo.org> (2012-08-22)
# USE=kernel-builtin is dangerous. Only those that know what they are doing
# should use it until documentation improves.
sys-fs/zfs kernel-builtin

# Diego Elio Pettenò (2012-08-20)
# The prevent-removal USE flag is only implemented for Linux.
sys-auth/pam_mktemp -prevent-removal

sys-devel/gcc hardened
sys-libs/glibc hardened

# Samuli Suominen <ssuominen@gentoo.org (2012-03-20)
# This is masked in base/package.use.mask as Linux only.
sys-auth/consolekit -acl

# Samuli Suominen <ssuominen@gentoo.org> (2012-01-10)
# Masked in base/package.use.mask as Linux -only feature
app-arch/libarchive -e2fsprogs

# Diego E. Pettenò <flameeyes@gentoo.org> (2009-08-20)
#
# Mask oss USE flag for PulseAudio; it's present for compatibility
# with FreeBSD and other operating systems that have no better
# interfaces, but people would probably abuse it with Linux as well.
#
# Older versions also had an OSS compatibility wrapper on that USE so
# they are excluded.
>=media-sound/pulseaudio-0.9.16_rc5 oss

# Diego E. Pettenò <flameeyes@gentoo.org> (2011-03-27)
#
# Unmask pdnsd's Linux-specific USE flags.
net-dns/pdnsd -isdn -urandom
# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

# Mike Frysinger <vapier@gentoo.org> (2016-05-08)
# This target supports VTV #547040.
>=sys-devel/gcc-4.9 -vtv

# Mike Frysinger <vapier@gentoo.org> (2014-10-21)
# This target supports ASAN/etc... #504200.
sys-devel/gcc -sanitize
# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# When you add an entry to the top of this file, add your name, the date, and
# an explanation of why something is getting masked. Please be extremely
# careful not to commit atoms that are not valid, as it can cause large-scale
# breakage, especially if it ends up in the daily snapshot.
#
## Example:
##
## # Dev E. Loper <developer@gentoo.org> (2012-06-28)
## # Masking foo USE flag until we can get the
## # foo stuff to work properly again (bug 12345)
## =media-video/mplayer-0.90_pre5 foo
## =media-video/mplayer-0.90_pre5-r1 foo
#

#--- END OF EXAMPLES ---

# Thomas Deutschmann <whissi@gentoo.org> (2020-04-30)
# sys-cluster/slurm is keyworded on amd64
app-metrics/collectd -collectd_plugins_slurm

# Georgy Yakovlev <gyakovlev@gentoo.org (2020-04-26)
# static-pie works on amd64, #719444
sys-libs/glibc -static-pie

# Guilherme Amadio <amadio@gentoo.org> (2020-03-16)
# media-libs/cudnn is keyworded on amd64
sci-physics/root -cudnn

# Thomas Deutschmann <whissi@gentoo.org> (2020-03-11)
# Encrypted Media Extensions (eme-free) can be disabled on amd64
www-client/firefox -eme-free

# Mart Raudsepp <leio@gentoo.org> (2020-03-01)
# dev-util/sysprof-capture is keyworded on amd64
x11-wm/mutter -sysprof
dev-libs/gjs -sysprof

# Andreas Sturmlechner <asturm@gentoo.org> (2020-02-26)
# Vulkan is available on amd64.
dev-qt/qtdeclarative -vulkan
dev-qt/qtgui -vulkan
dev-qt/qtwayland -vulkan

# James Le Cuirot <chewi@gentoo.org> (2019-12-10)
# The JIT feature only works on amd64 and x86.
app-emulation/aranym -jit

# Thomas Deutschmann <whissi@gentoo.org> (2019-11-11)
# dev-python/pandas is keyworded for amd64
sys-block/fio -python -gnuplot

# Craig Andrews <candrews@gentoo.org> (2019-10-08)
# net-misc/quiche is available on this arch
net-misc/curl -quiche

# Luke Dashjr <luke-jr+gentoobugs@utopios.org> (2019-09-21)
# iasl is stable on amd64
sys-firmware/seabios -debug

# Georgy Yakovlev <gyakovlev@gentoo.org> (2019-08-28)
# upstream provides docs and tools for tier-1 arches
dev-lang/rust-bin -doc
>=dev-lang/rust-bin-1.37 -clippy -rustfmt

# Thomas Deutschmann <whissi@gentoo.org> (2019-08-03)
# Early microcode loading is supported on amd64
sys-kernel/linux-firmware -initramfs

# Lars Wendler <polynomial-c@gentoo.org> (2019-07-26)
# dev-libs/gumbo is keyworded on amd64
mail-client/claws-mail -litehtml

# Georgy Yakovlev <gyakovlev@gentoo.org> (2019-06-22)
# openjfx is keyworded on amd64
dev-java/openjdk:11 -javafx

# Matt Turner <mattst88@gentoo.org> (2019-06-20)
# dev-lang/spidermonkey:60[jit] fails to build on most platforms, but does
# build on amd64.
>=dev-lang/spidermonkey-60 -jit

# Andreas Sturmlechner <asturm@gentoo.org> (2019-05-09)
# media-libs/libplacebo is keyworded on amd64
media-video/vlc -libplacebo

# Denis Lisov <dennis.lissov@gmail.com> (2019-05-09)
# Overlay can only be built on amd64 and x86
x11-apps/igt-gpu-tools -overlay

# Georgy Yakovlev <gyakovlev@gentoo.org> (2019-03-05)
# freeipmi is supported on amd64
app-admin/conserver -freeipmi

# Thomas Deutschmann <whissi@gentoo.org> (2018-11-15)
# - rdrand plugin is supported on amd64
net-vpn/strongswan -strongswan_plugins_rdrand

# James Le Cuirot <chewi@gentoo.org> (2018-11-02)
# Vulkan is only available on amd64 at present.
media-libs/libsdl2 -vulkan

# Michael Palimaka <kensington@gentoo.org> (2018-10-12)
# Unmask arch-specific USE flags available on amd64
net-analyzer/testssl -bundled-openssl -kerberos

# Thomas Deutschmann <whissi@gentoo.org> (2018-09-30)
# Unmask libheif support where media-libs/libheif is keyworded
media-gfx/imagemagick -heif

# Thomas Deutschmann <whissi@gentoo.org> (2018-08-23)
# Dependency sys-cluster/ceph is keyworded for amd64
net-fs/samba -ceph

# Rick Farina <zerochaos@gentoo.org> (2018-06-27)
# Catalyst has support for assembling bootloader on this arch
dev-util/catalyst -system-bootloader

# Ilya Tumaykin <itumaykin+gentoo@gmail.com> (2018-06-17)
# Vulkan support is only available on few selected arches atm.
# Mask everywhere, unmask where appropriate.
media-video/mpv -vulkan

# Nick Sarnie <sarnex@gentoo.org> (2018-04-29)
# media-libs/vulkan-loader and app-emulation/vkd3d are keyworded on amd64
app-emulation/wine-staging -vkd3d -vulkan
app-emulation/wine-vanilla -vkd3d -vulkan

# Richard Yao <ryao@gentoo.org> (2018-04-16)
# sys-fs/zfs is keyworded on amd64
sys-cluster/ceph -zfs
sys-boot/grub -libzfs

# Amy Liffey <amynka@gentoo.org> (2017-09-19)
# [cuda] is unmasked in this profiles.
media-libs/opencv -contribxfeatures2d

# David Seifert <soap@gentoo.org> (2017-05-20)
# cpyrit-cuda does not support GCC 4.9 or later
net-wireless/pyrit cuda

# Thomas Deutschmann <whissi@gentoo.org> (2017-03-01)
# dev-libs/libmaxminddb is keyworded on amd64
app-admin/rsyslog -mdblookup

# Alexis Ballier <aballier@gentoo.org> (2017-02-21)
# spacetime works on amd64
# https://caml.inria.fr/pub/docs/manual-ocaml/spacetime.html
dev-lang/ocaml -spacetime

# Alexis Ballier <aballier@gentoo.org> (2017-01-31)
# nvidia drivers are unmasked here
media-video/ffmpeg -nvenc

# Luke Dashjr <luke-jr+gentoobugs@utopios.org> (2017-01-04)
# Assembly optimisations are supported on amd64 for all versions
dev-libs/libsecp256k1 -asm

# Andreas Sturmlechner <asturm@gentoo.org> (2016-12-31)
# on behalf of Andreas K. Hüttel <dilfridge@gentoo.org> (2016-12-14)
# Fails to build with newly unmasked ffmpeg-3, so mask this useflag
# Workaround; leaving a real fix to the maintainers... bug 580630
media-libs/mlt vdpau

# Ettore Di Giacinto <mudler@gentoo.org> (2016-10-18)
# Enable gambit and scm only on supported architectures
>=dev-scheme/slib-3.2.5 -gambit -scm

# Thomas Deutschmann <whissi@gentoo.org> (2016-09-22)
# Enable MQTT support on supported architectures
app-metrics/collectd -collectd_plugins_mqtt

# Thomas Deutschmann <whissi@gentoo.org> (2016-08-26)
# Enable numa support on supported architectures
dev-db/mysql -numa
dev-db/percona-server -numa

# Brian Evans <grknight@gentoo.org> (2016-08-23)
# It's only supported on amd64
dev-db/percona-server -tokudb -tokudb-backup-plugin

# Brian Evans <grknight@gentoo.org> (2016-08-18)
# Allow jdbc on this arch
dev-db/mariadb -jdbc

# Andrew Savchenko <bircoph@gentoo.org> (2016-08-11)
# PGO is fixed on amd64
>=www-client/firefox-48 -pgo

# James Le Cuirot <chewi@gentoo.org> (2016-07-26)
# Only available on some architectures.
dev-java/icedtea -shenandoah

# Mike Gilbert <floppym@gentoo.org> (2016-06-23)
# This flag only has meaning on amd64
sys-boot/grub:2 -grub_platforms_xen-32

# Brian Evans <grknight@gentoo.org (2015-09-14)
# Unmask new USE flags for mariadb on supported arches
>=dev-db/mariadb-10.1.0 -mroonga -sst-xtrabackup -galera

# Mike Gilbert <floppym@gentoo.org> (2015-09-05)
# sys-boot/gnu-efi is not supported on all archs.
sys-apps/systemd -gnuefi

# James Le Cuirot <chewi@gentoo.org> (2015-07-28)
# JavaFX and the browser plugin are included on x64 Linux.
dev-java/oracle-jdk-bin -javafx -nsplugin
dev-java/oracle-jre-bin -javafx -nsplugin

# Ben de Groot <yngwin@gentoo.org> (2015-03-15)
# media-libs/libbdplus is keyworded on amd64, so unmask the useflag
media-libs/libbluray -bdplus

# Michał Górny <mgorny@gentoo.org> (2015-03-01)
# pidgin-opensteamworks is only available for amd64, ppc32 and x86
net-im/telepathy-connection-managers -steam

# Mike Frysinger <vapier@gentoo.org> (2014-08-04)
# Unmask flashrom drivers that only work on x86 due to in/out asm insns #454024
sys-apps/flashrom -atahpt -nic3com -nicnatsemi -nicrealtek -rayer-spi -satamv

# Jorge Manuel B. S. Vicetto <jmbsvicetto@gentoo.org> (2014-04-24)
# It's only supported on amd64
dev-db/mariadb -tokudb

# Samuli Suominen <ssuominen@gentoo.org> (2014-02-16)
# Still considered experimental by upstream:
# https://sourceforge.net/p/mikmod/bugs/16/#17ea
media-libs/libmikmod cpu_flags_x86_sse2

# Andreas K. Huettel <dilfridge@gentoo.org> (2014-01-04)
# While globally masked, the needed amd64 keywords are already present
dev-vcs/git -mediawiki -mediawiki-experimental

# Sergey Popov <pinkbyte@gentoo.org> (2013-12-27)
# Boost.Context can be built on amd64
dev-libs/boost	-context

# Michał Górny <mgorny@gentoo.org> (2013-12-15)
# mupen64plus' 2.0 new dynamic recompiler is supported on x86 and arm
# only.
games-emulation/mupen64plus-core new-dynarec

# Tim Harder <radhermit@gentoo.org> (2013-08-13)
# dev-lang/luajit keyworded for amd64 (masked in base)
# dev-scheme/racket keyworded for amd64 (masked in base)
app-editors/vim -luajit -racket
app-editors/gvim -luajit -racket

# Kacper Kowalik <xarthisius@gentoo.org> (2013-08-09)
# Works on amd64
sys-apps/hwloc -gl

# Michał Górny <mgorny@gentoo.org> (2013-07-22)
# Meaningless on amd64 (it controls the 32-bit x86 JIT).
dev-python/pypy-exe cpu_flags_x86_sse2
dev-python/pypy3-exe cpu_flags_x86_sse2

# Christoph Junghans <junghans@gentoo.org> (2012-12-26)
# cuda works on x86/amd64 (masked in base)
sci-chemistry/gromacs -mkl

# Diego Elio Pettenò <flameeyes@gentoo.org> (2012-11-03)
# Unmask here, as it's masked in base.
net-libs/gnutls -dane

# Ben de Groot <yngwin@gentoo.org> (2012-07-30)
# dependency keyworded here, masked in base
media-libs/freetype -infinality

# Alexandre Rostovtsev <tetromino@gentoo.org> (2012-04-12)
# Temporary mask-in-base, unmask-in-arch for dev-python/mako keywording for
# gobject-introspection[doctool], bug #411761
dev-libs/gobject-introspection -doctool

# Michał Górny <mgorny@gentoo.org> (2012-04-11)
# JIT compilation in zpaq generates code for x86/amd64.
app-arch/zpaq -jit

# Arun Raghavan <ford_prefect@gentoo.org> (2012-03-29)
# webrtc-audio-processing is only supported on x86/amd64. Possibly arm can be
# added.
media-sound/pulseaudio -webrtc-aec

# Bernard Cafarelli <voyageur@gentoo.org> (2012-02-27)
# Masked in base profile, supported on this arch
gnustep-base/gnustep-make -libobjc2

# Davide Pesavento <pesa@gentoo.org> (2011-11-30)
# The QML (V4) and JSC JITs are supported only on amd64/arm/x86,
# so the flag is masked in the base profile and unmasked here.
dev-qt/qtdeclarative -jit
dev-qt/qtscript -jit
dev-qt/qtwebkit -jit

# Robin H. Johnson <robbat2@gentoo.org> (2011-07-10), updated 2019-12-06
# PPS should work on all arches, but only keyworded on some arches
>=net-misc/ntp-4.2.6_p3-r1 -parse-clocks

# Tim Harder <radhermit@gentoo.org> (2011-02-13)
# Masked in base, unmask for amd64/x86
>=media-video/mplayer-1.0_rc4_p20101219 -bluray

# Chí-Thanh Christopher Nguyễn <chithanh@gentoo.org> (2010-11-28)
# sys-devel/llvm is keyworded on amd64
media-libs/mesa -llvm

# Tomáš Chvátal <scarabeus@gentoo.org> (2010-04-04)
# x86 platforms works
sys-power/pm-utils -video_cards_intel -video_cards_radeon

# Jean-Noël Rivasseau <elvanor@gentoo.org> (2009-09-23)
# X use flag only for amd64/x86, bug 285951.
media-gfx/iscan -X

# Bernard Cafarelli <voyageur@gentoo.org> (2009-03-20)
# Depends on packages not yet keyworded for amd64, cf bug #22042
app-backup/backup-manager s3

# Jeremy Olexa <darkside@gentoo.org> (2009-02-07)
# USE=mmx on imlib2 causes issues with other apps. See bug 218382 (comments
# 11-12)
media-libs/imlib2 cpu_flags_x86_mmx

# Jeremy Olexa <darkside@gentoo.org> (2008-12-27)
# Mask static USE flag because it just doesn't work on amd64. See bug 236591
>=app-arch/p7zip-4.58 static

# Jeremy Olexa <darkside@gentoo.org> (2008-12-24)
# unmask USE=bindist on amd64 - also unmasking the other flags that were under a
# bad comment heading. see bug #246144
media-video/mplayer -cpudetection

# assembler-section

# blubb@gentoo.org (2006-10-23)
# upstream forces sse2 for amd64; mmx flag does nothing
app-crypt/johntheripper cpu_flags_x86_mmx

# blubb@gentoo.org (2006-10-23)
# x86 asm only:
media-libs/libfame cpu_flags_x86_mmx
media-libs/sdl-gfx cpu_flags_x86_mmx
media-libs/smpeg cpu_flags_x86_mmx
x11-terms/eterm cpu_flags_x86_mmx

# x86_64 opts are enabled with USE sse. so masking the rest
media-sound/mpg123 cpu_flags_x86_mmx cpu_flags_x86_3dnow cpu_flags_x86_3dnowext

# Alistair Bush <ali_bush@gentoo.org> (2007-05-08)
# Mask doc flag for fop as nearly impossible to
# run javadoc target.  see #177585
>=dev-java/fop-0.93 doc

# Alexis Ballier <aballier@gentoo.org> (2008-08-08)
# x86 asm only, it won't be built on amd64 if mmx useflag is enabled but that
# saves people from needlessly installing nasm
# Refs bug #93279
media-sound/lame cpu_flags_x86_mmx
#sys-devel/gcc -jit
# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# New entries go on top.

# This file is only for generic masks. For arch-specific masks (i.e.
# mask everywhere, unmask on arch/*) use arch/base.

# Ben Kohler <bkohler@gentoo.org> (2020-04-29)
# Dependency pam_wrapper missing for tests, and RESTRICT=test is already set
net-fs/samba test

# Mike Gilbert <floppym@gentoo.org> (2020-04-23)
# Tests are very sensitive to the host environment.
dev-util/meson test

# Michał Górny <mgorny@gentoo.org> (2020-04-22)
# Requires dev-python/filemagic that is broken and masked for removal.
dev-python/jira magic

# Michał Górny <mgorny@gentoo.org> (2020-04-20)
# Requires dev-python/rst2pdf that is masked for removal.
media-video/mpv doc

# Michał Górny <mgorny@gentoo.org> (2020-04-19)
# Requires dev-vcs/ghp-import that is masked for removal.
www-apps/nikola ghpages

# Georgy Yakovlev <gyakovlev@gentoo.org> (2020-04-19)
# oracle-jdk-bin masked for removal, mask revdep useflag.
=app-forensics/sleuthkit-4.7.0 java

# Ulrich Müller <ulm@gentoo.org> (2020-04-08)
# Old versions of libjpeg-turbo have known security issues.
# Use the bundled lib on your own risk. Bug #715106.
net-im/zoom bundled-libjpeg-turbo

# Alfredo Tupone <tupone@gentoo.org> (2020-04-04)
# Ada support is not yet ready for sys-deve/gcc
sys-devel/gcc ada

# Joshua Kinard <kumba@gentoo.org> (2020-03-28)
# NCP support is masked pending removal of net-fs/ncpfs
# Bug #681820
net-analyzer/hydra ncp

# Michał Górny <mgorny@gentoo.org> (2020-03-28)
# Requires masked dev-vcs/bzr.  Remove the mask if breezy is packaged
# and the package is confirmed to work with it.
dev-vcs/cvs2svn bazaar
<=dev-util/rosinstall-0.7.8 test
<=dev-util/wstool-0.1.18 test
<=dev-python/vcstools-0.1.42 test

# Georgy Yakovlev <gyakovlev@gentoo.org> (2020-03-27)
# Depends on vulnerable old version of icedtea-web #711392
# new version is not packaged yet
# package/useflag is not going away anytime soon,
# just masked. unmask as needed.
dev-java/icedtea nsplugin webstart
dev-java/icedtea-bin nsplugin webstart
dev-java/openjdk nsplugin webstart
dev-java/openjdk-bin nsplugin webstart
dev-java/openjdk-jre-bin nsplugin webstart

# Michał Górny <mgorny@gentoo.org> (2020-03-27)
# www-client/weboob is masked for removal.
app-office/kmymoney weboob

# Michał Górny <mgorny@gentoo.org> (2020-03-27)
# Requires old version of dev-python/docutils.
dev-util/buildbot doc

# Lars Wendler <polynomial-c@gentoo.org> (2020-03-26)
# Requires deprecated python2 and pygtk
mail-client/claws-mail python

# Michał Górny <mgorny@gentoo.org> (2020-03-24)
# Require dev-python/recommonmark with py2.
<sys-devel/llvm-9 doc

# Georgy Yakovlev <gyakovlev@gentoo.org> (2020-03-18)
# USE=doc makes rust compile 6 times!
# Reason unknown yet, masking for now.
=dev-lang/rust-1.42.0 doc

# Michał Górny <mgorny@gentoo.org> (2020-03-17)
# dev-python/soappy is being removed.
dev-python/twisted soap

# Guilherme Amadio <amadio@gentoo.org> (2020-03-16)
# Requires dev-libs/cudnn which is only available on amd64
sci-physics/root cudnn

# Thomas Deutschmann <whissi@gentoo.org> (2020-03-11)
# Encrypted Media Extensions (eme-free) can't be disabled everywhere
www-client/firefox eme-free

# Michał Górny <mgorny@gentoo.org> (2020-03-03)
# Tests require py2 sleekxmpp and they are restricted anyway.
net-im/spectrum2 test

# Rick Farina <zerochaos@gentoo.org> (2020-02-26)
# dev-python/pyzmq USE=doc deps are not met with python2_7
dev-python/pyzmq doc

# Michał Górny <mgorny@gentoo.org> (2020-02-24)
# app-admin/packagekit* are masked for removal.
kde-frameworks/frameworkintegration appstream

# Michał Górny <mgorny@gentoo.org> (2020-02-08)
# The dependency is python-single-r1 while the package (and its revdeps)
# are python-r1.
dev-python/rdflib redland

# Michał Górny <mgorny@gentoo.org> (2020-02-04)
# Unsatisfiable dep due to missing impls.
dev-python/zeep tornado

# Michał Górny <mgorny@gentoo.org> (2020-01-29)
# Require Python 2 support in numpydoc.
<dev-python/matplotlib-3 doc
<=dev-python/pywavelets-0.5.2-r1 doc

# Michał Górny <mgorny@gentoo.org> (2020-01-29)
# Require dev-python/epydoc which is being removed.
<=dev-python/restkit-4.2.2 doc
<=dev-python/suds-0.6-r1 doc

# Michał Górny <mgorny@gentoo.org> (2020-01-27)
# Requires old split dev-python/twisted-*.
net-irc/telepathy-idle test
net-voip/telepathy-rakia test

# Michał Górny <mgorny@gentoo.org> (2020-01-27)
# dev-python/zsi is being removed.
sci-chemistry/pdb2pqr opal

# Michał Górny <mgorny@gentoo.org> (2020-01-12)
# libcxxrt is unmaintained and it's going to be removed.
sys-libs/libcxx libcxxrt

# Brian Evans <grknight@gentoo.org> (2019-12-19)
# PHP 7.1 is end of life and has security issues Bug 703326
# Associated packages are not ready for new versions tracked in bug 702110
dev-libs/ossp-uuid php

# Michał Górny <mgorny@gentoo.org> (2019-12-16)
# Requires old x11-libs/fox:1.6 slot.
dev-games/openscenegraph fox
dev-libs/hidapi fox
sci-mathematics/gsl-shell fox
sys-libs/gwenhywfar fox

# James Le Cuirot <chewi@gentoo.org> (2019-12-10)
# The JIT feature only works on amd64 and x86.
app-emulation/aranym jit

# Andreas Sturmlechner <asturm@gentoo.org> (2019-12-07)
# dev-python/flask-cors is PMASKED.
media-sound/beets webserver

# Michał Górny <mgorny@gentoo.org> (2019-12-04)
# Python 2.7 only packages optionally needing dev-python/ipython.
dev-python/restkit cli

# Michał Górny <mgorny@gentoo.org> (2019-12-04)
# Packages requiring dev-python/ipython to build the docs and still
# supporting Python 2.7.  This can be resolved long term via either
# using any-r1 API to build docs or cond-deps.
dev-python/matplotlib doc
dev-python/pandas doc
dev-python/pandas-datareader doc
dev-python/patsy doc
dev-python/statsmodels doc

# Haelwenn (lanodan) Monnier <contact@hacktivis.me> (2019-12-01)
# broken static-linking in glibc and maybe others
>app-shells/mksh-57-r1 static

# Patrick McLean <chutzpah@gentoo.org> (2019-11-07)
# Collides with sys-libc/glibc[crypt]
sys-libs/libxcrypt split-usr system

# Michał Górny <mgorny@gentoo.org> (2019-10-05)
# net-libs/openslp is being removed.
sys-block/open-iscsi slp
sys-block/open-isns slp

# Brian Evans <grknight@gentoo.org> (2019-10-01)
# PHP support is broken with current version
# Mask USE until they are fixed
=dev-libs/xapian-bindings-1.2.25 php
media-gfx/exact-image php
sci-geosciences/mapserver php

# Michał Górny <mgorny@gentoo.org> (2019-09-28)
# media-fonts/hkscs-ming is slated for removal.
app-i18n/xcin unicode

# Craig Andrews <candrews@gentoo.org> (2019-09-12)
# OpenSSL (as of version 1.1.1) doesn't have APIs for QUIC support
# see https://github.com/openssl/openssl/pull/8797
net-libs/ngtcp2 ssl
net-misc/curl nghttp3

# Michał Górny <mgorny@gentoo.org> (2019-09-07)
# media-libs/libnut is slated for removal.
media-video/mplayer nut

# Michał Górny <mgorny@gentoo.org> (2019-09-07)
# media-libs/fmod is slated for removal.
games-arcade/savagewheels fmod
games-fps/doomsday fmod

# Thomas Deutschmann <whissi88@gentoo.org> (2019-09-02)
# PGO not yet supported for comm-central, #693160
>=mail-client/thunderbird-68 pgo

# Matt Turner <mattst88@gentoo.org> (2019-09-01)
# <dev-scheme/guile-2 is package.mask'd
www-client/elinks guile

# Kent Fredric <kentnl@gentoo.org> (2019-08-07)
# Newer versions of nqp need newer versions of jdk to run on the jvm,
# but these currently don't exist in gentoo. The USE flags have to be
# kept around for dependency reasons from rakudo and friends.
>=dev-lang/nqp-2019.07 java

# Thomas Deutschmann <whissi@gentoo.org> (2019-08-03)
# Early microcode loading is only supported on amd64 & x86
sys-kernel/linux-firmware initramfs

# Lars Wendler <polynomial-c@gentoo.org> (2019-07-26)
# Requires dev-libs/gumbo which is only keyworded for amd64 and x86
mail-client/claws-mail litehtml

# James Le Cuirot <chewi@gentoo.org> (2019-05-21)
# Doesn't support ffmpeg-4. Portage takes a long time to report the
# conflict and the feature is half broken anyway. See
# https://github.com/visualboyadvance-m/visualboyadvance-m/issues/179.
games-emulation/vbam ffmpeg

# Ulrich Müller <ulm@gentoo.org> (2019-05-18)
# Firmware images without a known license. Most likely, upstream
# redistribution may conflict with the licenses or lack thereof
# on the images. Check the WHENCE file in the package for specific
# terms. Masked to prevent accidental installation of these files,
# bug #318841#c20.
sys-kernel/linux-firmware unknown-license

# Virgil Dupras <vdupras@gentoo.org> (2019-04-29)
# Docs are temporarily broken. See bug #680014
dev-python/pandas doc

# Ulrich Müller <ulm@gentoo.org> (2019-04-25)
# Pulls in media-fonts/kochi-substitute as dependency, which allows
# only non-commercial distribution and can therefore not be included
# with Gentoo install media. Mask the l10n_ja flag until a better
# solution is found, e.g., transition to a free font package.
app-text/ghostscript-gpl l10n_ja

# Michał Górny <mgorny@gentoo.org> (2019-03-21)
# x11-wm/afterstep is slated for removal.
x11-terms/aterm background

# Georgy Yakovlev <gyakovlev@gentoo.org> (2019-03-05)
# Not keyworded on non-x86 arches
app-admin/conserver freeipmi

# William Hubbs <williamh@gentoo.org> (2019-01-13)
# For memtest86+ mask the floppy use flag since it requires grub:0 which
# is being removed
# (bug #674364)
sys-apps/memtest86+ floppy

# Brian Evans <grknight@gentoo.org> (2019-01-11)
# Depend on the EOL PHP 7.0
dev-libs/Ice php

# Michał Górny <mgorny@gentoo.org> (2018-11-28)
# Requires last-rited games-server/ut2003-ded.
games-fps/ut2003 dedicated

# Alfredo Tupone <tupone@gentoo.org> (2018-11-18)
# go brokes build of gnat-gpl
dev-lang/gnat-gpl go

# Pacho Ramos <pacho@gentoo.org> (2018-11-11)
# pm-utils will be removed, bug #659616
sys-apps/razercfg pm-utils
sys-power/powermgmt-base pm-utils

# Alfredo Tupone <tupone@gentoo.org> (2018-10-24)
# jit brokes build of gnat-gpl
dev-lang/gnat-gpl jit

# Virgil Dupras <vdupras@gentoo.org> (2018-09-26)
# Server feature in paramiko is patched out for security reasons. It can
# be re-enabled with the 'server' USE flag, but this flag is hard
# masked. bug #666619
dev-python/paramiko server

# Andreas K. Hüttel <dilfridge@gentoo.org> (2018-08-03)
# New and for Gentoo still quite experimental. You've been
# warned. (Also, works only on x86_64 and with certain CPUs
# and compiler/binutils combinations.)
sys-libs/glibc cet

# Michał Górny <mgorny@gentoo.org> (2018-06-13)
# Requires app-arch/snappy[static-libs] that is no longer available.
# Bugs #651604, #651606.
<dev-libs/leveldb-1.20 static-libs
sys-block/fio static

# Michał Górny <mgorny@gentoo.org> (2018-05-16)
# Depends on last-rited sci-libs/coinhsl.
sci-libs/ipopt hsl

# Göktürk Yüksek <gokturk@gentoo.org> (2018-04-04)
# libewf is getting treecleaned (#547418).
# Starting with sleuthkit-4.6.0, we statically link sleuthkit to a
# locally compiled libewf. Mask the prior versions.
<app-forensics/sleuthkit-4.6.0 ewf

# Lars Wendler <polynomial-c@gentoo.org> (2018-03-14)
# Broken on all 32bit arches. Globally masked because of sys-apps/dmapi having
# no active upstream anymore.
# sys-cluster/ceph is only available on amd64 & x86, unmask per arch
net-fs/samba dmapi ceph

# Jan Ziak <0xe2.0x9a.0x9b@gmail.com> (2018-03-14)
# Mask local USE flag to satisfy repoman
app-emulation/fuse backend-svga

# Patrick McLean <chutzpah@gentoo.org> (2018-03-06)
# Requires dev-libs/boost-1.66 that is unkeyworded/masked
>=sys-cluster/ceph-12.2.4 system-boost
>=sys-cluster/ceph-14.2.8 -system-boost

# Brian Evans <grknight@gentoo.org> (2018-03-05)
# Mask embedded USE on virtual/mysql and friends to transition it to be obsolete
virtual/mysql embedded

# Brian Evans <grknight@gentoo.org> (2018-02-28)
# Mask client-libs USE to force users to install alternative standard pacakges
dev-db/mariadb client-libs
dev-db/mysql client-libs
dev-db/percona-server client-libs

# Andreas K. Hüttel <dilfridge@gentoo.org> (2018-02-02)
# This feature is not ready yet pre-2.29-r4, see bug 146882 comment 26
<sys-libs/glibc-2.29-r4 compile-locales

# Mart Raudsepp <leio@gentoo.org> (2018-01-10)
# Failing tests, with extra deps that aren't keyworded due to that
>=gnome-base/gnome-settings-daemon-3.24 test

# Andrey Utkin <andrey_utkin@gentoo.org> (2017-12-29)
# This flag can be sensibly enabled only on macos.
# Can be unmasked in prefix/darwin/macos profiles. Bug #637482
media-video/ffmpeg appkit

# James Le Cuirot <chewi@gentoo.org> (2017-12-17)
# Java 9+ is not yet fully supported on Gentoo. Having a masked
# gentoo-vm flag allows us to provide it without it breaking Gentoo
# packages. Those who wish to experiment with it as a fully recognised
# Gentoo JVM can unmask the flag.
dev-java/openjdk-jre-bin:11 gentoo-vm
dev-java/oracle-jdk-bin:11 gentoo-vm
dev-java/openjdk-bin:11 gentoo-vm
dev-java/openjdk:11 gentoo-vm

# Andreas K. Huettel <dilfridge@gentoo.org> (2017-09-09)
# Potentially destructive. Use it only if you know what you're doing.
sys-libs/glibc vanilla

# Michael Orlitzky <mjo@gentoo.org> (2017-08-24)
# This is a security risk if not used carefully, bug 628596.
net-analyzer/nrpe command-args

# Michał Górny <mgorny@gentoo.org> (2017-08-04)
# sys-kernel/openvz-sources is being treecleaned wrt #580516.
app-emulation/libvirt openvz

# Mike Gilbert <floppym@gentoo.org> (2017-07-09)
# Matthias Dahl <matthias.dahl@binary-island.eu> (2017-07-05)
# Both are not your typical garden-variety Linux programs and are
# rather sensitive when it comes to compiler flags, resulting in
# black screens, hangs or crashes. The average joe should have no
# need or even advantage to set custom compiler flags at all.
# Example: https://bugs.gentoo.org/619628
sys-boot/gnu-efi custom-cflags
sys-boot/refind custom-cflags

# Andreas K. Hüttel <dilfridge@gentoo.org> (2017-05-29)
# Does not build
sci-libs/linux-gpib php

# Michał Górny <mgorny@gentoo.org> (2017-05-15)
# Mask ruby20-only compatible packages
~media-libs/libcaca-0.99_beta19 ruby

# Matthias Maier <tamiko@gentoo.org> (2017-05-11)
# Globally mask pie use flag. Selectively unmask on specific profiles.
sys-devel/gcc pie

# Pawel Hajdan jr <phajdan.jr@gentoo.org> (2017-03-02)
# Known build issue with system libvpx:
# https://bugs.gentoo.org/show_bug.cgi?id=611394
>=www-client/chromium-58.0.3026.3 system-libvpx

# Mike Gilbert <floppym@gentoo.org> (2017-02-16)
# Multiple test failures.
sys-boot/grub:2 test

# Ian Stakenvicius (2017-01-25)
# rust on mozilla packages is experimental
www-client/firefox rust
mail-client/thunderbird rust

# Lars Wendler <polynomial-c@gentoo.org> (2017-01-19)
# Masked until system heimdal can finally be used with AD DC
net-fs/samba system-heimdal

# Magnus Granberg <zorry@gentoo.org> (2017-01-18)
# Adding the mask so that end users and devlopers are notified of the removal and have some
# time to migrate. There is no support for gcj in gcc-7
>=sys-devel/gcc-6.3.0 gcj

# Robin H. Johnson <robbat2@gentoo.org> (2016-12-04)
# Additional HAProxy functionality waiting for related stuff to land in the
# tree.
# bug 541042 - lua 5.3
net-proxy/haproxy lua
# bug (none) - 51Degrees
net-proxy/haproxy 51degrees
# bug (none) - WURFL
net-proxy/haproxy wurfl

# Pacho Ramos <pacho@gentoo.org> (2016-11-27)
# Not rely on monodevelop, bug #596656
# dev-dotnet/zeitgeist-sharp is going to be removed, bug #582894
net-irc/hexchat theme-manager

# Ulrich Müller <ulm@gentoo.org> (2016-10-15)
# The --with-cairo option is considered as experimental by upstream
# and causes problems with updating the X window, bug #592238
app-editors/emacs:25 cairo
app-editors/emacs:26 cairo

# Michał Górny <mgorny@gentoo.org> (2016-07-05)
# Mask system-clang support as it requires llvm-3.4 that is subject
# to security bug cleanup, #585102.
dev-util/intel-ocl-sdk system-clang

# Matthew Brewer <tomboy64@sina.cn> (2016-06-02)
# Acked-by: Amy Winston <amynka@gentoo.org>
# Currently broken upstream.
>=dev-lang/rakudo-2016.05 java

# Michał Górny <mgorny@gentoo.org> (2016-05-14)
# (on behalf of QA team)
# sys-devel/gcc[jit]:
# - violates strict multilib rules, #569608
# - causes collisions between multiple gcc versions, #583010
# Masking the flag until it is fixed.
sys-devel/gcc jit

# Mike Frysinger <vapier@gentoo.org> (2016-05-08)
# Most targets do not support VTV #547040.
sys-devel/gcc vtv

# Matthew Brewer <tomboy64@sina.cn> (2016-05-08)
# Acked-by: Amy Winston <amynka@gentoo.org>
# Upstream supports using Clang's address sanitizer, but apparently
# invokes it wrong.
dev-lang/moarvm asan

# Ian Stakenvicius <axs@gentoo.org> (2016-04-28)
# system-cairo support causes lots of crashing in mozilla pkgs
# (see bug #556378 and others)
www-client/firefox system-cairo
mail-client/thunderbird system-cairo

# James Le Cuirot <chewi@gentoo.org> (2016-04-10)
# Slower and less reliable than HotSpot and we don't support Java on
# any non-HotSpot platforms. Don't unmask these unless you know what
# you're doing. This has upstream's approval.
dev-java/icedtea cacao jamvm

# Patrice Clement <monsieurp@gentoo.org> (2015-11-27)
# Unsastified dependencies are upsetting repoman. Needs investigation.
app-arch/cfv bittorrent

# Rick Farina <zerochaos@gentoo.org> (2015-10-06)
# forward porting this patch is non-trivial, mask for now
>=net-wireless/wpa_supplicant-2.5 wimax

# Justin Lecher <jlec@gentoo.org> (2015-09-22)
# Downloads files during installation
# https://bugs.gentoo.org/show_bug.cgi?id=533876
>=sci-libs/vtk-6.1.0 examples

# Jason Zaman <perfinion@gentoo.org> (2015-09-06)
# This is for cgmanager which is linux only.
# Unmasked in default/linux/package.use.mask.
sys-auth/consolekit cgroups

# James Le Cuirot <chewi@gentoo.org> (2015-08-18)
# gcc[awt] is only used by gcj-jdk[awt] and that isn't needed by
# anything in the tree. The toolchain folks aren't keen to support it
# and it should probably only be used if you really know what you're
# doing *and* your name is gnu_andrew. ;) See bug #531900.
sys-devel/gcc awt
dev-java/gcj-jdk awt

# Mike Gilbert <floppym@gentoo.org> (2015-05-16)
# dev-lang/python[berkdb] fails with recent multilib sys-libs/db (bug 519584).
dev-lang/python berkdb

# Lars Wendler <polynomial-c@gentoo.org> (2015-03-19)
# >=games-strategy/freeciv-2.5.0 requires >=dev-lang/lua-5.2
games-strategy/freeciv system-lua

# Andrew Savchenko <bircoph@gentoo.org> (2015-02-11)
# Cluster code is still under development, only base functionality
# is implemented. Masking for testing and evaluation.
app-admin/clsync cluster mhash

# Brian Evans <grknight@gentoo.org> (2014-11-29)
# Strongly recommened by upstream to disable lzo and zlib due to memory leaks
# https://github.com/groonga/groonga/issues/6 (Japanese)
app-text/groonga lzo zlib

# Jeroen Roovers <jer@gentoo.org> (2014-11-18)
# Requires dev-lang/lua-5.2 (bug #253269)
net-analyzer/nmap system-lua

# Mike Frysinger <vapier@gentoo.org> (2014-10-21)
# Most targets do not support ASAN/etc... #504200.
sys-devel/gcc sanitize

# Rick Farina <zerochaos@gentoo.org> (2014-08-26)
# mirisdr does not seem to like making releases
<net-wireless/gr-osmosdr-9999 mirisdr

# Maxim Koltsov <maksbotan@gentoo.org> (2014-08-05)
# net-libs/tox is in mva overlay only ATM
app-leechcraft/lc-azoth sarin

# Pacho Ramos <pacho@gentoo.org> (2014-06-01)
# Needs hardmasked lua-5.2
>=media-plugins/grilo-plugins-0.2.12 lua

# Tom Wijsman <TomWij@gentoo.org> (2014-05-14)
# Documentation generation needs APIviz which is not in the Portage tree yet.
# Tracked in Gentoo bug #509602.
dev-java/jboss-logging doc

# Mike Frysinger <vapier@gentoo.org> (2014-02-03)
# Upstream says to not use this for now.
dev-libs/elfutils threads

# Gilles Dartiguelongue <eva@gentoo.org> (2013-12-23)
# Gstreamer support fails to build due to libtool shortcomings when using
# an intermediate library as a dependency of another libtool target dependency.
# Since this is new and unused yet, keep it masked for now and check
# with upstream if there is something that can be done to fix the issue.
media-libs/cogl gstreamer

# Julian Ospald <hasufell@gentoo.org> (2013-08-31)
# Randomly breaks consumers at runtime. Do not report
# gentoo bugs.
media-libs/libsdl2 custom-cflags

# Chí-Thanh Christopher Nguyễn <chithanh@gentoo.org> (2013-07-13)
# GL/GLES support in cairo is mutually exclusive, bug #428770.
x11-libs/cairo gles2-only

# Kacper Kowalik <xarthisius@gentoo.org> (2013-05-19)
# Fails to build, haven't had time to debug
app-doc/doxygen sqlite

# Alexis Ballier <aballier@gentoo.org> (2013-05-08)
# On behalf of Pavel Sanda <ps@twin.jikos.cz>
# Lyx is currently not working with subversion 1.7 and needs 1.6.
app-office/lyx subversion

# Christian Faulhammer <fauli@gentoo.org> (2013-05-08)
# Will not build successfully yet
mail-client/claws-mail gtk3

# Bernard Cafarelli <voyageur@gentoo.org> (2013-04-04)
# Broken for now (segmentation fault on play)
gnustep-apps/cynthiune flac

# Richard Freeman <rich0@gentoo.org> (2013-03-24)
# Dependency is masked for buffer overflows for now
dev-python/pyocr cuneiform

# Tom Wijsman <TomWij@gentoo.org> (2013-03-10)
# Experimental, masked until it compiles and works.
media-libs/avidemux-core system-ffmpeg

# Jory A. Pratt <anarchy@gentoo.org> (2012-12-15)
# PGO is known to be busted with most configurations
www-client/firefox pgo

# Diego Elio Pettenò (2012-08-27)
# The libpci access is only used for linux.
net-analyzer/net-snmp pci

# Diego Elio Pettenò (2012-08-20)
# The prevent-removal USE flag is only implemented for Linux.
sys-auth/pam_mktemp prevent-removal

# Samuli Suominen <ssuominen@gentoo.org> (2012-03-20)
# This is for udev-acl. Unmasked in default/linux/package.use.mask.
sys-auth/consolekit acl

# Bernard Cafarelli <voyageur@gentoo.org> (2012-02-08)
# libobjc2/clang experimental support
>=gnustep-base/gnustep-make-2.6.2 libobjc2

# Samuli Suominen <ssuominen@gentoo.org> (2012-01-10)
# This is required only for Linux, so mask it here and unmask in
# default/linux/package.use.mask. Bug 354923.
app-arch/libarchive e2fsprogs

# Mike Frysinger <vapier@gentoo.org> (2011-12-06)
# No one should be mucking with libssp unless they really know what they're
# doing.  Force extra work on the smart peeps to protect the not-so-smart.
sys-devel/gcc libssp

# Andrey Grozin <grozin@gentoo.org> (2011-03-29)
# dev-lang/swig-2.0.2 with -octave generated .cpp files
# incompatible with >=sci-mathematics/octave-3.4.0
sci-libs/mathgl octave

# Diego E. Pettenò <flameeyes@gentoo.org> (2011-03-27)
#
# Mask pdnsd's Linux-specific USE flags.
net-dns/pdnsd isdn urandom

# Raúl Porcel <armin76@gentoo.org> (2011-02-13)
# Masked until devs know how to use repoman
<media-video/mplayer-9999 bluray

# Tomas Touceda <chiiph@gentoo.org> (2010-04-13)
# Masked because libsvm module is broken with this release on every arch.
dev-lisp/clisp svm

# Diego E. Pettenò <flameeyes@gmail.com> (2010-01-18)
# Don't use system-wide mode unless you *really* know what you're doing.
# Keep it masked here so that I don't need to manage two versions every time.
media-sound/pulseaudio system-wide

# These are for BSD only
net-proxy/squid ipf-transparent pf-transparent

# Alexis Ballier <aballier@gentoo.org> (2011-02-16)
# Win32 specific useflag for vlc. Can be used for cross-compiling.
media-video/vlc directx
