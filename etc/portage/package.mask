# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

# Pacho Ramos <pacho@gentoo.org> (2015-04-21)
# This is for running sys-apps/systemd and also helps
# portage to not try to pull in other providers leading to
# strange blockers.
sys-fs/eudev
sys-fs/udev
#>=dev-libs/openssl-1.1.1g
#sys-libs/ldb
>=dev-python/zipp-2.0.1
>=dev-python/pytest-5.4.1
#>=dev-util/colm-0.13.0.6
#>=dev-python/twisted-20.3.0
#app-arch/brotli
#dev-python/brotlipy
=dev-lang/python-3.8.3
#>=dev-util/aruba-0.14.12
sys-fs/squashfs-tools::mv
#dev-libs/openssl::gentoo
dev-lang/python:3.9
