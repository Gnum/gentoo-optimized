#!/sbin/openrc-run
# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

extra_commands="forcestart is_master"

depend() {
	need localmount logger
	after bootmisc sshd
	use net
}

DAEMON_UTIL="/usr/%LIBDIR%/ganeti/daemon-util"

is_master() {
	[ -z "${ganeti_master}" ] && ganeti_master="$(gnt-cluster getmaster)"
	[ -z "${local_hostname}" ] && local_hostname="$(hostname -f)"
	[ "${ganeti_master}" = "${local_hostname}" ]
}

# This exists specifically for restarting a 2-node cluster where quorum might
# not be available.
forcestart() {
	ewarn "Forcing non-quorum Ganeti master start"
	GANETI_WCONFD_OPTS="${GANETI_WCONFD_OPTS} --no-voting --yes-do-it"
	GANETI_LUXID_OPTS="${GANETI_LUXID_OPTS} --no-voting --yes-do-it"
	GANETI_MASTERD_OPTS="${GANETI_LUXID_OPTS} --no-voting --yes-do-it"
	export GANETI_WCONFD_OPTS GANETI_LUXID_OPTS GANETI_MASTERD_OPTS
	start
}

start() {
	if ! [ -e ${DAEMON_UTIL} ]; then
		eerror "Could not find daemon utility at ${DAEMON_UTIL}"
		return 1
	elif ! ${DAEMON_UTIL} check-config ; then
		eerror "Incomplete configuration, will not run."
		return 1
	fi

	local daemon retval=0 optsvar started_daemons daemon_opts

	for daemon in $(${DAEMON_UTIL} list-start-daemons); do
		optsvar="$(printf "${daemon}_OPTS" | tr - _ | LC_ALL=C tr '[:lower:]' '[:upper:]')"

		case "${daemon#ganeti-}" in
			masterd|rapi|luxid|wconfd) is_master || continue;;
		esac

		eval daemon_opts=\"\$\{${optsvar}\}\"

		ebegin "Starting ${daemon}"
		eindent
		veinfo ${DAEMON_UTIL} start ${daemon} ${GANETI_OPTS} ${daemon_opts}
		${DAEMON_UTIL} start ${daemon} ${GANETI_OPTS} ${daemon_opts} || retval=${?}

		if [ ${retval} != 0  ] && [ -n "${started_daemons}" ]; then
			case ${daemon} in
				*-kvmd) retval=0; ewarn "Failed to start kvmd, continuing anyway";;
				*)
					eerror "Stopping already started daemons"
					eindent
					eend ${code} "$(${DAEMON_UTIL} check-exitcode ${code})"

					for daemon in ${started_daemons}; do
						ebegin "Stopping ${daemon}"
						${DAEMON_UTIL} stop ${daemon} ${GANETI_OPTS}
						eend ${?}
					done
					eoutdent; eoutdent
					return ${retval}
				;;
			esac
		fi
		eoutdent
		started_daemons="${started_daemons} ${daemon}"
	done
}

stop() {
	if ! [ -e ${DAEMON_UTIL} ]; then
		eerror "Could not find daemon utility at ${DAEMON_UTIL}"
		return 1
	fi

	local daemon
	local ret=0
	for daemon in $(${DAEMON_UTIL} list-stop-daemons) ; do \
		case "${daemon#ganeti-}" in
			masterd|rapi|luxid|wconfd) is_master && is_running ${daemon} || continue;;
		esac
		
		# Overwrite daemon utils to prefer using the pidfile instead of the process name
		pidfile="/run/ganeti/${daemon}.pid"
		if is_running $daemon 
		then
			ebegin "Stopping ${daemon}"
			if start-stop-daemon --stop --quiet --retry 30 --pidfile $pidfile $daemon 
			then 
				eend 0
			else
				eend 1
				ret=1
			fi
		fi
	done
	
	ebegin "Stopping"
	eend $ret
}

is_running() {
	pidfile="/run/ganeti/${1}.pid"
	test -e $pidfile && ps -p $(cat $pidfile) &>/dev/null
	return $?
}

status() {
        if ! [ -e ${DAEMON_UTIL} ]; then
                eerror "Could not find daemon utility at ${DAEMON_UTIL}"
                return 1
        fi

        local daemon
	local ret=1

        for daemon in $(${DAEMON_UTIL} list-start-daemons) ; do \
                case "${daemon#ganeti-}" in
                        masterd|rapi|luxid|wconfd) is_master || continue;;
                esac
                #${DAEMON_UTIL} stop ${daemon} ${GANETI_OPTS}
		pidfile="/run/ganeti/${daemon}.pid"
		if [[ -e $pidfile ]]
		then
			if is_running $daemon
			then
				ebegin "$daemon: running"
				ret=0
				eend 0
			else
				eerror "$daemon: crashed"
				ewend 3
				ret=3
			fi
		else
			ebegin "$daemon: stopped"
		fi
        done

	echo ""
	case $ret in 
		0) ebegin "Status: running" ; eend $ret ;;
		3) ebegin "Status: crashed" ; eend $ret ;;
		*) ebegin "Status: stopped" ;;
	esac
	return $ret
}

# vim:ft=gentoo-init-d:ts=4:sts=4:sw=4:noet:
