How to add this repo in Layman:

sudo layman -a brave-overlay

Or, if automation makes your face ache:
sudo layman -o https://gitlab.com/jason.oliveira/brave-overlay/raw/master/repositories.xml -f -a brave-overlay
