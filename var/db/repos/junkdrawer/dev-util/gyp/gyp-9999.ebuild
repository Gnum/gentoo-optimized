# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=5
PYTHON_COMPAT=( python2_7 )

inherit distutils-r1 git-r3

DESCRIPTION="Generate Your Projects"
HOMEPAGE="https://gyp.gsrc.io"
SRC_URI=""
EGIT_REPO_URI="https://chromium.googlesource.com/external/${PN}"

LICENSE="BSD"
SLOT="0"
KEYWORDS=""
IUSE=""
