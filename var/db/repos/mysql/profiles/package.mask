# Brian Evans <grknight@gentoo.org> (10 Oct 2015)
# Latest versions are masked for testing
>=dev-db/mysql-5.7
>=virtual/libmysqlclient-19
>=virtual/libmysqlclient-20
>=dev-db/percona-server-5.7

# Sergey Popov <pinkbyte@gentoo.org> (04 Sep 2014)
# Security mask, wrt bugs #488212, #498164, #500260,
# #507802 and #518718
<virtual/mysql-5.5
<dev-db/mysql-5.5.39
<dev-db/mariadb-5.5.39
