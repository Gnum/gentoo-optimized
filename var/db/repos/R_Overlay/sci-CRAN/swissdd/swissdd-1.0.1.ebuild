# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Get Swiss Federal and Cantonal V... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/swissdd_1.0.1.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/tidyr
	sci-CRAN/magrittr
	sci-CRAN/jsonlite
	sci-CRAN/dplyr
	sci-CRAN/curl
	sci-CRAN/purrr
	sci-CRAN/tibble
"
RDEPEND="${DEPEND-}"
