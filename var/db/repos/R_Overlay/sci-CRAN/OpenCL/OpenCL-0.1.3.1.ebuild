# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Interface Allowing R to Use OpenCL'
SRC_URI="http://cran.r-project.org/src/contrib/OpenCL_0.1-3.1.tar.gz"
LICENSE='BSD'

RDEPEND="${DEPEND-} virtual/opencl"
