# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Murphy Diagrams for Forecast Comparisons'
SRC_URI="http://cran.r-project.org/src/contrib/murphydiagram_0.12.2.tar.gz"
LICENSE='GPL-3'
