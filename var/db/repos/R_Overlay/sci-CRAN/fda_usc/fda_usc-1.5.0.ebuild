# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Functional Data Analysis and Uti... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/fda.usc_1.5.0.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/fda
	virtual/mgcv
	virtual/rpart
	virtual/nlme
	virtual/MASS
"
RDEPEND="${DEPEND-}"
