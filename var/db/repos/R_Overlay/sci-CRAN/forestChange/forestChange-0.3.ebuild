# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Computing Essential Biodiversity... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/forestChange_0.3.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/rgdal
	sci-CRAN/SDMTools
	sci-CRAN/raster
	sci-CRAN/rmarkdown
"
RDEPEND="${DEPEND-}"
