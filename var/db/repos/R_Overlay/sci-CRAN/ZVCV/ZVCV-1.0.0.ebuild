# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Zero-Variance Control Variates'
SRC_URI="http://cran.r-project.org/src/contrib/ZVCV_1.0.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/glmnet
	sci-CRAN/mvtnorm
	>=sci-CRAN/Rcpp-0.11.0
	sci-CRAN/partitions
	sci-CRAN/abind
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
"
