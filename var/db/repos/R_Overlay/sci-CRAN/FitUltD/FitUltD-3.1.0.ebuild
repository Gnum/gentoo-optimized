# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Fit Univariate Mixed and Usual Distributions'
SRC_URI="http://cran.r-project.org/src/contrib/FitUltD_3.1.0.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/mclust
	sci-CRAN/purrr
	sci-CRAN/ADGofTest
	sci-CRAN/fitdistrplus
	sci-CRAN/assertthat
	virtual/MASS
	sci-CRAN/ggplot2
	sci-CRAN/cowplot
	>=dev-lang/R-3.2.0
"
RDEPEND="${DEPEND-}"
