# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Directional Statistics'
SRC_URI="http://cran.r-project.org/src/contrib/Directional_3.7.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/foreach
	virtual/MASS
	sci-CRAN/Rfast
	sci-CRAN/doParallel
	sci-CRAN/RcppZiggurat
	sci-CRAN/Rfast2
	sci-CRAN/rgl
"
RDEPEND="${DEPEND-}"
