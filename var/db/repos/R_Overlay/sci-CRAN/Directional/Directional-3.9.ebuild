# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Directional Statistics'
SRC_URI="http://cran.r-project.org/src/contrib/Directional_3.9.tar.gz"
LICENSE='GPL-2'

DEPEND="virtual/MASS
	sci-CRAN/foreach
	sci-CRAN/RcppZiggurat
	sci-CRAN/bigstatsr
	sci-CRAN/doParallel
	sci-CRAN/rgl
	sci-CRAN/Rfast
	sci-CRAN/Rfast2
"
RDEPEND="${DEPEND-}"
