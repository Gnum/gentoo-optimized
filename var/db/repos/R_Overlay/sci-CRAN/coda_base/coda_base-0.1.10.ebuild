# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='A Basic Set of Functions for Com... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/coda.base_0.1.10.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-3.0.4
	>=sci-CRAN/Rcpp-0.12.12
	virtual/MASS
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
"
