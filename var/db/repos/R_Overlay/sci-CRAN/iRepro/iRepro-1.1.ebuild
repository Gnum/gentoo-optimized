# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Reproducibility for Interval-Censored Data'
SRC_URI="http://cran.r-project.org/src/contrib/iRepro_1.1.tar.gz"
LICENSE='GPL-3'
