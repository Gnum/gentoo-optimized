# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Visual Field (VF) Progression An... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/vfprogression_0.7.1.tar.gz"
LICENSE='GPL-2+'
