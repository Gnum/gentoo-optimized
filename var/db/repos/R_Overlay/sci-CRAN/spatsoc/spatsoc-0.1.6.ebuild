# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Group Animal Relocation Data by ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/spatsoc_0.1.6.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_asnipe r_suggests_knitr r_suggests_rmarkdown
	r_suggests_testthat"
R_SUGGESTS="
	r_suggests_asnipe? ( sci-CRAN/asnipe )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_testthat? ( sci-CRAN/testthat )
"
DEPEND="sci-CRAN/igraph
	sci-CRAN/rgeos
	sci-CRAN/sp
	>=sci-CRAN/data_table-1.10.5
	>=dev-lang/R-3.4
	sci-CRAN/adehabitatHR
"
RDEPEND="${DEPEND-}
	sci-libs/geos
	${R_SUGGESTS-}
"
