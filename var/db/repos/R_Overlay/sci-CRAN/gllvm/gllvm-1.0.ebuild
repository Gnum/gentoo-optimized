# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Generalized Linear Latent Variable Models'
SRC_URI="http://cran.r-project.org/src/contrib/gllvm_1.0.tar.gz"
LICENSE='GPL-2'

DEPEND="virtual/Matrix
	sci-CRAN/TMB
	sci-CRAN/fishMod
	sci-CRAN/mvabund
	sci-CRAN/mvtnorm
	sci-CRAN/statmod
	virtual/MASS
"
RDEPEND="${DEPEND-}
	sci-CRAN/TMB
	sci-CRAN/RcppEigen
"
