# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Implementation of Random Variables'
SRC_URI="http://cran.r-project.org/src/contrib/RandVar_1.1.0.tar.gz"
LICENSE='LGPL-3'

DEPEND=">=dev-lang/R-2.14.0
	>=sci-CRAN/distrEx-2.5
	sci-CRAN/startupmsg
	>=sci-CRAN/distr-2.5.2
"
RDEPEND="${DEPEND-}"
