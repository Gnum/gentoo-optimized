# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Mean and Variance Modeling of Binary Data'
SRC_URI="http://cran.r-project.org/src/contrib/BinaryEPPM_2.2.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/expm
	sci-CRAN/numDeriv
	sci-CRAN/Formula
	sci-CRAN/lmtest
"
RDEPEND="${DEPEND-}"
