# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Bayesian Logistic Regression wit... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/HTLR_0.4-1.tar.gz"
LICENSE='GPL-2'

IUSE="${IUSE-} r_suggests_bayesplot r_suggests_corrplot r_suggests_ggplot2
	r_suggests_knitr r_suggests_rda r_suggests_rmarkdown
	r_suggests_testthat"
R_SUGGESTS="
	r_suggests_bayesplot? ( sci-CRAN/bayesplot )
	r_suggests_corrplot? ( sci-CRAN/corrplot )
	r_suggests_ggplot2? ( sci-CRAN/ggplot2 )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_rda? ( sci-CRAN/rda )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_testthat? ( >=sci-CRAN/testthat-2.0.0 )
"
DEPEND=">=dev-lang/R-3.1.0
	>=sci-CRAN/Rcpp-0.12.0
	sci-CRAN/BCBCSF
	sci-CRAN/magrittr
	sci-CRAN/glmnet
"
RDEPEND="${DEPEND-}
	>=sci-CRAN/Rcpp-0.12.0
	sci-CRAN/RcppArmadillo
	${R_SUGGESTS-}
"
