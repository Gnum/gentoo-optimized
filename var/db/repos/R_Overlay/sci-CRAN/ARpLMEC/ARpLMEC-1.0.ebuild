# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Fitting Autoregressive Censored ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/ARpLMEC_1.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="virtual/MASS
	sci-CRAN/tmvtnorm
	sci-CRAN/lmec
	sci-CRAN/mvtnorm
	sci-CRAN/mnormt
	sci-CRAN/sandwich
	virtual/Matrix
	sci-CRAN/gmm
	sci-CRAN/numDeriv
"
RDEPEND="${DEPEND-}"
