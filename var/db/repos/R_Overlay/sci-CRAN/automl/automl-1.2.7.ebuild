# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Deep Learning with Metaheuristic'
SRC_URI="http://cran.r-project.org/src/contrib/automl_1.2.7.tar.gz"
LICENSE='GPL-2+'
