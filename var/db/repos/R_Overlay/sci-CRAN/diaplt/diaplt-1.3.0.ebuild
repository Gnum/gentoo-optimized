# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Beads Summary Plot of Ranges'
SRC_URI="http://cran.r-project.org/src/contrib/diaplt_1.3.0.tar.gz"
LICENSE='MIT'
