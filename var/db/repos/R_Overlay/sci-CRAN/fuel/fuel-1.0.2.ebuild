# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Framework for Unified Estimation... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/fuel_1.0.2.tar.gz"
LICENSE='GPL-3'
