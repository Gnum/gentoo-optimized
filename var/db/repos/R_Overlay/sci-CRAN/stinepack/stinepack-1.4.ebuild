# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Stineman, a Consistently Well Be... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/stinepack_1.4.tar.gz"
LICENSE='GPL-2'
