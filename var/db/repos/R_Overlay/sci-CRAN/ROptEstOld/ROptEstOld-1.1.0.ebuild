# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Optimally Robust Estimation - Old Version'
SRC_URI="http://cran.r-project.org/src/contrib/ROptEstOld_1.1.0.tar.gz"
LICENSE='LGPL-3'

DEPEND=">=sci-CRAN/distr-2.7.0
	>=sci-CRAN/distrEx-2.7.0
	sci-CRAN/evd
	>=dev-lang/R-2.14.0
	>=sci-CRAN/RandVar-1.1.0
"
RDEPEND="${DEPEND-}"
