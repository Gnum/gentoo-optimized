# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Wait for a Key Press in a Terminal'
SRC_URI="http://cran.r-project.org/src/contrib/keypress_1.2.0.tar.gz"
LICENSE='MIT'
