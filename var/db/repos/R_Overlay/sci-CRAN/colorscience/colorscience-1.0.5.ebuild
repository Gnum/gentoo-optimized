# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Color Science Methods and Data'
SRC_URI="http://cran.r-project.org/src/contrib/colorscience_1.0.5.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/Hmisc
	sci-CRAN/pracma
	sci-CRAN/sp
"
RDEPEND="${DEPEND-}"
