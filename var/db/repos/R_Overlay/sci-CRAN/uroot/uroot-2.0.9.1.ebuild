# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Unit Root Tests for Seasonal Time Series'
SRC_URI="http://cran.r-project.org/src/contrib/uroot_2.0-9.1.tar.gz"
LICENSE='GPL-2'

DEPEND=">=dev-lang/R-3.0.0"
RDEPEND="${DEPEND-} dev-util/nvidia-cuda-toolkit"
