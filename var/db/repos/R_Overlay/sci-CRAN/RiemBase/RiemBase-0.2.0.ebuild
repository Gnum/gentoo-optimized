# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Functions and C++ Header Files f... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/RiemBase_0.2.0.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/Rcpp
	>=dev-lang/R-3.0.0
	sci-CRAN/pracma
	sci-CRAN/Rdpack
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
"
