# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Temporal Process Regression'
SRC_URI="http://cran.r-project.org/src/contrib/tpr_0.3-1.2.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/lgtdl"
RDEPEND="${DEPEND-}"
