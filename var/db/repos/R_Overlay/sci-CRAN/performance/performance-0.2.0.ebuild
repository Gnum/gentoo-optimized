# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Assessment of Regression Models Performance'
SRC_URI="http://cran.r-project.org/src/contrib/performance_0.2.0.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_aer r_suggests_betareg r_suggests_brms
	r_suggests_covr r_suggests_glmmtmb r_suggests_lme4 r_suggests_loo
	r_suggests_mass r_suggests_matrix r_suggests_mlogit r_suggests_nlme
	r_suggests_ordinal r_suggests_pscl r_suggests_psych
	r_suggests_randomforest r_suggests_rmarkdown r_suggests_rstanarm
	r_suggests_rstantools r_suggests_see r_suggests_survival
	r_suggests_testthat"
R_SUGGESTS="
	r_suggests_aer? ( sci-CRAN/AER )
	r_suggests_betareg? ( sci-CRAN/betareg )
	r_suggests_brms? ( sci-CRAN/brms )
	r_suggests_covr? ( sci-CRAN/covr )
	r_suggests_glmmtmb? ( sci-CRAN/glmmTMB )
	r_suggests_lme4? ( sci-CRAN/lme4 )
	r_suggests_loo? ( sci-CRAN/loo )
	r_suggests_mass? ( virtual/MASS )
	r_suggests_matrix? ( virtual/Matrix )
	r_suggests_mlogit? ( sci-CRAN/mlogit )
	r_suggests_nlme? ( virtual/nlme )
	r_suggests_ordinal? ( sci-CRAN/ordinal )
	r_suggests_pscl? ( sci-CRAN/pscl )
	r_suggests_psych? ( sci-CRAN/psych )
	r_suggests_randomforest? ( sci-CRAN/randomForest )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_rstanarm? ( sci-CRAN/rstanarm )
	r_suggests_rstantools? ( sci-CRAN/rstantools )
	r_suggests_see? ( sci-CRAN/see )
	r_suggests_survival? ( virtual/survival )
	r_suggests_testthat? ( sci-CRAN/testthat )
"
DEPEND=">=dev-lang/R-3.0
	sci-CRAN/insight
	sci-CRAN/bayestestR
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
