# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Headers for the xtensor Library'
SRC_URI="http://cran.r-project.org/src/contrib/xtensor_0.9.0-0.tar.gz"
LICENSE='BSD'

DEPEND="sci-CRAN/Rcpp"
RDEPEND="${DEPEND-} sci-CRAN/Rcpp"
