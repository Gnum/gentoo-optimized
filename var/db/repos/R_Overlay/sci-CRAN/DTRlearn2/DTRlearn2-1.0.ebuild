# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Statistical Learning Methods for... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/DTRlearn2_1.0.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/glmnet
	virtual/MASS
	sci-CRAN/foreach
	sci-CRAN/kernlab
	virtual/Matrix
"
RDEPEND="${DEPEND-}"
