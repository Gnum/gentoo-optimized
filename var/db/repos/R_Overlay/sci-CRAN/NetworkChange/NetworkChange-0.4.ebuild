# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Bayesian Package for Network Changepoint Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/NetworkChange_0.4.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/Rmpfr
	sci-CRAN/plyr
	virtual/MASS
	sci-CRAN/tidyr
	sci-CRAN/ggvis
	sci-CRAN/MCMCpack
	sci-CRAN/ggplot2
	sci-CRAN/RColorBrewer
	sci-CRAN/abind
	sci-CRAN/mvtnorm
	sci-CRAN/LaplacesDemon
"
RDEPEND="${DEPEND-}"
