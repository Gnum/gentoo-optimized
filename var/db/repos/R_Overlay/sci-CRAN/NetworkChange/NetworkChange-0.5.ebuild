# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Bayesian Package for Network Changepoint Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/NetworkChange_0.5.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/RColorBrewer
	sci-CRAN/Rmpfr
	sci-CRAN/abind
	sci-CRAN/tidyr
	virtual/MASS
	sci-CRAN/GGally
	sci-CRAN/sna
	sci-CRAN/LaplacesDemon
	sci-CRAN/ggplot2
	sci-CRAN/mvtnorm
	sci-CRAN/qgraph
	sci-CRAN/ggvis
	sci-CRAN/rlang
	sci-CRAN/ggrepel
	sci-CRAN/gridExtra
	sci-CRAN/MCMCpack
	sci-CRAN/reshape
	sci-CRAN/igraph
	sci-CRAN/network
"
RDEPEND="${DEPEND-}"
