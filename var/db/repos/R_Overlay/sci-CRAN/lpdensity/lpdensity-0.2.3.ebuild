# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Local Polynomial Density Estimation and Inference'
SRC_URI="http://cran.r-project.org/src/contrib/lpdensity_0.2.3.tar.gz"
LICENSE='GPL-2'

DEPEND=">=dev-lang/R-3.1
	sci-CRAN/ggplot2
"
RDEPEND="${DEPEND-}"
