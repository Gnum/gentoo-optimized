# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Tools to Handle Taxonomic Lists'
SRC_URI="http://cran.r-project.org/src/contrib/taxotools_0.0.35.tar.gz"
LICENSE='CC0-1.0'

DEPEND="sci-CRAN/taxize
	sci-CRAN/wikitaxa
	sci-CRAN/plyr
	sci-CRAN/sqldf
"
RDEPEND="${DEPEND-}"
