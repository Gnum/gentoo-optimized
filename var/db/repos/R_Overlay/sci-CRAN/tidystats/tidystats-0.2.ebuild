# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Create a Tidy Statistics Output File'
SRC_URI="http://cran.r-project.org/src/contrib/tidystats_0.2.tar.gz"
LICENSE='MIT'

DEPEND="sci-CRAN/tibble
	>=dev-lang/R-3.5.0
	sci-CRAN/knitr
	sci-CRAN/purrr
	sci-CRAN/readr
	sci-CRAN/tidyr
	sci-CRAN/magrittr
	sci-CRAN/stringr
	sci-CRAN/dplyr
"
RDEPEND="${DEPEND-}"
