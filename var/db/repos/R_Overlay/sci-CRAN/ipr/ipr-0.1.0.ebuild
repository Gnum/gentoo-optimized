# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Iterative Proportional Repartition Algorithm'
SRC_URI="http://cran.r-project.org/src/contrib/ipr_0.1.0.tar.gz"
LICENSE='GPL-2+'
