# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='A Computer Program to Detect Mar... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/DetSel_1.0.3.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-2.15
	sci-CRAN/ash
"
RDEPEND="${DEPEND-}"
