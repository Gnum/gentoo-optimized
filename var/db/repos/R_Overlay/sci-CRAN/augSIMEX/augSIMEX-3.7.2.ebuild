# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Analysis of Data with Mixed Meas... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/augSIMEX_3.7.2.tar.gz"
LICENSE='GPL-2+'

DEPEND="virtual/MASS
	sci-CRAN/nleqslv
	sci-CRAN/rootSolve
	sci-CRAN/Formula
	>=sci-CRAN/Rcpp-0.12.11
"
RDEPEND="${DEPEND-} sci-CRAN/Rcpp"
