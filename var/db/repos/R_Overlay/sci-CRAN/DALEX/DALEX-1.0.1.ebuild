# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='moDel Agnostic Language for Expl... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/DALEX_1.0.1.tar.gz"
LICENSE='GPL-2+'

IUSE="${IUSE-} r_suggests_ggpubr r_suggests_gower r_suggests_ibreakdown
	r_suggests_ingredients r_suggests_ranger r_suggests_testthat"
R_SUGGESTS="
	r_suggests_ggpubr? ( sci-CRAN/ggpubr )
	r_suggests_gower? ( sci-CRAN/gower )
	r_suggests_ibreakdown? ( sci-CRAN/iBreakDown )
	r_suggests_ingredients? ( sci-CRAN/ingredients )
	r_suggests_ranger? ( sci-CRAN/ranger )
	r_suggests_testthat? ( sci-CRAN/testthat )
"
DEPEND=">=dev-lang/R-3.5
	sci-CRAN/ggplot2
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
