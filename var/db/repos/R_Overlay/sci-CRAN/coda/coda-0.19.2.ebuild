# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Output Analysis and Diagnostics for MCMC'
SRC_URI="http://cran.r-project.org/src/contrib/coda_0.19-2.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-2.14.0
	virtual/lattice
"
RDEPEND="${DEPEND-}"
