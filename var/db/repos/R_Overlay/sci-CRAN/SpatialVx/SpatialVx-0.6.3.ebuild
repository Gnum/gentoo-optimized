# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Spatial Forecast Verification'
SRC_URI="http://cran.r-project.org/src/contrib/SpatialVx_0.6-3.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/smoothie
	>=sci-CRAN/spatstat-1.37.0
	sci-CRAN/maps
	sci-CRAN/smatr
	sci-CRAN/waveslim
	virtual/boot
	sci-CRAN/distillery
	virtual/cluster
	sci-CRAN/turboEM
	>=sci-CRAN/fields-6.8
	sci-CRAN/CircStats
"
RDEPEND="${DEPEND-}"
