# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Collecting Social Media Data and... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/vosonSML_0.27.2.tar.gz"
LICENSE='GPL-3+'

DEPEND=">=sci-CRAN/rtweet-0.6.8
	sci-CRAN/tictoc
	sci-CRAN/httpuv
	sci-CRAN/data_table
	>=sci-CRAN/dplyr-0.7.8
	sci-CRAN/httr
	sci-CRAN/Hmisc
	sci-CRAN/tm
	>=sci-CRAN/igraph-1.2.2
	sci-CRAN/textutils
	sci-omegahat/RCurl
	sci-CRAN/magrittr
	>=sci-CRAN/rlang-0.3.0.1
	>=dev-lang/R-3.2.0
	>=sci-CRAN/RedditExtractoR-2.1.2
	sci-CRAN/stringr
"
RDEPEND="${DEPEND-}"
