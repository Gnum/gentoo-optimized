# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Generalised Joint Regression Modelling'
SRC_URI="http://cran.r-project.org/src/contrib/GJRM_0.1-5.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/trust
	sci-CRAN/distrEx
	sci-CRAN/copula
	sci-CRAN/psych
	virtual/Matrix
	sci-CRAN/Rmpfr
	sci-CRAN/VineCopula
	sci-CRAN/VGAM
	virtual/mgcv
	>=dev-lang/R-3.2.1
	virtual/Matrix
	virtual/survival
	sci-CRAN/mnormt
	sci-CRAN/survey
	sci-CRAN/magic
	sci-CRAN/scam
	sci-CRAN/ggplot2
	sci-CRAN/gamlss_dist
	sci-CRAN/numDeriv
"
RDEPEND="${DEPEND-}"
