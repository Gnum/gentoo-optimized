# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Detect Heatwaves and Cold-Spells'
SRC_URI="http://cran.r-project.org/src/contrib/heatwaveR_0.3.0.tar.gz"
LICENSE='MIT'

IUSE="${IUSE-} r_suggests_knitr r_suggests_rmarkdown r_suggests_testthat
	r_suggests_tidyverse"
R_SUGGESTS="
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_testthat? ( sci-CRAN/testthat )
	r_suggests_tidyverse? ( sci-CRAN/tidyverse )
"
DEPEND="sci-CRAN/dplyr
	sci-CRAN/ggplot2
	>=dev-lang/R-3.0.2
	sci-CRAN/data_table
	sci-CRAN/zoo
	sci-CRAN/tibble
	sci-CRAN/RcppRoll
	sci-CRAN/lubridate
"
RDEPEND="${DEPEND-}
	>=sci-CRAN/Rcpp-0.12.16
	sci-CRAN/RcppArmadillo
	${R_SUGGESTS-}
"
