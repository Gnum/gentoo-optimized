# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Multi-Isotope Labeling for Metabolomics Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/Miso_0.1.4.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-BIOC/S4Vectors
	sci-CRAN/plotly
"
RDEPEND="${DEPEND-}"
