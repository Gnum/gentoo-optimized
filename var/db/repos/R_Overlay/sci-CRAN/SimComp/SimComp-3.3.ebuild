# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Simultaneous Comparisons for Multiple Endpoints'
SRC_URI="http://cran.r-project.org/src/contrib/SimComp_3.3.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/mvtnorm
	sci-CRAN/multcomp
	sci-CRAN/mratios
"
RDEPEND="${DEPEND-}"
