# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Time Series Plot'
SRC_URI="http://cran.r-project.org/src/contrib/rtsplot_0.1.0.tar.gz"
LICENSE='MIT'

DEPEND="sci-CRAN/xts
	sci-CRAN/quantmod
	sci-CRAN/RColorBrewer
	sci-CRAN/zoo
	sci-CRAN/TTR
"
RDEPEND="${DEPEND-}"
