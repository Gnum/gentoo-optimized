# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Randomization for Block Random Clinical Trials'
SRC_URI="http://cran.r-project.org/src/contrib/blockrand_1.5.tar.gz"
LICENSE='GPL-2'
