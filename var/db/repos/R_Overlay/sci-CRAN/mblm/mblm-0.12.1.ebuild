# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Median-Based Linear Models'
SRC_URI="http://cran.r-project.org/src/contrib/mblm_0.12.1.tar.gz"
LICENSE='GPL-2+'
