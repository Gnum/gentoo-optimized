# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Simple Interface to Microsoft Graph'
SRC_URI="http://cran.r-project.org/src/contrib/AzureGraph_1.0.2.tar.gz"
LICENSE='MIT'

IUSE="${IUSE-} r_suggests_azurermr r_suggests_knitr r_suggests_testthat"
R_SUGGESTS="
	r_suggests_azurermr? ( sci-CRAN/AzureRMR )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_testthat? ( sci-CRAN/testthat )
"
DEPEND="sci-CRAN/jsonlite
	>=sci-CRAN/httr-1.3
	>=dev-lang/R-3.3
	sci-CRAN/AzureAuth
	sci-CRAN/openssl
	sci-CRAN/R6
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
