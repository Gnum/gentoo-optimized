# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Tools for 12-Tone Musical Composition'
SRC_URI="http://cran.r-project.org/src/contrib/schoenberg_2.0.0.tar.gz"
LICENSE='GPL-3+'
