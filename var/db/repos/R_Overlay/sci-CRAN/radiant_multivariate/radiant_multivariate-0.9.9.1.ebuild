# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Multivariate Menu for Radiant: B... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/radiant.multivariate_0.9.9.1.tar.gz"
LICENSE='AGPL-3'

IUSE="${IUSE-} r_suggests_pkgdown r_suggests_testthat"
R_SUGGESTS="
	r_suggests_pkgdown? ( >=sci-CRAN/pkgdown-1.1.0 )
	r_suggests_testthat? ( >=sci-CRAN/testthat-2.0.0 )
"
DEPEND=">=sci-CRAN/psych-1.8.4
	>=sci-CRAN/radiant_data-0.9.9
	>=sci-CRAN/magrittr-1.5
	virtual/MASS
	>=sci-CRAN/ggplot2-2.2.1
	>=dev-lang/R-3.4.0
	>=sci-CRAN/GPArotation-2014.11.1
	>=sci-CRAN/gridExtra-2.0.0
	>=sci-CRAN/scales-0.4.0
	>=sci-CRAN/rlang-0.3.1
	>=sci-CRAN/shiny-1.2.0
	>=sci-CRAN/radiant_model-0.9.9
	>=sci-CRAN/car-2.1.1
	>=sci-CRAN/import-1.1.0
	>=sci-CRAN/Gmedian-1.2.3
	>=sci-CRAN/ggrepel-0.8
	>=sci-CRAN/dplyr-0.8.0
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
