# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Command-Line Argument Parser'
SRC_URI="http://cran.r-project.org/src/contrib/argparser_0.6.tar.gz"
LICENSE='GPL-3+'
