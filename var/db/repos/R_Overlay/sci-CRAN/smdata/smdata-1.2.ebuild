# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Data to Accompany Smithson & Merkle, 2013'
SRC_URI="http://cran.r-project.org/src/contrib/smdata_1.2.tar.gz"
LICENSE='GPL-2'
