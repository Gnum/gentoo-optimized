# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Procedures for Ecological Assess... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/ecoval_1.2.2.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/utility
	sci-CRAN/rivernet
	sci-CRAN/jpeg
"
RDEPEND="${DEPEND-}"
