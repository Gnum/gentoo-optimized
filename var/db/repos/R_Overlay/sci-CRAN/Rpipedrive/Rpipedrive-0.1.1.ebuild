# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Pipedrive APIs Functions to Impr... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/Rpipedrive_0.1.1.tar.gz"
LICENSE='GPL-2'

DEPEND=">=dev-lang/R-3.0.0
	>=sci-CRAN/httr-1.3.1
	>=sci-CRAN/jsonlite-1.5
"
RDEPEND="${DEPEND-}"
