# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Microarray Data Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/MiDA_0.1.0.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/caret
	sci-BIOC/preprocessCore
	sci-CRAN/SQN
	sci-CRAN/gbm
	sci-BIOC/limma
	sci-CRAN/pROC
	sci-BIOC/genefilter
	>=dev-lang/R-3.5.0
"
RDEPEND="${DEPEND-}"
