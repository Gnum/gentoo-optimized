# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Companion Animal Population Management'
SRC_URI="http://cran.r-project.org/src/contrib/capm_0.13.8.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/magrittr
	sci-CRAN/sf
	sci-CRAN/tidyr
	sci-CRAN/deSolve
	>=dev-lang/R-3.4
	sci-CRAN/circlize
	sci-CRAN/FME
	sci-CRAN/ggplot2
	sci-CRAN/dplyr
	sci-CRAN/survey
"
RDEPEND="${DEPEND-}"
