# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Companion Animal Population Management'
SRC_URI="http://cran.r-project.org/src/contrib/capm_0.13.5.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/deSolve
	sci-CRAN/FME
	sci-CRAN/dplyr
	sci-CRAN/magrittr
	sci-CRAN/survey
	sci-CRAN/tidyr
	sci-CRAN/ggplot2
	sci-CRAN/circlize
	>=dev-lang/R-3.4
	sci-CRAN/sf
"
RDEPEND="${DEPEND-}"
