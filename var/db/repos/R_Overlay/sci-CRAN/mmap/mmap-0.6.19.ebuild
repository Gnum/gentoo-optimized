# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Map Pages of Memory'
SRC_URI="http://cran.r-project.org/src/contrib/mmap_0.6-19.tar.gz"
LICENSE='GPL-3'
