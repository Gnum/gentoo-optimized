# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Map Pages of Memory'
SRC_URI="http://cran.r-project.org/src/contrib/mmap_0.6-17.tar.gz"
LICENSE='GPL-3'
