# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Open GenBank Files'
SRC_URI="http://cran.r-project.org/src/contrib/read.gb_2.0.1.tar.gz"
LICENSE='GPL-2+'
