# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='ttf2pt1 Program'
SRC_URI="http://cran.r-project.org/src/contrib/Rttf2pt1_1.3.7.tar.gz"

DEPEND=">=dev-lang/R-2.15"
RDEPEND="${DEPEND-}"
