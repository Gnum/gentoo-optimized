# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Snowball Stemmers Based on the C... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/SnowballC_0.6.0.tar.gz"
LICENSE='BSD'
