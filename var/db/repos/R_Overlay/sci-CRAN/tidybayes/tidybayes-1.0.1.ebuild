# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Tidy Data and Geoms for Bayesian Models'
SRC_URI="http://cran.r-project.org/src/contrib/tidybayes_1.0.1.tar.gz"
LICENSE='GPL-3+'

IUSE="${IUSE-} r_suggests_bayesplot r_suggests_bindrcpp r_suggests_brms
	r_suggests_broom r_suggests_covr r_suggests_cowplot
	r_suggests_dotwhisker r_suggests_emmeans r_suggests_gdtools
	r_suggests_import r_suggests_jagsui r_suggests_knitr
	r_suggests_mcmcglmm r_suggests_modelr r_suggests_rjags
	r_suggests_rmarkdown r_suggests_rstan r_suggests_rstanarm
	r_suggests_rstantools r_suggests_runjags r_suggests_testthat
	r_suggests_vdiffr"
R_SUGGESTS="
	r_suggests_bayesplot? ( sci-CRAN/bayesplot )
	r_suggests_bindrcpp? ( sci-CRAN/bindrcpp )
	r_suggests_brms? ( >=sci-CRAN/brms-2.4.0 )
	r_suggests_broom? ( >=sci-CRAN/broom-0.4.3 )
	r_suggests_covr? ( sci-CRAN/covr )
	r_suggests_cowplot? ( sci-CRAN/cowplot )
	r_suggests_dotwhisker? ( sci-CRAN/dotwhisker )
	r_suggests_emmeans? ( sci-CRAN/emmeans )
	r_suggests_gdtools? ( sci-CRAN/gdtools )
	r_suggests_import? ( sci-CRAN/import )
	r_suggests_jagsui? ( sci-CRAN/jagsUI )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_mcmcglmm? ( sci-CRAN/MCMCglmm )
	r_suggests_modelr? ( sci-CRAN/modelr )
	r_suggests_rjags? ( sci-CRAN/rjags )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_rstan? ( >=sci-CRAN/rstan-2.17.0 )
	r_suggests_rstanarm? ( >=sci-CRAN/rstanarm-2.17.2 )
	r_suggests_rstantools? ( sci-CRAN/rstantools )
	r_suggests_runjags? ( sci-CRAN/runjags )
	r_suggests_testthat? ( sci-CRAN/testthat )
	r_suggests_vdiffr? ( sci-CRAN/vdiffr )
"
DEPEND="sci-CRAN/tidyselect
	sci-CRAN/forcats
	>=sci-CRAN/purrr-0.2.3
	>=sci-CRAN/ggplot2-3.0.0
	virtual/MASS
	sci-CRAN/stringr
	sci-CRAN/HDInterval
	sci-CRAN/tidyr
	sci-CRAN/magrittr
	sci-CRAN/plyr
	sci-CRAN/dplyr
	sci-CRAN/rlang
	sci-CRAN/arrayhelpers
	sci-CRAN/coda
	sci-CRAN/ggstance
	sci-CRAN/ggridges
	sci-CRAN/stringi
	sci-CRAN/tibble
	sci-CRAN/LaplacesDemon
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
