# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Scale-Dependent Hyperpriors in S... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/sdPrior_0.6.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/pscl
	>=dev-lang/R-3.1.0
	virtual/mgcv
	sci-CRAN/doParallel
	sci-CRAN/GB2
	virtual/MASS
	sci-CRAN/mvtnorm
"
RDEPEND="${DEPEND-}"
