# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Seasonal Analysis of Health Data'
SRC_URI="http://cran.r-project.org/src/contrib/season_0.3.6.tar.gz"
LICENSE='GPL-3+'

DEPEND="virtual/MASS
	>=dev-lang/R-3.0.1
	>=sci-CRAN/ggplot2-0.9.3
	sci-CRAN/coda
	virtual/mgcv
	virtual/survival
"
RDEPEND="${DEPEND-}"
