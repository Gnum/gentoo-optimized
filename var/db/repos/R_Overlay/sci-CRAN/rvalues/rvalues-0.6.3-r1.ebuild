# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='R-Values for Ranking in High-Dimensional Settings'
SRC_URI="http://cran.r-project.org/src/contrib/rvalues_0.6.3.tar.gz -> rvalues_0.6.3-r1.tar.gz"
LICENSE='GPL-2'
