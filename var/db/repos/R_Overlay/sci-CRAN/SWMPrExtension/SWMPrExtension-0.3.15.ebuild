# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Additional Functions for Retriev... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/SWMPrExtension_0.3.15.tar.gz"
LICENSE='CC0-1.0'

DEPEND="sci-CRAN/sp
	sci-CRAN/ggplot2
	>=dev-lang/R-3.2.0
	sci-CRAN/magrittr
	sci-CRAN/flextable
	sci-CRAN/lubridate
	sci-CRAN/rlang
	sci-CRAN/RColorBrewer
	sci-CRAN/maptools
	sci-CRAN/EnvStats
	sci-CRAN/ggthemes
	sci-CRAN/dplyr
	sci-CRAN/leaflet
	sci-CRAN/rgeos
	sci-CRAN/officer
	sci-CRAN/broom
	sci-CRAN/scales
	sci-CRAN/tidyr
	sci-CRAN/SWMPr
	sci-CRAN/rgdal
"
RDEPEND="${DEPEND-}"
