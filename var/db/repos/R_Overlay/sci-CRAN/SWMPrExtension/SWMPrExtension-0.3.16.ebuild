# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Additional Functions for Retriev... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/SWMPrExtension_0.3.16.tar.gz"
LICENSE='CC0-1.0'

DEPEND="sci-CRAN/tidyr
	sci-CRAN/ggplot2
	sci-CRAN/flextable
	sci-CRAN/EnvStats
	sci-CRAN/rgdal
	sci-CRAN/rgeos
	sci-CRAN/lubridate
	>=dev-lang/R-3.2.0
	sci-CRAN/dplyr
	sci-CRAN/sp
	sci-CRAN/SWMPr
	sci-CRAN/officer
	sci-CRAN/magrittr
	sci-CRAN/ggthemes
	sci-CRAN/leaflet
	sci-CRAN/scales
	sci-CRAN/broom
	sci-CRAN/RColorBrewer
	sci-CRAN/maptools
	sci-CRAN/rlang
"
RDEPEND="${DEPEND-}"
