# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Fit Continuous-Time State-Space ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/foieGras_0.2.0.tar.gz"
LICENSE='AGPL-3'

IUSE="${IUSE-} r_suggests_covr r_suggests_knitr r_suggests_rgeos
	r_suggests_rmarkdown r_suggests_rnaturalearth r_suggests_spatial
	r_suggests_testthat r_suggests_units"
R_SUGGESTS="
	r_suggests_covr? ( sci-CRAN/covr )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_rgeos? ( sci-CRAN/rgeos )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_rnaturalearth? ( sci-CRAN/rnaturalearth )
	r_suggests_spatial? ( virtual/spatial )
	r_suggests_testthat? ( sci-CRAN/testthat )
	r_suggests_units? ( sci-CRAN/units )
"
DEPEND="sci-CRAN/gridExtra
	sci-CRAN/lubridate
	sci-CRAN/ggplot2
	sci-CRAN/argosfilter
	sci-CRAN/stringr
	sci-CRAN/dplyr
	sci-CRAN/tibble
	>=dev-lang/R-3.3.0
	sci-CRAN/sf
	sci-CRAN/TMB
	sci-CRAN/magrittr
"
RDEPEND="${DEPEND-}
	sci-CRAN/TMB
	sci-CRAN/RcppEigen
	${R_SUGGESTS-}
"
