# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Analysis of Basketball Data'
SRC_URI="http://cran.r-project.org/src/contrib/BAwiR_1.1.1.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/rworldmap
	sci-CRAN/Anthropometry
	sci-CRAN/purrr
	sci-CRAN/httr
	sci-CRAN/stringi
	sci-CRAN/ggplot2
	sci-CRAN/stringr
	sci-CRAN/ggthemes
	sci-CRAN/dplyr
	sci-CRAN/scales
	sci-CRAN/lubridate
	sci-CRAN/rvest
	>=dev-lang/R-3.4.0
	sci-CRAN/tidyr
	sci-CRAN/reshape2
	sci-CRAN/magrittr
	sci-CRAN/plyr
	sci-CRAN/hrbrthemes
	sci-CRAN/xml2
"
RDEPEND="${DEPEND-}"
