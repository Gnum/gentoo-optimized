# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Sample Size Calculation for the ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/SampleSize4ClinicalTrials_0.2.2.tar.gz"
LICENSE='GPL-3'
