# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Time Series Regression Models wi... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/dLagM_1.0.10.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/dynlm
	sci-CRAN/AER
	sci-CRAN/formula_tools
	sci-CRAN/wavethresh
	sci-CRAN/plyr
"
RDEPEND="${DEPEND-}"
