# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Time Series Regression Models wi... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/dLagM_1.0.12.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/nardl
	sci-CRAN/wavethresh
	sci-CRAN/plyr
	sci-CRAN/formula_tools
	sci-CRAN/dynlm
	sci-CRAN/strucchange
	sci-CRAN/lmtest
	sci-CRAN/AER
"
RDEPEND="${DEPEND-}"
