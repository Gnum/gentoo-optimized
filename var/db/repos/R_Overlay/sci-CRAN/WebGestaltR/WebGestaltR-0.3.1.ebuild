# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='The R Version of WebGestalt'
SRC_URI="http://cran.r-project.org/src/contrib/WebGestaltR_0.3.1.tar.gz"
LICENSE='LGPL-3+'

DEPEND="sci-CRAN/readr
	sci-CRAN/doRNG
	>=dev-lang/R-3.3
	sci-CRAN/igraph
	>=sci-CRAN/rjson-0.2.15
	sci-CRAN/dplyr
	>=sci-CRAN/foreach-1.4.0
	sci-CRAN/rlang
	virtual/cluster
	sci-CRAN/Rcpp
	>=sci-CRAN/doParallel-1.0.10
	sci-CRAN/httr
	sci-CRAN/whisker
"
RDEPEND="${DEPEND-} sci-CRAN/Rcpp"
