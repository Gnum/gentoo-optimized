# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='The R Version of WebGestalt'
SRC_URI="http://cran.r-project.org/src/contrib/WebGestaltR_0.1.1.tar.gz"
LICENSE='LGPL-3+'

DEPEND=">=sci-CRAN/PythonInR-0.1.3
	>=sci-CRAN/doParallel-1.0.10
	>=sci-CRAN/rjson-0.2.15
	>=sci-CRAN/foreach-1.4.0
	>=sci-CRAN/pkgmaker-0.22
	>=sci-CRAN/data_table-1.10.0
	>=dev-lang/R-3.3
"
RDEPEND="${DEPEND-}
	virtual/jdk
	dev-lang/python
"
