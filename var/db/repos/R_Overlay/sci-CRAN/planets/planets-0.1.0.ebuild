# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Simple and Accessible Data from all Known Planets'
SRC_URI="http://cran.r-project.org/src/contrib/planets_0.1.0.tar.gz"
LICENSE='MIT'
