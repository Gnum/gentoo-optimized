# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Data Mining Classification and Regression Methods'
SRC_URI="http://cran.r-project.org/src/contrib/rminer_1.4.3.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/plotrix
	virtual/rpart
	virtual/nnet
	virtual/lattice
	sci-CRAN/pls
	sci-CRAN/e1071
	sci-CRAN/adabag
	sci-CRAN/glmnet
	sci-CRAN/kernlab
	sci-CRAN/Cubist
	sci-CRAN/mda
	sci-CRAN/randomForest
	sci-CRAN/kknn
	sci-CRAN/xgboost
	virtual/MASS
	sci-CRAN/party
"
RDEPEND="${DEPEND-}"
