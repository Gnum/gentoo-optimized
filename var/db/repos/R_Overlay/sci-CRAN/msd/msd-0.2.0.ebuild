# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Method of Successive Dichotomizations'
SRC_URI="http://cran.r-project.org/src/contrib/msd_0.2.0.tar.gz"
LICENSE='GPL-2+'
