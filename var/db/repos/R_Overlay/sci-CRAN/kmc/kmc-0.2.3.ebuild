# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Kaplan-Meier Estimator with Cons... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/kmc_0.2-3.tar.gz"
LICENSE='LGPL-3'

DEPEND=">=dev-lang/R-2.13.1
	sci-CRAN/rootSolve
	sci-CRAN/emplik
"
RDEPEND="${DEPEND-} sci-CRAN/Rcpp"
