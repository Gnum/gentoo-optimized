# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Global Validation of Linear Models Assumptions'
SRC_URI="http://cran.r-project.org/src/contrib/gvlma_1.0.0.3.tar.gz"
LICENSE='GPL-2+'
