# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Market Area Models for Retail an... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/MCI2_1.0.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/MCI
	sci-CRAN/REAT
	sci-CRAN/reshape
	sci-CRAN/tmaptools
	sci-CRAN/osrm
"
RDEPEND="${DEPEND-}"
