# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Index Numbers in Social Sciences'
SRC_URI="http://cran.r-project.org/src/contrib/IndexNumber_1.1.tar.gz"
LICENSE='GPL-2'
