# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Exponentially Modified Gaussian (EMG) Distribution'
SRC_URI="http://cran.r-project.org/src/contrib/emg_1.0.7.tar.gz"
LICENSE='GPL-2'
