# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Create a Random List for Crossover Studies'
SRC_URI="http://cran.r-project.org/src/contrib/randomizeBE_0.3-4.tar.gz"
LICENSE='GPL-2+'
