# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Accurate Parentage Analysis in t... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/apparent_1.1.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-3.0.2
	sci-CRAN/outliers
"
RDEPEND="${DEPEND-}"
