# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Dose-Finding by the Continual Reassessment Method'
SRC_URI="http://cran.r-project.org/src/contrib/dfcrm_0.2-2.1.tar.gz"
LICENSE='GPL-2'
