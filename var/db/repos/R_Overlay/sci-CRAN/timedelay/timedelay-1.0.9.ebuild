# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Time Delay Estimation for Stocha... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/timedelay_1.0.9.tar.gz"
LICENSE='GPL-2'

DEPEND=">=dev-lang/R-3.5.0
	virtual/MASS
	>=sci-CRAN/mvtnorm-1.0.11
"
RDEPEND="${DEPEND-}"
