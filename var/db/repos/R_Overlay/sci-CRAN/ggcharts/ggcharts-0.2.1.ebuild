# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Shorten the Distance from Data V... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/ggcharts_0.2.1.tar.gz"
LICENSE='MIT'

IUSE="${IUSE-} r_suggests_gapminder r_suggests_knitr r_suggests_lintr
	r_suggests_rmarkdown r_suggests_scales r_suggests_spelling
	r_suggests_testthat r_suggests_tibble r_suggests_tidyr
	r_suggests_vdiffr"
R_SUGGESTS="
	r_suggests_gapminder? ( sci-CRAN/gapminder )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_lintr? ( sci-CRAN/lintr )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_scales? ( sci-CRAN/scales )
	r_suggests_spelling? ( sci-CRAN/spelling )
	r_suggests_testthat? ( >=sci-CRAN/testthat-2.1.0 )
	r_suggests_tibble? ( sci-CRAN/tibble )
	r_suggests_tidyr? ( sci-CRAN/tidyr )
	r_suggests_vdiffr? ( sci-CRAN/vdiffr )
"
DEPEND=">=sci-CRAN/ggplot2-3.0.0
	sci-CRAN/dplyr
	>=dev-lang/R-3.5.0
	sci-CRAN/colorspace
	sci-CRAN/lifecycle
	sci-CRAN/patchwork
	sci-CRAN/rlang
	sci-CRAN/magrittr
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
