# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Interface Utilities, Model Templ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/runjags_2.0.4-2.tar.gz"
LICENSE='GPL-2'

IUSE="${IUSE-} r_suggests_knitr r_suggests_modeest r_suggests_rjags"
R_SUGGESTS="
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_modeest? ( sci-CRAN/modeest )
	r_suggests_rjags? ( sci-CRAN/rjags )
"
DEPEND="virtual/lattice
	>=dev-lang/R-2.14.0
	>=sci-CRAN/coda-0.17.1
"
RDEPEND="${DEPEND-}
	sci-mathematics/jags
	${R_SUGGESTS-}
"
