# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Generalized Boosted Regression Models'
SRC_URI="http://cran.r-project.org/src/contrib/gbm_2.1.5.tar.gz"
LICENSE='GPL-2+'

IUSE="${IUSE-} r_suggests_knitr r_suggests_pdp r_suggests_runit
	r_suggests_viridis"
R_SUGGESTS="
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_pdp? ( sci-CRAN/pdp )
	r_suggests_runit? ( sci-CRAN/RUnit )
	r_suggests_viridis? ( sci-CRAN/viridis )
"
DEPEND="sci-CRAN/gridExtra
	virtual/survival
	virtual/lattice
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
