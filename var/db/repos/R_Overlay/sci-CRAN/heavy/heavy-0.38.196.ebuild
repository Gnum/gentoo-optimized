# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Robust Estimation Using Heavy-Tailed Distributions'
SRC_URI="http://cran.r-project.org/src/contrib/heavy_0.38.196.tar.gz"
LICENSE='GPL-2+'
