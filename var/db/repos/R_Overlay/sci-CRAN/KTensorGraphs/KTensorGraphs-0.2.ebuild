# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Co-Tucker3 Analysis of Two Sequences of Matrices'
SRC_URI="http://cran.r-project.org/src/contrib/KTensorGraphs_0.2.tar.gz"
LICENSE='GPL-2+'
