# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Weighting and Weighted Statistics'
SRC_URI="http://cran.r-project.org/src/contrib/weights_1.0.1.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/Hmisc
	sci-CRAN/gdata
	sci-CRAN/mice
"
RDEPEND="${DEPEND-}"
