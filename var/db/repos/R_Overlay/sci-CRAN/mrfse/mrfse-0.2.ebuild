# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Markov Random Field Structure Estimator'
SRC_URI="http://cran.r-project.org/src/contrib/mrfse_0.2.tar.gz"
LICENSE='GPL-3+'
