# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Integrated Regression Goodness of Fit'
SRC_URI="http://cran.r-project.org/src/contrib/intRegGOF_0.85-5.tar.gz"
LICENSE='GPL-2+'
