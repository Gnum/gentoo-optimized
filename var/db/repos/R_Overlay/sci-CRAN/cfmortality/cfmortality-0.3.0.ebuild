# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Cystic Fibrosis Survival Predict... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/cfmortality_0.3.0.tar.gz"
LICENSE='GPL-3'
