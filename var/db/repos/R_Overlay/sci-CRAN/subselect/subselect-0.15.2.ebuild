# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Selecting Variable Subsets'
SRC_URI="http://cran.r-project.org/src/contrib/subselect_0.15.2.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-3.5.0
	virtual/MASS
	sci-CRAN/corpcor
	sci-CRAN/ISwR
"
RDEPEND="${DEPEND-}"
