# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='log Normal Linear Regression'
SRC_URI="http://cran.r-project.org/src/contrib/logNormReg_0.3-0.tar.gz"
LICENSE='GPL-2+'
