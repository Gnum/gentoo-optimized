# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Using KEEL in R Code'
SRC_URI="http://cran.r-project.org/src/contrib/RKEEL_1.2.1.tar.gz"
LICENSE='GPL-2+'

DEPEND="virtual/Matrix
	sci-CRAN/R6
	sci-CRAN/foreach
	>=sci-CRAN/RKEELjars-1.0.18
	sci-CRAN/pmml
	sci-CRAN/gdata
	>=sci-CRAN/RKEELdata-1.0.5
	>=dev-lang/R-3.4.0
	sci-CRAN/arules
	sci-CRAN/rJava
	sci-CRAN/doParallel
	sci-omegahat/XML
"
RDEPEND="${DEPEND-} virtual/jdk"
