# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Stanford ATLAS Search Engine API'
SRC_URI="http://cran.r-project.org/src/contrib/atlas_0.6.0.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/httr
	sci-CRAN/testthat
"
RDEPEND="${DEPEND-}"
