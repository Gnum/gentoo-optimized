# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Gathering Metadata About Publica... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/pubmedR_0.0.1.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_bibliometrix r_suggests_knitr r_suggests_rmarkdown"
R_SUGGESTS="
	r_suggests_bibliometrix? ( sci-CRAN/bibliometrix )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
"
DEPEND="sci-CRAN/rentrez
	sci-omegahat/XML
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
