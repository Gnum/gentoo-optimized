# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Doubly Robust Generalized Estimating Equations'
SRC_URI="http://cran.r-project.org/src/contrib/drgee_1.1.8.tar.gz"
LICENSE='|| ( GPL-2 GPL-3 )'

DEPEND="sci-CRAN/nleqslv
	sci-CRAN/data_table
	sci-CRAN/Rcpp
	virtual/survival
	>=dev-lang/R-3.0.0
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
"
