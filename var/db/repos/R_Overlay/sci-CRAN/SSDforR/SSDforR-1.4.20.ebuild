# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Functions to Analyze Single System Data'
SRC_URI="http://cran.r-project.org/src/contrib/SSDforR_1.4.20.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/metafor
	sci-CRAN/TTR
	virtual/MASS
	sci-CRAN/TSA
	sci-CRAN/psych
	dev-lang/R[tk]
	sci-CRAN/MAd
"
RDEPEND="${DEPEND-}"
