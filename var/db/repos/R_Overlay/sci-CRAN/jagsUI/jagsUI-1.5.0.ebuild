# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='A Wrapper Around rjags to Streamline JAGS Analyses'
SRC_URI="http://cran.r-project.org/src/contrib/jagsUI_1.5.0.tar.gz"
LICENSE='GPL-2'

DEPEND="virtual/lattice
	>=dev-lang/R-2.14.0
	>=sci-CRAN/coda-0.13
	>=sci-CRAN/rjags-3.13
"
RDEPEND="${DEPEND-} sci-mathematics/jags"
