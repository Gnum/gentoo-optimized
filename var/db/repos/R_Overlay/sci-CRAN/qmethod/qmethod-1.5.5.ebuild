# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Analysis of Subjective Perspecti... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/qmethod_1.5.5.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/xtable
	sci-CRAN/psych
	sci-CRAN/digest
	sci-CRAN/knitr
	sci-CRAN/GPArotation
"
RDEPEND="${DEPEND-}"
