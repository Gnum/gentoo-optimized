# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Spectral Irradiance Data for Lamps'
SRC_URI="http://cran.r-project.org/src/contrib/photobiologyLamps_0.4.2.tar.gz"
LICENSE='GPL-2+'

IUSE="${IUSE-} r_suggests_ggplot2 r_suggests_ggspectra r_suggests_knitr
	r_suggests_photobiologywavebands"
R_SUGGESTS="
	r_suggests_ggplot2? ( >=sci-CRAN/ggplot2-2.2.1 )
	r_suggests_ggspectra? ( >=sci-CRAN/ggspectra-0.2.3 )
	r_suggests_knitr? ( >=sci-CRAN/knitr-1.19 )
	r_suggests_photobiologywavebands? ( >=sci-CRAN/photobiologyWavebands-0.4.2 )
"
DEPEND=">=dev-lang/R-3.3.0
	>=sci-CRAN/photobiology-0.9.18
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
