# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Case-Control Analysis of Multi-Allelic Loci'
SRC_URI="http://cran.r-project.org/src/contrib/BIGDAWG_2.3.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-omegahat/XML
	>=dev-lang/R-3.5.0
	sci-CRAN/knitr
	sci-CRAN/httr
	sci-CRAN/haplo_stats
"
RDEPEND="${DEPEND-}"
