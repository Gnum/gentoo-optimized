# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Interpolation Grids for Packages... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/RobAStRDA_1.2.0.tar.gz"
LICENSE='LGPL-3'
