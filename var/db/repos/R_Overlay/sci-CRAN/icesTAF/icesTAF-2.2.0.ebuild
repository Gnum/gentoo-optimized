# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Functions to Support the ICES Tr... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/icesTAF_2.2-0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/bibtex
	sci-CRAN/remotes
"
RDEPEND="${DEPEND-}"
