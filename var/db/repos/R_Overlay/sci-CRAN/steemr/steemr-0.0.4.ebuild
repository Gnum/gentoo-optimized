# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='A Tool for Processing Steem Data'
SRC_URI="http://cran.r-project.org/src/contrib/steemr_0.0.4.tar.gz"
LICENSE='MIT'

DEPEND="sci-omegahat/RCurl
	sci-CRAN/openair
	virtual/lattice
	sci-CRAN/rlist
	sci-CRAN/wordcloud
	sci-CRAN/VennDiagram
	sci-omegahat/XML
	>=dev-lang/R-3.1.0
	sci-CRAN/zoo
	sci-CRAN/tm
"
RDEPEND="${DEPEND-}"
