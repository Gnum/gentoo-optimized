# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Semiparametric Bayesian Gaussian... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/sbgcop_0.980.tar.gz"
LICENSE='GPL-2+'
