# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Objective Bayesian Model Discrim... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/OBsMD_5.3.tar.gz"
LICENSE='GPL-2+'
