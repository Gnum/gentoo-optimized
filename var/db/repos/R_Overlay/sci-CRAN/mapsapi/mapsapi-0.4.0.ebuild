# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='sf-Compatible Interface to Google Maps APIs'
SRC_URI="http://cran.r-project.org/src/contrib/mapsapi_0.4.0.tar.gz"
LICENSE='MIT'

IUSE="${IUSE-} r_suggests_knitr r_suggests_leaflet r_suggests_rmarkdown"
R_SUGGESTS="
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_leaflet? ( sci-CRAN/leaflet )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
"
DEPEND=">=sci-CRAN/magrittr-1.5
	>=sci-CRAN/xml2-1.1.1
	>=sci-CRAN/sf-0.5.3
	>=sci-CRAN/plyr-1.8.4
	>=sci-CRAN/bitops-1.0.6
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
