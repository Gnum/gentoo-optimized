# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Functions Related to ICES Advice'
SRC_URI="http://cran.r-project.org/src/contrib/icesAdvice_1.4-2.tar.gz"
LICENSE='GPL-2+'
