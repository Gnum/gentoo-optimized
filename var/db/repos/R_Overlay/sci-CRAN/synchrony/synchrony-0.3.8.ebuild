# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Methods for Computing Spatial, T... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/synchrony_0.3.8.tar.gz"
LICENSE='GPL-2+'
