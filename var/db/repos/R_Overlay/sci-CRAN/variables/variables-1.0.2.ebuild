# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Variable Descriptions'
SRC_URI="http://cran.r-project.org/src/contrib/variables_1.0-2.tar.gz"
LICENSE='GPL-2'
