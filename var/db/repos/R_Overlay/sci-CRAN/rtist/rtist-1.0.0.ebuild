# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='A Color Palette Generator'
SRC_URI="http://cran.r-project.org/src/contrib/rtist_1.0.0.tar.gz"
LICENSE='MIT'
