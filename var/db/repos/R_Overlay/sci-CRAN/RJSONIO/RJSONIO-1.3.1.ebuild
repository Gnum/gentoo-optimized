# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Serialize R Objects to JSON, Jav... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/RJSONIO_1.3-1.tar.gz"
LICENSE='BSD'
