# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Non-Targeted Fluxomics on High-R... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/HiResTEC_0.54.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-BIOC/xcms
	sci-CRAN/beeswarm
	sci-CRAN/openxlsx
	sci-CRAN/InterpretMSSpectrum
	sci-BIOC/Biobase
	sci-CRAN/plyr
	sci-BIOC/Rdisop
"
RDEPEND="${DEPEND-}"
