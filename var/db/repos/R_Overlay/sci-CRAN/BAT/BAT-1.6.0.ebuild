# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Biodiversity Assessment Tools'
SRC_URI="http://cran.r-project.org/src/contrib/BAT_1.6.0.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/nls2
	sci-CRAN/hypervolume
	sci-CRAN/spatstat
	sci-CRAN/vegan
	>=dev-lang/R-3.0.0
	sci-CRAN/raster
"
RDEPEND="${DEPEND-}"
