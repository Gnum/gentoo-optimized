# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Generalized Quantitative Genetics Data Analyses'
SRC_URI="http://cran.r-project.org/src/contrib/qgtools_2.0.tar.gz"
LICENSE='GPL-2'
