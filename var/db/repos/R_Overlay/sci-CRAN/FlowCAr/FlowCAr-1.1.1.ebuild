# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Flow Network Construction and Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/FlowCAr_1.1.1.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_knitr"
R_SUGGESTS="r_suggests_knitr? ( sci-CRAN/knitr )"
DEPEND=">=dev-lang/R-3.5.0
	sci-CRAN/LIM
	sci-CRAN/limSolve
	sci-CRAN/enaR
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
