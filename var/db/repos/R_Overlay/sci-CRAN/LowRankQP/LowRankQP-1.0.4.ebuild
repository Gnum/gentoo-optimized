# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Low Rank Quadratic Programming'
SRC_URI="http://cran.r-project.org/src/contrib/LowRankQP_1.0.4.tar.gz"
LICENSE='GPL-2+'
