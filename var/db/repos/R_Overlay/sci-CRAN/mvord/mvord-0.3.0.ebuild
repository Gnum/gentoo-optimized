# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Multivariate Ordinal Regression Models'
SRC_URI="http://cran.r-project.org/src/contrib/mvord_0.3.0.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/numDeriv
	sci-CRAN/pbivnorm
	virtual/MASS
	sci-CRAN/mnormt
	virtual/Matrix
	sci-CRAN/optimx
	>=dev-lang/R-3.1.0
"
RDEPEND="${DEPEND-}"
