# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Person Fit'
SRC_URI="http://cran.r-project.org/src/contrib/PerFit_1.4.2.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/ltm
	virtual/MASS
	sci-CRAN/Hmisc
	sci-CRAN/fda
	virtual/Matrix
	sci-CRAN/mirt
	sci-CRAN/irtoys
"
RDEPEND="${DEPEND-}"
