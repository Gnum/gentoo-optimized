# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='GAMLSS Data'
SRC_URI="http://cran.r-project.org/src/contrib/gamlss.data_5.0-1.tar.gz"
LICENSE='|| ( GPL-2 GPL-3 )'
