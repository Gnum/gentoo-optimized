# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Genome and Transcriptome Wide Association Study'
SRC_URI="http://cran.r-project.org/src/contrib/gtWAS_1.1.0.tar.gz"
LICENSE='GPL-2+'
