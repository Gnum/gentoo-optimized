# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Tools for Working with XML Files as R Dataframes'
SRC_URI="http://cran.r-project.org/src/contrib/flatxml_0.0.1.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-omegahat/RCurl
	sci-CRAN/xml2
"
RDEPEND="${DEPEND-}"
