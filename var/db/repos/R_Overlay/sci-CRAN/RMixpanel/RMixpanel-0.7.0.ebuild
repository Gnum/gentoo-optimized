# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='API for Mixpanel'
SRC_URI="http://cran.r-project.org/src/contrib/RMixpanel_0.7-0.tar.gz"
LICENSE='MIT'

DEPEND=">=dev-lang/R-3.2.0
	sci-CRAN/jsonlite
	sci-omegahat/RCurl
	sci-CRAN/base64enc
	sci-CRAN/uuid
"
RDEPEND="${DEPEND-}"
