# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Forecasting Mortality, Fertility... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/demography_1.21.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=sci-CRAN/forecast-8.5
	virtual/mgcv
	sci-CRAN/rainbow
	sci-omegahat/RCurl
	>=dev-lang/R-3.4
	sci-CRAN/cobs
	>=sci-CRAN/ftsa-4.8
	sci-CRAN/strucchange
"
RDEPEND="${DEPEND-}"
