# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Functions and Datasets to Accomp... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/NSM3_1.12.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/SemiPar
	sci-CRAN/binom
	sci-CRAN/quantreg
	sci-CRAN/ash
	sci-CRAN/SuppDists
	sci-CRAN/waveslim
	sci-CRAN/epitools
	sci-CRAN/agricolae
	sci-CRAN/Hmisc
	virtual/survival
	sci-CRAN/Rfit
	sci-CRAN/gtools
	sci-CRAN/km_ci
	sci-CRAN/BSDA
	sci-CRAN/np
	sci-CRAN/partitions
	sci-CRAN/coin
	virtual/MASS
	sci-CRAN/nortest
	sci-CRAN/combinat
	sci-CRAN/fANCOVA
	sci-CRAN/metafor
"
RDEPEND="${DEPEND-}"
