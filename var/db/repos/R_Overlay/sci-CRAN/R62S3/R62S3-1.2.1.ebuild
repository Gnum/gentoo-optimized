# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Automatic S3 Method Generation from R6'
SRC_URI="http://cran.r-project.org/src/contrib/R62S3_1.2.1.tar.gz"
LICENSE='MIT'

IUSE="${IUSE-} r_suggests_pkgdown r_suggests_r6 r_suggests_testthat"
R_SUGGESTS="
	r_suggests_pkgdown? ( sci-CRAN/pkgdown )
	r_suggests_r6? ( sci-CRAN/R6 )
	r_suggests_testthat? ( sci-CRAN/testthat )
"
DEPEND=">=dev-lang/R-3.4.0
	sci-CRAN/checkmate
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
