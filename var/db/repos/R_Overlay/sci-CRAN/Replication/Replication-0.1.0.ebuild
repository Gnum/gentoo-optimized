# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Test Replications by Means of th... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/Replication_0.1.0.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/mice
	sci-CRAN/rjags
	sci-CRAN/blavaan
	virtual/MASS
	>=sci-CRAN/runjags-2.0.4.2
	>=sci-CRAN/lavaan-0.6.3
	sci-CRAN/quadprog
"
RDEPEND="${DEPEND-}"
