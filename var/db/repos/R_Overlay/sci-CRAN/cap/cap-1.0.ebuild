# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Covariate Assisted Principal (CA... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/cap_1.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="virtual/MASS
	sci-CRAN/multigroup
"
RDEPEND="${DEPEND-}"
