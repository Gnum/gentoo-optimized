# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Calculate the Standardized Diffe... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/stddiff_3.0.tar.gz"
LICENSE='GPL-3'
