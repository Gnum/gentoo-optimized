# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Downloads Tweets from a List of Tweet IDs'
SRC_URI="http://cran.r-project.org/src/contrib/rehydratoR_0.5.1.tar.gz"
LICENSE='BSD'

DEPEND=">=sci-CRAN/rtweet-0.6.7
	>=sci-CRAN/tibble-1.3.4
	>=sci-CRAN/dplyr-0.7.6
	>=sci-CRAN/jsonlite-1.5
"
RDEPEND="${DEPEND-}"
