# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='An R-Tool for Comprehensive Scie... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/bibliometrix_2.3.2.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_knitr r_suggests_rmarkdown r_suggests_testthat"
R_SUGGESTS="
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_testthat? ( >=sci-CRAN/testthat-2.1.0 )
"
DEPEND="sci-CRAN/stringr
	sci-CRAN/ggraph
	sci-CRAN/dplyr
	sci-CRAN/stringdist
	sci-CRAN/reshape2
	sci-CRAN/SnowballC
	sci-CRAN/shinycssloaders
	sci-CRAN/factoextra
	sci-CRAN/RColorBrewer
	virtual/Matrix
	sci-CRAN/shinythemes
	sci-CRAN/FactoMineR
	sci-CRAN/DT
	sci-CRAN/ggplot2
	sci-CRAN/igraph
	sci-CRAN/ggrepel
	sci-CRAN/rio
	sci-CRAN/RISmed
	sci-CRAN/rscopus
	sci-CRAN/networkD3
	sci-CRAN/shiny
	>=dev-lang/R-3.3.0
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
