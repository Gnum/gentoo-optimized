# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Algorithms for Pitman-Yor Process Mixtures'
SRC_URI="http://cran.r-project.org/src/contrib/BNPmix_0.1.1.tar.gz"
LICENSE='LGPL-3'

DEPEND=">=dev-lang/R-3.3.0
	sci-CRAN/ggplot2
"
RDEPEND="${DEPEND-}
	sci-CRAN/RcppArmadillo
	>=sci-CRAN/Rcpp-0.12.13
"
