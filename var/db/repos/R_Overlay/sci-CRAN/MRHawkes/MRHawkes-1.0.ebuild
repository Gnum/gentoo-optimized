# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Multivariate Renewal Hawkes Process'
SRC_URI="http://cran.r-project.org/src/contrib/MRHawkes_1.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/IHSEP"
RDEPEND="${DEPEND-}"
