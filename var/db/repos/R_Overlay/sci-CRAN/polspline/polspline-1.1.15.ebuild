# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Polynomial Spline Routines'
SRC_URI="http://cran.r-project.org/src/contrib/polspline_1.1.15.tar.gz"
LICENSE='GPL-2+'
