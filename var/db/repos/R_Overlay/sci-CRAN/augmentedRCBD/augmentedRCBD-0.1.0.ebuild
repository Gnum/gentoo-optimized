# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Analysis of Augmented Randomised... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/augmentedRCBD_0.1.0.tar.gz"
LICENSE='|| ( GPL-2 GPL-3 )'

DEPEND="sci-CRAN/emmeans
	sci-CRAN/dplyr
	sci-CRAN/stringi
	sci-CRAN/moments
	sci-CRAN/Rdpack
	sci-CRAN/reshape2
	sci-CRAN/multcomp
	sci-CRAN/officer
	sci-CRAN/flextable
	sci-CRAN/ggplot2
	>=dev-lang/R-3.0.1
	sci-CRAN/multcompView
"
RDEPEND="${DEPEND-}"
