# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='R Interface to the DieHarder RNG Test Suite'
SRC_URI="http://cran.r-project.org/src/contrib/RDieHarder_0.2.1.tar.gz"
LICENSE='GPL-2+'

RDEPEND="${DEPEND-} sci-libs/gsl"
