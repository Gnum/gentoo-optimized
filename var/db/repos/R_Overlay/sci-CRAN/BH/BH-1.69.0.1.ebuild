# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Boost C++ Header Files'
SRC_URI="http://cran.r-project.org/src/contrib/BH_1.69.0-1.tar.gz"
LICENSE='Boost-1.0'
