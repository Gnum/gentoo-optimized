# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Calculates Measures of Scholarly Impact'
SRC_URI="http://cran.r-project.org/src/contrib/RImpact_1.0.tar.gz"
LICENSE='MIT'
