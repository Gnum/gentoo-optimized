# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Infinite Mixtures of Infinite Fa... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/IMIFA_2.1.3.tar.gz"
LICENSE='GPL-2+'

IUSE="${IUSE-} r_suggests_gmp r_suggests_knitr r_suggests_mcclust
	r_suggests_rmarkdown r_suggests_rmpfr"
R_SUGGESTS="
	r_suggests_gmp? ( sci-CRAN/gmp )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_mcclust? ( sci-CRAN/mcclust )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_rmpfr? ( sci-CRAN/Rmpfr )
"
DEPEND=">=dev-lang/R-4.0.0
	virtual/Matrix
	>=sci-CRAN/mclust-5.1
	>=sci-CRAN/Rfast-1.9.8
	sci-CRAN/slam
	sci-CRAN/viridis
	sci-CRAN/mvnfast
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
