# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Plot Soccer Event Data'
SRC_URI="http://cran.r-project.org/src/contrib/ggsoccer_0.1.4.tar.gz"
LICENSE='MIT'

DEPEND=">=dev-lang/R-3.3.0
	sci-CRAN/ggplot2
"
RDEPEND="${DEPEND-}"
