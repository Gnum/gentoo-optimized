# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Constructing Hierarchical Vorono... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/muHVT_0.1.0.tar.gz"
LICENSE='|| ( GPL-2 GPL-3 )'

DEPEND="virtual/MASS
	sci-CRAN/plyr
	sci-CRAN/sp
	sci-CRAN/conf_design
	sci-CRAN/splancs
	sci-CRAN/deldir
	sci-CRAN/Hmisc
	>=dev-lang/R-3.1.0
"
RDEPEND="${DEPEND-}"
