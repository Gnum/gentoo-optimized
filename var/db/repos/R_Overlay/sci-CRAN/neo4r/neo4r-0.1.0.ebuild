# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='A Neo4J Driver'
SRC_URI="http://cran.r-project.org/src/contrib/neo4r_0.1.0.tar.gz"
LICENSE='MIT'

DEPEND="sci-CRAN/jsonlite
	sci-CRAN/httr
	sci-CRAN/rstudioapi
	sci-CRAN/attempt
	sci-CRAN/magrittr
	sci-CRAN/data_table
	sci-CRAN/glue
	sci-CRAN/purrr
	sci-CRAN/igraph
	sci-CRAN/tidyr
	sci-CRAN/tibble
	sci-CRAN/tidyselect
	sci-CRAN/R6
	sci-CRAN/shiny
	sci-CRAN/rlang
"
RDEPEND="${DEPEND-}"
