# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Lightweight but Feature Complete... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/tinytest_0.9.3.tar.gz"
LICENSE='GPL-3'
