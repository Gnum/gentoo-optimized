# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Spatial Management and Assessmen... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/smartR_0.62.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/ggthemes
	sci-CRAN/clues
	sci-CRAN/rgeos
	sci-CRAN/gstat
	sci-CRAN/chron
	sci-CRAN/mapdata
	sci-CRAN/gWidgets2RGtk2
	sci-CRAN/scales
	sci-CRAN/jsonlite
	sci-CRAN/plyr
	sci-CRAN/jpeg
	sci-CRAN/shape
	sci-CRAN/igraph
	virtual/foreign
	sci-CRAN/RColorBrewer
	sci-CRAN/mapproj
	sci-CRAN/rjags
	sci-CRAN/reshape2
	sci-CRAN/spdep
	sci-CRAN/nnls
	sci-CRAN/sqldf
	virtual/lattice
	sci-CRAN/caret
	sci-CRAN/ggrepel
	sci-CRAN/sp
	sci-CRAN/R6
	virtual/cluster
	sci-CRAN/gsubfn
	sci-CRAN/gridExtra
	sci-CRAN/mtsdi
	sci-CRAN/rgdal
	sci-CRAN/maptools
	sci-CRAN/marmap
	sci-CRAN/gWidgets2
	sci-CRAN/vegan
	virtual/rpart
	sci-CRAN/ggplot2
	sci-CRAN/ROCR
	sci-CRAN/PBSmapping
	sci-CRAN/maps
"
RDEPEND="${DEPEND-}"
