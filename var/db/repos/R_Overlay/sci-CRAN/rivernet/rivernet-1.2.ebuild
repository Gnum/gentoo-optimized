# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Read, Analyze and Plot River Networks'
SRC_URI="http://cran.r-project.org/src/contrib/rivernet_1.2.tar.gz"
LICENSE='GPL-3'
