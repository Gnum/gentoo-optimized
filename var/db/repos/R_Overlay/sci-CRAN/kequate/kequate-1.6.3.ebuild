# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='The Kernel Method of Test Equating'
SRC_URI="http://cran.r-project.org/src/contrib/kequate_1.6.3.tar.gz"
LICENSE='|| ( GPL-2 GPL-3 )'

DEPEND="sci-CRAN/mirt
	sci-CRAN/equateIRT
	>=dev-lang/R-2.11.0
	sci-CRAN/ltm
"
RDEPEND="${DEPEND-}"
