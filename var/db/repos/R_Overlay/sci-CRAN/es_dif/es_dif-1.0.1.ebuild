# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Compute Effect Sizes of the Difference'
SRC_URI="http://cran.r-project.org/src/contrib/es.dif_1.0.1.tar.gz"
LICENSE='MIT'
