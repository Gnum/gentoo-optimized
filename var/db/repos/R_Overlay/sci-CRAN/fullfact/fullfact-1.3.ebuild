# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Full Factorial Breeding Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/fullfact_1.3.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-3.6
	sci-CRAN/lme4
	sci-CRAN/afex
"
RDEPEND="${DEPEND-}"
