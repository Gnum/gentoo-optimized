# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Statistical Analysis of Chemistr... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/StatCharrms_0.90.91.tar.gz"
LICENSE='CC0-1.0'

DEPEND="virtual/nlme
	sci-CRAN/RGtk2
	virtual/lattice
	sci-CRAN/R2HTML
	sci-CRAN/clinfun
	>=dev-lang/R-3.1.0
	sci-CRAN/coxme
	sci-CRAN/cairoDevice
	sci-CRAN/gWidgetsRGtk2
	sci-CRAN/RSCABS
	sci-CRAN/multcomp
	virtual/survival
	sci-CRAN/car
	sci-CRAN/gWidgets
"
RDEPEND="${DEPEND-} x11-libs/gtk+"
