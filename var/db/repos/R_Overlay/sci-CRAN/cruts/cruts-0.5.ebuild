# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Interface to Climatic Research U... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/cruts_0.5.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/sp
	sci-CRAN/ncdf4
	sci-CRAN/lubridate
	sci-CRAN/raster
	>=dev-lang/R-3.2.1
	sci-CRAN/stringr
"
RDEPEND="${DEPEND-}"
