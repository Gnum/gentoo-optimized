# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Flexible Tools for Multiple Imputation'
SRC_URI="http://cran.r-project.org/src/contrib/Conigrave_0.3.0.tar.gz"
LICENSE='GPL-3'

DEPEND=">=sci-CRAN/ggplot2-2.1.0
	>=sci-CRAN/mitools-2.3
	>=sci-CRAN/miceadds-1.8.0
	>=sci-CRAN/stringdist-0.9.4.7
	>=sci-CRAN/magrittr-1.5
"
RDEPEND="${DEPEND-}"
