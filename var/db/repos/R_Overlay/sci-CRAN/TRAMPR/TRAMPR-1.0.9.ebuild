# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='TRFLP Analysis and Matching Package for R'
SRC_URI="http://cran.r-project.org/src/contrib/TRAMPR_1.0-9.tar.gz"
LICENSE='GPL-2'
