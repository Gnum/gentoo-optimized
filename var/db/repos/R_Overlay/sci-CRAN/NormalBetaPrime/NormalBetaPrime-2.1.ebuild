# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Normal Beta Prime Prior'
SRC_URI="http://cran.r-project.org/src/contrib/NormalBetaPrime_2.1.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/HyperbolicDist
	>=dev-lang/R-3.1.0
	virtual/Matrix
	sci-CRAN/glmnet
	sci-CRAN/pracma
	virtual/MASS
	sci-CRAN/truncnorm
	sci-CRAN/pscl
	sci-CRAN/GIGrvg
"
RDEPEND="${DEPEND-}"
