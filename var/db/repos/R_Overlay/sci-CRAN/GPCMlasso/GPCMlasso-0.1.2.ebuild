# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Differential Item Functioning in... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/GPCMlasso_0.1-2.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=sci-CRAN/Rcpp-0.12.4
	sci-CRAN/ltm
	sci-CRAN/TeachingDemos
	sci-CRAN/mirt
	sci-CRAN/caret
	sci-CRAN/mvtnorm
	sci-CRAN/statmod
	sci-CRAN/cubature
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
"
