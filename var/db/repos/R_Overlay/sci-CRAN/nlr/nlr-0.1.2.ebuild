# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Nonlinear Regression Modelling u... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/nlr_0.1-2.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/robcor
	sci-CRAN/GA
	virtual/nlme
	virtual/MASS
	sci-CRAN/tseries
	sci-CRAN/TSA
	sci-CRAN/quantreg
	>=dev-lang/R-3.6.0
"
RDEPEND="${DEPEND-}"
