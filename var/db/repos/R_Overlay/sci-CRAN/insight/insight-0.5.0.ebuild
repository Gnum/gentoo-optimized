# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Easy Access to Model Information... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/insight_0.5.0.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_aer r_suggests_afex r_suggests_aod
	r_suggests_bayesfactor r_suggests_bayestestr r_suggests_betareg
	r_suggests_biglm r_suggests_blme r_suggests_brms r_suggests_censreg
	r_suggests_covr r_suggests_coxme r_suggests_crch r_suggests_estimatr
	r_suggests_feisr r_suggests_gam r_suggests_gamlss r_suggests_gamm4
	r_suggests_gbm r_suggests_gee r_suggests_geepack
	r_suggests_glmmadaptive r_suggests_glmmtmb r_suggests_gmnl
	r_suggests_hrqol r_suggests_httr r_suggests_knitr r_suggests_lfe
	r_suggests_lme4 r_suggests_logistf r_suggests_mass
	r_suggests_mcmcglmm r_suggests_mgcv r_suggests_mlogit
	r_suggests_multgee r_suggests_nlme r_suggests_nnet r_suggests_ordinal
	r_suggests_panelr r_suggests_plm r_suggests_pscl r_suggests_quantreg
	r_suggests_rmarkdown r_suggests_rms r_suggests_robust
	r_suggests_robustbase r_suggests_robustlmm r_suggests_rstanarm
	r_suggests_rstudioapi r_suggests_speedglm r_suggests_spelling
	r_suggests_survey r_suggests_survival r_suggests_testthat
	r_suggests_truncreg r_suggests_vgam"
R_SUGGESTS="
	r_suggests_aer? ( sci-CRAN/AER )
	r_suggests_afex? ( sci-CRAN/afex )
	r_suggests_aod? ( sci-CRAN/aod )
	r_suggests_bayesfactor? ( sci-CRAN/BayesFactor )
	r_suggests_bayestestr? ( sci-CRAN/bayestestR )
	r_suggests_betareg? ( sci-CRAN/betareg )
	r_suggests_biglm? ( sci-CRAN/biglm )
	r_suggests_blme? ( sci-CRAN/blme )
	r_suggests_brms? ( sci-CRAN/brms )
	r_suggests_censreg? ( sci-CRAN/censReg )
	r_suggests_covr? ( sci-CRAN/covr )
	r_suggests_coxme? ( sci-CRAN/coxme )
	r_suggests_crch? ( sci-CRAN/crch )
	r_suggests_estimatr? ( sci-CRAN/estimatr )
	r_suggests_feisr? ( sci-CRAN/feisr )
	r_suggests_gam? ( sci-CRAN/gam )
	r_suggests_gamlss? ( sci-CRAN/gamlss )
	r_suggests_gamm4? ( sci-CRAN/gamm4 )
	r_suggests_gbm? ( sci-CRAN/gbm )
	r_suggests_gee? ( sci-CRAN/gee )
	r_suggests_geepack? ( sci-CRAN/geepack )
	r_suggests_glmmadaptive? ( sci-CRAN/GLMMadaptive )
	r_suggests_glmmtmb? ( sci-CRAN/glmmTMB )
	r_suggests_gmnl? ( sci-CRAN/gmnl )
	r_suggests_hrqol? ( sci-CRAN/HRQoL )
	r_suggests_httr? ( sci-CRAN/httr )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_lfe? ( sci-CRAN/lfe )
	r_suggests_lme4? ( sci-CRAN/lme4 )
	r_suggests_logistf? ( sci-CRAN/logistf )
	r_suggests_mass? ( virtual/MASS )
	r_suggests_mcmcglmm? ( sci-CRAN/MCMCglmm )
	r_suggests_mgcv? ( virtual/mgcv )
	r_suggests_mlogit? ( sci-CRAN/mlogit )
	r_suggests_multgee? ( sci-CRAN/multgee )
	r_suggests_nlme? ( virtual/nlme )
	r_suggests_nnet? ( virtual/nnet )
	r_suggests_ordinal? ( sci-CRAN/ordinal )
	r_suggests_panelr? ( sci-CRAN/panelr )
	r_suggests_plm? ( sci-CRAN/plm )
	r_suggests_pscl? ( sci-CRAN/pscl )
	r_suggests_quantreg? ( sci-CRAN/quantreg )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_rms? ( sci-CRAN/rms )
	r_suggests_robust? ( sci-CRAN/robust )
	r_suggests_robustbase? ( sci-CRAN/robustbase )
	r_suggests_robustlmm? ( sci-CRAN/robustlmm )
	r_suggests_rstanarm? ( sci-CRAN/rstanarm )
	r_suggests_rstudioapi? ( sci-CRAN/rstudioapi )
	r_suggests_speedglm? ( sci-CRAN/speedglm )
	r_suggests_spelling? ( sci-CRAN/spelling )
	r_suggests_survey? ( sci-CRAN/survey )
	r_suggests_survival? ( virtual/survival )
	r_suggests_testthat? ( sci-CRAN/testthat )
	r_suggests_truncreg? ( sci-CRAN/truncreg )
	r_suggests_vgam? ( sci-CRAN/VGAM )
"
DEPEND=">=dev-lang/R-3.0"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
