# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Easy Access to Model Information... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/insight_0.1.2.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_aer r_suggests_betareg r_suggests_brms
	r_suggests_coxme r_suggests_gam r_suggests_gamm4 r_suggests_gee
	r_suggests_glmmadaptive r_suggests_glmmtmb r_suggests_gmnl
	r_suggests_lfe r_suggests_lme4 r_suggests_mass r_suggests_mcmcglmm
	r_suggests_mgcv r_suggests_mlogit r_suggests_nlme r_suggests_nnet
	r_suggests_ordinal r_suggests_plm r_suggests_pscl r_suggests_rstanarm
	r_suggests_survey r_suggests_survival r_suggests_testthat
	r_suggests_truncreg r_suggests_vgam"
R_SUGGESTS="
	r_suggests_aer? ( sci-CRAN/AER )
	r_suggests_betareg? ( sci-CRAN/betareg )
	r_suggests_brms? ( sci-CRAN/brms )
	r_suggests_coxme? ( sci-CRAN/coxme )
	r_suggests_gam? ( sci-CRAN/gam )
	r_suggests_gamm4? ( sci-CRAN/gamm4 )
	r_suggests_gee? ( sci-CRAN/gee )
	r_suggests_glmmadaptive? ( sci-CRAN/GLMMadaptive )
	r_suggests_glmmtmb? ( sci-CRAN/glmmTMB )
	r_suggests_gmnl? ( sci-CRAN/gmnl )
	r_suggests_lfe? ( sci-CRAN/lfe )
	r_suggests_lme4? ( sci-CRAN/lme4 )
	r_suggests_mass? ( virtual/MASS )
	r_suggests_mcmcglmm? ( sci-CRAN/MCMCglmm )
	r_suggests_mgcv? ( virtual/mgcv )
	r_suggests_mlogit? ( sci-CRAN/mlogit )
	r_suggests_nlme? ( virtual/nlme )
	r_suggests_nnet? ( virtual/nnet )
	r_suggests_ordinal? ( sci-CRAN/ordinal )
	r_suggests_plm? ( sci-CRAN/plm )
	r_suggests_pscl? ( sci-CRAN/pscl )
	r_suggests_rstanarm? ( sci-CRAN/rstanarm )
	r_suggests_survey? ( sci-CRAN/survey )
	r_suggests_survival? ( virtual/survival )
	r_suggests_testthat? ( sci-CRAN/testthat )
	r_suggests_truncreg? ( sci-CRAN/truncreg )
	r_suggests_vgam? ( sci-CRAN/VGAM )
"
DEPEND=">=dev-lang/R-3.0"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
