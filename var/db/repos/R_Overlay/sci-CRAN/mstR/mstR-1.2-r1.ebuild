# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Procedures to Generate Patterns ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/mstR_1.2.tar.gz -> mstR_1.2-r1.tar.gz"
LICENSE='GPL-2+'
