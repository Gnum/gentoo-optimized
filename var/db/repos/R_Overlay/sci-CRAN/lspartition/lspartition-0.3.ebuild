# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Nonparametric Estimation and Inf... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/lspartition_0.3.tar.gz"
LICENSE='GPL-2'

DEPEND="virtual/MASS
	>=dev-lang/R-3.1
	sci-CRAN/pracma
	virtual/Matrix
	sci-CRAN/ggplot2
	virtual/mgcv
	sci-CRAN/combinat
	sci-CRAN/dplyr
"
RDEPEND="${DEPEND-}"
