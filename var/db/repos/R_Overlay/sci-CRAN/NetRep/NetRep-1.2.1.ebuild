# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Permutation Testing Network Modu... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/NetRep_1.2.1.tar.gz"
LICENSE='GPL-2'

IUSE="${IUSE-} r_suggests_bigmemory r_suggests_knitr r_suggests_rmarkdown
	r_suggests_testthat"
R_SUGGESTS="
	r_suggests_bigmemory? ( sci-CRAN/bigmemory )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_testthat? ( sci-CRAN/testthat )
"
DEPEND="sci-CRAN/statmod
	>=dev-lang/R-3.0.2
	sci-CRAN/foreach
	sci-CRAN/RhpcBLASctl
	sci-CRAN/abind
	sci-CRAN/RColorBrewer
	>=sci-CRAN/Rcpp-0.11
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/BH
	>=sci-CRAN/RcppArmadillo-0.4
	virtual/mpi
	${R_SUGGESTS-}
"
