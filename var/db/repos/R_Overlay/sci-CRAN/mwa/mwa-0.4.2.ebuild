# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Causal Inference in Spatiotemporal Event Data'
SRC_URI="http://cran.r-project.org/src/contrib/mwa_0.4.2.tar.gz"
LICENSE='LGPL-3'

DEPEND=">=sci-CRAN/cem-0.3
	>=sci-CRAN/rJava-0.9
	virtual/MASS
"
RDEPEND="${DEPEND-} virtual/jdk"
