# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Cross-Recurrence Quantification ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/crqa_1.0.8.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/tseriesChaos
	>=dev-lang/R-3.0.0
	virtual/Matrix
	sci-CRAN/fields
	sci-CRAN/plot3D
	sci-CRAN/pracma
"
RDEPEND="${DEPEND-}"
