# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Audio Interface for R'
SRC_URI="http://cran.r-project.org/src/contrib/audio_0.1-5.1.tar.gz"
LICENSE='MIT'
