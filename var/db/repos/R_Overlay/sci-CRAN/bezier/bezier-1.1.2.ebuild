# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Toolkit for Bezier Curves and Splines'
SRC_URI="http://cran.r-project.org/src/contrib/bezier_1.1.2.tar.gz"
LICENSE='GPL-2+'
