# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Q-Kernel-Based and Conditionally... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/qkerntool_1.19.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-3.0.1
	virtual/class
"
RDEPEND="${DEPEND-}"
