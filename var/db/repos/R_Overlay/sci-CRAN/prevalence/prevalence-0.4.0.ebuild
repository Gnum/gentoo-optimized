# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Tools for Prevalence Assessment Studies'
SRC_URI="http://cran.r-project.org/src/contrib/prevalence_0.4.0.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-3.0.0
	sci-CRAN/rjags
	sci-CRAN/coda
"
RDEPEND="${DEPEND-} sci-mathematics/jags"
