# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Containerize R Markdown Document... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/liftr_0.9.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/yaml
	sci-CRAN/rmarkdown
	sci-CRAN/knitr
	>=dev-lang/R-3.0.2
	sci-CRAN/rstudioapi
	sci-CRAN/stringr
"
RDEPEND="${DEPEND-}"
