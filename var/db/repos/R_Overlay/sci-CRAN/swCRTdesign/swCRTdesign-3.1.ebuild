# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Stepped Wedge Cluster Randomized... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/swCRTdesign_3.1.tar.gz"
LICENSE='GPL-2'
