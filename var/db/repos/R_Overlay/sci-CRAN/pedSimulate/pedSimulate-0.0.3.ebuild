# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Pedigree, Genetic Merit and Phenotype Simulation'
SRC_URI="http://cran.r-project.org/src/contrib/pedSimulate_0.0.3.tar.gz"
LICENSE='GPL-3'
