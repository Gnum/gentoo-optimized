# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Robust Estimation for Compositional Data'
SRC_URI="http://cran.r-project.org/src/contrib/robCompositions_2.0.9.tar.gz"
LICENSE='GPL-2+'

IUSE="${IUSE-} r_suggests_knitr"
R_SUGGESTS="r_suggests_knitr? ( sci-CRAN/knitr )"
DEPEND="sci-CRAN/robustbase
	sci-CRAN/VIM
	sci-CRAN/ggplot2
	sci-CRAN/fpc
	sci-CRAN/pls
	sci-CRAN/e1071
	sci-CRAN/mclust
	virtual/MASS
	virtual/cluster
	sci-CRAN/cvTools
	sci-CRAN/rrcov
	sci-CRAN/data_table
	sci-CRAN/GGally
	sci-CRAN/car
	sci-CRAN/Rcpp
	>=dev-lang/R-3.0.0
	sci-CRAN/sROC
	sci-CRAN/zCompositions
	sci-CRAN/kernlab
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	${R_SUGGESTS-}
"
