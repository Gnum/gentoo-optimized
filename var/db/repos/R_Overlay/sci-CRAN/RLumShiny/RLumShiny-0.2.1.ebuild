# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Shiny Applications for the R Package Luminescence'
SRC_URI="http://cran.r-project.org/src/contrib/RLumShiny_0.2.1.tar.gz"
LICENSE='GPL-3'

DEPEND=">=dev-lang/R-3.4.0
	>=sci-CRAN/Luminescence-0.7.3
	>=sci-CRAN/rhandsontable-0.3.4
	>=sci-CRAN/readxl-1.0.0
	>=sci-CRAN/knitr-1.20
	>=sci-CRAN/DT-0.4
	>=sci-CRAN/data_table-1.10.4
	>=sci-CRAN/shinydashboard-0.5.3
	>=sci-CRAN/googleVis-0.6.2
	>=sci-CRAN/shiny-1.0.5
"
RDEPEND="${DEPEND-}"
