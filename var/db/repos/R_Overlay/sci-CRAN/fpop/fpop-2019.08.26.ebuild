# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Segmentation using Optimal Parti... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/fpop_2019.08.26.tar.gz"
LICENSE='LGPL-2.1+'
