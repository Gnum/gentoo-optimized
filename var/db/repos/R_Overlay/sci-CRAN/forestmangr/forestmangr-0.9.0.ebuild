# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Functions for Forest Mensuration and Management'
SRC_URI="http://cran.r-project.org/src/contrib/forestmangr_0.9.0.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/tidyr
	sci-CRAN/rlang
	>=dev-lang/R-3.3
	sci-CRAN/htmltools
	sci-CRAN/ggpmisc
	sci-CRAN/plyr
	sci-CRAN/rmarkdown
	sci-CRAN/broom
	>=sci-CRAN/ggplot2-2.0
	sci-CRAN/covr
	>=sci-CRAN/scales-0.4.1
	>=sci-CRAN/dplyr-0.7.0
	sci-CRAN/ggthemes
	sci-CRAN/curl
	sci-CRAN/tibble
	sci-CRAN/psych
	sci-CRAN/systemfit
	sci-CRAN/purrr
	sci-CRAN/car
	sci-CRAN/magrittr
	sci-CRAN/minpack_lm
"
RDEPEND="${DEPEND-}"
