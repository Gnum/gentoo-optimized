# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Test and Item Analysis via Shiny'
SRC_URI="http://cran.r-project.org/src/contrib/ShinyItemAnalysis_1.3.0.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/xtable
	virtual/nnet
	>=sci-CRAN/difR-5.0
	sci-CRAN/ltm
	>=sci-CRAN/difNLR-1.2.2
	sci-CRAN/deltaPlotR
	sci-CRAN/msm
	sci-CRAN/ggdendro
	sci-CRAN/corrplot
	sci-CRAN/knitr
	sci-CRAN/plotly
	sci-CRAN/gridExtra
	sci-CRAN/psych
	sci-CRAN/CTT
	sci-CRAN/psychometric
	sci-CRAN/data_table
	sci-CRAN/rmarkdown
	>=sci-CRAN/ggplot2-2.2.1
	sci-CRAN/cowplot
	>=sci-CRAN/shiny-1.0.3
	>=dev-lang/R-3.1
	virtual/lattice
	sci-CRAN/shinyBS
	sci-CRAN/reshape2
	sci-CRAN/shinydashboard
	sci-CRAN/moments
	>=sci-CRAN/mirt-1.24
	>=sci-CRAN/shinyjs-0.9
	sci-CRAN/DT
	sci-CRAN/stringr
"
RDEPEND="${DEPEND-}"
