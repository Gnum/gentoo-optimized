# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Test and Item Analysis via Shiny'
SRC_URI="http://cran.r-project.org/src/contrib/ShinyItemAnalysis_1.2.8.tar.gz"
LICENSE='GPL-3'

DEPEND=">=sci-CRAN/shiny-1.0.3
	sci-CRAN/knitr
	sci-CRAN/plotly
	sci-CRAN/shinydashboard
	sci-CRAN/reshape2
	sci-CRAN/corrplot
	>=sci-CRAN/shinyjs-0.9
	sci-CRAN/cowplot
	sci-CRAN/data_table
	sci-CRAN/msm
	>=dev-lang/R-3.1
	>=sci-CRAN/difNLR-1.2.2
	virtual/lattice
	virtual/nnet
	>=sci-CRAN/mirt-1.24
	>=sci-CRAN/difR-5.0
	sci-CRAN/ltm
	sci-CRAN/psychometric
	sci-CRAN/gridExtra
	sci-CRAN/stringr
	sci-CRAN/moments
	sci-CRAN/psych
	>=sci-CRAN/ggplot2-2.2.1
	sci-CRAN/DT
	sci-CRAN/deltaPlotR
	sci-CRAN/rmarkdown
	sci-CRAN/xtable
	sci-CRAN/CTT
	sci-CRAN/shinyBS
"
RDEPEND="${DEPEND-}"
