# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Spatial Analysis and Modelling Utilities'
SRC_URI="http://cran.r-project.org/src/contrib/spatialEco_1.1-1.tar.gz"
LICENSE='GPL-3'

DEPEND="virtual/MASS
	sci-CRAN/sp
	sci-CRAN/spatstat
	sci-omegahat/RCurl
	sci-CRAN/raster
	>=dev-lang/R-3.4.0
	sci-CRAN/readr
	sci-CRAN/SDMTools
	sci-CRAN/RANN
	sci-CRAN/yaImpute
	virtual/cluster
	sci-CRAN/rms
	virtual/spatial
	sci-CRAN/maptools
	sci-CRAN/sf
	sci-CRAN/EnvStats
	sci-CRAN/spdep
	sci-CRAN/rgeos
"
RDEPEND="${DEPEND-}"
