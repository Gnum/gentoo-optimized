# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Linear Mixed Models'
SRC_URI="http://cran.r-project.org/src/contrib/lmm_1.2.tar.gz"
