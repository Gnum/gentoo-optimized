# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Robust Testing in GLMs'
SRC_URI="http://cran.r-project.org/src/contrib/flipscores_0.2.tar.gz"
LICENSE='GPL-2+'
