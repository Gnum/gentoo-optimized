# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Yet Another Canonical Correlation Analysis Package'
SRC_URI="http://cran.r-project.org/src/contrib/yacca_1.1.1.tar.gz"
LICENSE='GPL-3+'
