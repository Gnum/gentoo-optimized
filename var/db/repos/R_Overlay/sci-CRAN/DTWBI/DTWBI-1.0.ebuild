# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Imputation of Time Series Based ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/DTWBI_1.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/dtw
	sci-CRAN/entropy
	>=dev-lang/R-3.0.0
	sci-CRAN/lsa
	sci-CRAN/rlist
	sci-CRAN/e1071
"
RDEPEND="${DEPEND-}"
