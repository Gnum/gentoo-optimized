# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Computerized Adaptive Testing and Decision Trees'
SRC_URI="http://cran.r-project.org/src/contrib/cat.dt_0.1.0.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-mathematics/glpk
	virtual/Matrix
"
RDEPEND="${DEPEND-}"
