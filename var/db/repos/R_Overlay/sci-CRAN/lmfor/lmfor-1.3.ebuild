# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Functions for Forest Biometrics'
SRC_URI="http://cran.r-project.org/src/contrib/lmfor_1.3.tar.gz"
LICENSE='GPL-2'

IUSE="${IUSE-} r_suggests_lme4"
R_SUGGESTS="r_suggests_lme4? ( sci-CRAN/lme4 )"
DEPEND="sci-CRAN/spatstat
	virtual/nlme
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
