# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='R Interface to RNG with Multiple Streams'
SRC_URI="http://cran.r-project.org/src/contrib/rlecuyer_0.3-5.tar.gz"
LICENSE='GPL-2+'
