# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Interim Monitoring Using Adaptiv... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/YPInterimTesting_1.0.3.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/Rcpp
	virtual/MASS
"
RDEPEND="${DEPEND-} sci-CRAN/Rcpp"
