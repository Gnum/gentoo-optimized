# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Uniformly Most Powerful Invarian... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/equivUMP_0.1.1.tar.gz"
LICENSE='GPL-2+'
