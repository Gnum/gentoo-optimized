# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Tools for Units of Measurement'
SRC_URI="http://cran.r-project.org/src/contrib/measurements_1.3.0.tar.gz"
LICENSE='GPL-3'
