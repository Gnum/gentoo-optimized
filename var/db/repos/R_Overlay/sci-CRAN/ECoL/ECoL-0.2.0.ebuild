# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Complexity Measures for Supervised Problems'
SRC_URI="http://cran.r-project.org/src/contrib/ECoL_0.2.0.tar.gz"
LICENSE='MIT'

DEPEND="virtual/cluster
	sci-CRAN/e1071
	sci-CRAN/FNN
	virtual/MASS
	sci-CRAN/igraph
"
RDEPEND="${DEPEND-}"
