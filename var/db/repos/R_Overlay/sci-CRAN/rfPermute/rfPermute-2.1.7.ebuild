# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Estimate Permutation p-Values fo... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/rfPermute_2.1.7.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/randomForest
	>=dev-lang/R-3.2.0
	sci-CRAN/abind
	sci-CRAN/tidyr
	sci-CRAN/gridExtra
	sci-CRAN/tibble
	sci-CRAN/scales
	sci-CRAN/ggplot2
	sci-CRAN/dplyr
	sci-CRAN/rlang
	sci-CRAN/plyr
	sci-CRAN/magrittr
	>=sci-CRAN/swfscMisc-1.1.1
	sci-CRAN/reshape2
"
RDEPEND="${DEPEND-}"
