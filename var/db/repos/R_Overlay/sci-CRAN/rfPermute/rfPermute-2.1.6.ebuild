# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Estimate Permutation p-Values fo... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/rfPermute_2.1.6.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/dplyr
	>=dev-lang/R-3.2.0
	sci-CRAN/tidyr
	sci-CRAN/scales
	sci-CRAN/viridis
	>=sci-CRAN/swfscMisc-1.1.1
	sci-CRAN/ggplot2
	sci-CRAN/plyr
	sci-CRAN/magrittr
	sci-CRAN/rlang
	sci-CRAN/randomForest
	sci-CRAN/gridExtra
	sci-CRAN/tibble
	sci-CRAN/abind
	sci-CRAN/reshape2
"
RDEPEND="${DEPEND-}"
