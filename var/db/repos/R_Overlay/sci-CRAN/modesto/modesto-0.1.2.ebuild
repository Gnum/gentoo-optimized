# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Modeling and Analysis of Stochastic Systems'
SRC_URI="http://cran.r-project.org/src/contrib/modesto_0.1.2.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_testthat"
R_SUGGESTS="r_suggests_testthat? ( sci-CRAN/testthat )"
DEPEND="sci-CRAN/markovchain"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
