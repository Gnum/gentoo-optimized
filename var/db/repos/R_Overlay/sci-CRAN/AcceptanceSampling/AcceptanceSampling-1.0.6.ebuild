# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Creation and Evaluation of Accep... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/AcceptanceSampling_1.0-6.tar.gz"
LICENSE='GPL-3+'
