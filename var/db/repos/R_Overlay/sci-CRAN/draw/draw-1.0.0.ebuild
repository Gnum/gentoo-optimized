# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Wrapper Functions for Producing Graphics'
SRC_URI="http://cran.r-project.org/src/contrib/draw_1.0.0.tar.gz"
LICENSE='MIT'
