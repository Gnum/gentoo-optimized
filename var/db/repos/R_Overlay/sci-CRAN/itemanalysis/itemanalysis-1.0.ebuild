# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Classical Test Theory Item Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/itemanalysis_1.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/ggplot2
	sci-CRAN/polycor
	sci-CRAN/car
"
RDEPEND="${DEPEND-}"
