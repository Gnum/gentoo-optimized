# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Interface for the RobinHood.com ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/RobinHood_1.0.7.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/lubridate
	sci-CRAN/profvis
	sci-CRAN/httr
	sci-CRAN/jsonlite
	sci-CRAN/dplyr
	sci-CRAN/curl
	sci-CRAN/magrittr
"
RDEPEND="${DEPEND-}"
