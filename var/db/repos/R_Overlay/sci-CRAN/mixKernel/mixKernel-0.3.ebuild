# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Omics Data Integration Using Kernel Methods'
SRC_URI="http://cran.r-project.org/src/contrib/mixKernel_0.3.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/vegan
	sci-CRAN/LDRTools
	sci-CRAN/corrplot
	sci-CRAN/ggplot2
	virtual/Matrix
	sci-BIOC/phyloseq
	sci-CRAN/mixOmics
	sci-CRAN/quadprog
	sci-CRAN/psych
"
RDEPEND="${DEPEND-}"
