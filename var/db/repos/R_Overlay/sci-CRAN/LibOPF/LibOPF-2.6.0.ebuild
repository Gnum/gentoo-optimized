# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Design of Optimum-Path Forest Classifiers'
SRC_URI="http://cran.r-project.org/src/contrib/LibOPF_2.6.0.tar.gz"
LICENSE='BSD-2'
