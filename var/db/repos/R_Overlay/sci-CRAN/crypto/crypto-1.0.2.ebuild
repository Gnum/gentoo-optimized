# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Cryptocurrency Market Data'
SRC_URI="http://cran.r-project.org/src/contrib/crypto_1.0.2.tar.gz"
LICENSE='MIT'

DEPEND="sci-CRAN/foreach
	sci-CRAN/rvest
	sci-CRAN/doSNOW
	sci-CRAN/magrittr
	>=dev-lang/R-3.4.0
	sci-CRAN/lubridate
	sci-CRAN/curl
	sci-CRAN/yaml
	sci-CRAN/dplyr
	sci-CRAN/jsonlite
	sci-CRAN/tidyr
	sci-CRAN/xml2
	sci-CRAN/xts
"
RDEPEND="${DEPEND-}"
