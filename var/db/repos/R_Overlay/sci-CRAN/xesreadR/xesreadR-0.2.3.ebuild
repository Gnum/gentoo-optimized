# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Read and Write XES Files'
SRC_URI="http://cran.r-project.org/src/contrib/xesreadR_0.2.3.tar.gz"
LICENSE='MIT'

IUSE="${IUSE-} r_suggests_knitr r_suggests_rmarkdown"
R_SUGGESTS="
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
"
DEPEND="sci-CRAN/dplyr
	sci-omegahat/XML
	sci-CRAN/tidyr
	>=dev-lang/R-3.0.0
	sci-CRAN/data_table
	sci-CRAN/bupaR
	sci-CRAN/purrr
	sci-CRAN/xml2
	sci-CRAN/stringr
	sci-CRAN/lubridate
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
