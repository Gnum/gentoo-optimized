# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Adaptive Weights Smoothing'
SRC_URI="http://cran.r-project.org/src/contrib/aws_2.2-0.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-3.2.0
	>=sci-CRAN/awsMethods-1.0.1
	sci-CRAN/gsl
"
RDEPEND="${DEPEND-}"
