# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Assessing Essential Unidimension... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/unival_1.0.1.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/psych
	sci-CRAN/optimbase
"
RDEPEND="${DEPEND-}"
