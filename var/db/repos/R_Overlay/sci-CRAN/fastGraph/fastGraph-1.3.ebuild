# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Fast Drawing and Shading of Grap... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/fastGraph_1.3.tar.gz"
LICENSE='GPL-3'
