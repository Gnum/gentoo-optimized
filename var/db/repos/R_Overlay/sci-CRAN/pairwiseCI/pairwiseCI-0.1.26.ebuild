# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Confidence Intervals for Two Sample Comparisons'
SRC_URI="http://cran.r-project.org/src/contrib/pairwiseCI_0.1-26.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/coin
	virtual/boot
	sci-CRAN/mcprofile
	sci-CRAN/MCPAN
	virtual/MASS
"
RDEPEND="${DEPEND-}"
