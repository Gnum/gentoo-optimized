# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Fit Sparse Linear Regression Mod... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/sparsenet_1.3.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/glmnet
	virtual/Matrix
	sci-CRAN/shape
"
RDEPEND="${DEPEND-}"
