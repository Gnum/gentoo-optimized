# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Tools for Grid-Based Survey Sampling Design'
SRC_URI="http://cran.r-project.org/src/contrib/gridsample_0.2.1.tar.gz"
LICENSE='MIT'

IUSE="${IUSE-} r_suggests_knitr r_suggests_rmarkdown"
R_SUGGESTS="
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
"
DEPEND=">=sci-CRAN/spatstat-1.49.0
	>=sci-CRAN/rgdal-1.2.4
	>=sci-CRAN/raster-2.5.8
	sci-CRAN/spatstat_utils
	>=sci-CRAN/data_table-1.10.4
	>=sci-CRAN/maptools-0.8.41
	>=sci-CRAN/sp-1.2.4
	>=sci-CRAN/geosphere-1.5.5
	>=sci-CRAN/rgeos-0.3.21
	>=dev-lang/R-3.2.3
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
