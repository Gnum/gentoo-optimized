# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Matrix Reconstruction from a Few Entries'
SRC_URI="http://cran.r-project.org/src/contrib/ROptSpace_0.2.1.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/Rdpack"
RDEPEND="${DEPEND-}"
