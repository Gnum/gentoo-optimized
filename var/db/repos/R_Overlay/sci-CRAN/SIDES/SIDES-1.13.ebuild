# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Subgroup Identification Based on... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/SIDES_1.13.tar.gz"
LICENSE='GPL-3'

DEPEND="virtual/nnet
	>=dev-lang/R-3.1.2
	virtual/MASS
	>=sci-CRAN/doParallel-1.0.10
	>=sci-CRAN/foreach-1.4.3
	>=sci-CRAN/memoise-1.0.0
	virtual/survival
	>=sci-CRAN/multicool-0.1.9
"
RDEPEND="${DEPEND-}"
