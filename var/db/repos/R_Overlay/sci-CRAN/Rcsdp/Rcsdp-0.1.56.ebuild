# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='R Interface to the CSDP Semidefi... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/Rcsdp_0.1.56.tar.gz"
LICENSE='CPL-1.0'
