# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Relevance-Integrated Statistical Inference Engine'
SRC_URI="http://cran.r-project.org/src/contrib/LPRelevance_1.0.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/ggplot2
	sci-CRAN/leaps
	sci-CRAN/BayesGOF
	sci-CRAN/Bolstad2
	sci-CRAN/reshape2
	sci-CRAN/locfdr
	>=dev-lang/R-3.5.0
	sci-CRAN/polynom
"
RDEPEND="${DEPEND-}"
