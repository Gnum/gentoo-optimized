# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Subtracting Summary Statistics o... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/MetaSubtract_1.41.tar.gz"
LICENSE='GPL-3+'
