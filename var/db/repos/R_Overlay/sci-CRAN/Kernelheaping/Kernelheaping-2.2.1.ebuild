# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Kernel Density Estimation for He... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/Kernelheaping_2.2.1.tar.gz"
LICENSE='|| ( GPL-2 GPL-3 )'

DEPEND="sci-CRAN/mvtnorm
	sci-CRAN/sp
	sci-CRAN/ks
	>=dev-lang/R-2.15.0
	sci-CRAN/fastmatch
	sci-CRAN/sparr
	virtual/MASS
	sci-CRAN/plyr
	sci-CRAN/magrittr
"
RDEPEND="${DEPEND-}"
