# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Methods and Measures for Brain, ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/NetworkToolbox_1.2.1.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/psych
	virtual/Matrix
	sci-CRAN/doParallel
	sci-CRAN/corrplot
	sci-CRAN/R_matlab
	sci-CRAN/igraph
	sci-CRAN/fdrtool
	sci-CRAN/ppcor
	sci-CRAN/qgraph
	sci-CRAN/pwr
	virtual/MASS
	>=dev-lang/R-3.3.0
	sci-CRAN/foreach
"
RDEPEND="${DEPEND-}"
