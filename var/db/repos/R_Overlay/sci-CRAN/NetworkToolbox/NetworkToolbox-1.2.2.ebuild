# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Methods and Measures for Brain, ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/NetworkToolbox_1.2.2.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/fdrtool
	virtual/Matrix
	sci-CRAN/psych
	sci-CRAN/igraph
	sci-CRAN/corrplot
	sci-CRAN/qgraph
	virtual/MASS
	sci-CRAN/ppcor
	sci-CRAN/foreach
	sci-CRAN/R_matlab
	sci-CRAN/doParallel
	>=dev-lang/R-3.3.0
	sci-CRAN/pwr
"
RDEPEND="${DEPEND-}"
