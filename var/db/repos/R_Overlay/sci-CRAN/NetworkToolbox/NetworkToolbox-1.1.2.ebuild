# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Methods and Measures for Brain, ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/NetworkToolbox_1.1.2.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/doSNOW
	sci-CRAN/igraph
	sci-CRAN/IsingFit
	sci-CRAN/R_matlab
	virtual/Matrix
	sci-CRAN/corrplot
	sci-CRAN/foreach
	sci-CRAN/ppcor
	sci-CRAN/fdrtool
	sci-CRAN/psych
	>=dev-lang/R-3.3.0
	sci-CRAN/pwr
	virtual/MASS
	sci-CRAN/qgraph
"
RDEPEND="${DEPEND-}"
