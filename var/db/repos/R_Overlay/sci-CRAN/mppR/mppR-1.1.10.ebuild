# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Multi-Parent Population QTL Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/mppR_1.1.10.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/qtl
	sci-CRAN/ggplot2
	sci-CRAN/igraph
	>=dev-lang/R-3.1.0
	sci-CRAN/synbreed
"
RDEPEND="${DEPEND-}"
