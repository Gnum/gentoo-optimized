# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Prais-Winsten Estimator for AR(1... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/prais_1.0.0.tar.gz"
LICENSE='GPL-2'

DEPEND=">=dev-lang/R-3.2.0"
RDEPEND="${DEPEND-}"
