# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Functions for the Book An Introd... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/bootstrap_2019.6.tar.gz"
LICENSE='BSD'
