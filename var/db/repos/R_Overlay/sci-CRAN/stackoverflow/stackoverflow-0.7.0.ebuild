# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Stack Overflows Greatest Hits'
SRC_URI="http://cran.r-project.org/src/contrib/stackoverflow_0.7.0.tar.gz"
