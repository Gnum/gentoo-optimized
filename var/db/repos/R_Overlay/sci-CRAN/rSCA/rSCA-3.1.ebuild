# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='An R Package for Stepwise Cluster Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/rSCA_3.1.tar.gz"
LICENSE='GPL-2+'
