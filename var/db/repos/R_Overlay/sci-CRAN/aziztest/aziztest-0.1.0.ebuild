# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Novel Statistical Test for Aberration Enrichment'
SRC_URI="http://cran.r-project.org/src/contrib/aziztest_0.1.0.tar.gz"
LICENSE='GPL-3'
