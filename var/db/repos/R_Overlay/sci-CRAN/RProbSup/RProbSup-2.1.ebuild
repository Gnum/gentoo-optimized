# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Calculates Probability of Superiority'
SRC_URI="http://cran.r-project.org/src/contrib/RProbSup_2.1.tar.gz"
LICENSE='MIT'
