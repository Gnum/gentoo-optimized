# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Verbal Autopsy Data Transform fo... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/CrossVA_0.9.3.tar.gz"
LICENSE='GPL-3'

DEPEND=">=dev-lang/R-3.2.0
	sci-CRAN/lubridate
	sci-CRAN/stringi
"
RDEPEND="${DEPEND-}"
