# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Handling of Data from the Clinic... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/secuTrialR_1.0.3.tar.gz"
LICENSE='MIT'

IUSE="${IUSE-} r_suggests_knitr r_suggests_lintr r_suggests_rmarkdown
	r_suggests_testthat r_suggests_tufte"
R_SUGGESTS="
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_lintr? ( sci-CRAN/lintr )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_testthat? ( sci-CRAN/testthat )
	r_suggests_tufte? ( sci-CRAN/tufte )
"
DEPEND="sci-CRAN/dplyr
	sci-CRAN/stringr
	sci-CRAN/readr
	dev-lang/R[tk]
	sci-CRAN/tibble
	>=sci-CRAN/haven-2.2.0
	sci-CRAN/lubridate
	sci-CRAN/igraph
	>=dev-lang/R-3.5
	sci-CRAN/magrittr
	sci-CRAN/readxl
	sci-CRAN/purrr
	sci-CRAN/tidyr
	sci-CRAN/rlang
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
