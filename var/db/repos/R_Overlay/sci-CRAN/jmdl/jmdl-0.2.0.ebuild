# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Joint Mean-Correlation Regressio... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/jmdl_0.2.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/abind
	>=dev-lang/R-3.4.0
	sci-CRAN/Formula
	virtual/MASS
	sci-CRAN/mnormt
	virtual/boot
	sci-CRAN/mvtnorm
	sci-CRAN/minqa
"
RDEPEND="${DEPEND-}"
