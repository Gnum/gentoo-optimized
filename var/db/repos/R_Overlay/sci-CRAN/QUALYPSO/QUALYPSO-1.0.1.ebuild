# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Partitioning Uncertainty Compone... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/QUALYPSO_1.0.1.tar.gz"
LICENSE='GPL-3'

DEPEND="virtual/MASS
	sci-CRAN/foreach
	sci-CRAN/Rfast
	sci-CRAN/doParallel
	sci-CRAN/expm
"
RDEPEND="${DEPEND-}"
