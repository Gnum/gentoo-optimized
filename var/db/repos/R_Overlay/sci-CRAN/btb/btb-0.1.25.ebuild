# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Beyond the Border'
SRC_URI="http://cran.r-project.org/src/contrib/btb_0.1.25.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=sci-CRAN/Rcpp-0.11.3
	sci-CRAN/RcppParallel
	>=dev-lang/R-3.3.0
	sci-CRAN/sp
	sci-CRAN/sf
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppParallel
	>=sci-CRAN/BH-1.60.0.1
	sci-CRAN/RcppArmadillo
"
