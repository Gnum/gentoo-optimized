# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Optimal Design Emulators via Point Processes'
SRC_URI="http://cran.r-project.org/src/contrib/demu_0.2.0.tar.gz"
LICENSE='AGPL-3'

DEPEND="virtual/Matrix
	>=sci-CRAN/Rcpp-0.12.12
	>=dev-lang/R-3.2.3
	virtual/cluster
	sci-CRAN/fields
	sci-CRAN/spam
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
"
