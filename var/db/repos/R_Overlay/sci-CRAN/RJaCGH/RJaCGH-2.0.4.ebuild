# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Reversible Jump MCMC for the Ana... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/RJaCGH_2.0.4.tar.gz"
LICENSE='GPL-3'

DEPEND=">=dev-lang/R-2.13"
RDEPEND="${DEPEND-}"
