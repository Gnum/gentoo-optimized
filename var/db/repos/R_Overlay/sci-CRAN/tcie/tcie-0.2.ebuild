# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Topologically Correct Isosurface Extraction'
SRC_URI="http://cran.r-project.org/src/contrib/tcie_0.2.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-3.2.3
	>=sci-CRAN/nat-1.8.11
	>=sci-CRAN/rgl-0.99.9
	>=sci-CRAN/geomorph-3.0.5
	>=sci-CRAN/Rvcg-0.17
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
"
