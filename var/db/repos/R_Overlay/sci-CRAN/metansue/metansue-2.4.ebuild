# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Meta-Analysis of Studies with No... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/metansue_2.4.tar.gz"
LICENSE='GPL-3'
