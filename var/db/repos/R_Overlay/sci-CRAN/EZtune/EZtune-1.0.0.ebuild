# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Tunes AdaBoost, Support Vector M... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/EZtune_1.0.0.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/gbm
	sci-CRAN/e1071
	sci-CRAN/ada
	>=dev-lang/R-3.1.0
	sci-CRAN/doParallel
	sci-CRAN/mlbench
	sci-CRAN/GA
"
RDEPEND="${DEPEND-}"
