# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Bayesian Regularization for Feed... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/brnn_0.7.tar.gz"
LICENSE='GPL-2'

DEPEND=">=dev-lang/R-3.1.2
	sci-CRAN/Formula
"
RDEPEND="${DEPEND-}"
