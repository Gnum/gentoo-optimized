# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Functions for Handling Dates'
SRC_URI="http://cran.r-project.org/src/contrib/date_1.2-39.tar.gz"
LICENSE='GPL-2'
