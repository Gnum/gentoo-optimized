# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Integrated Tool for Hairping Ext... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/HextractoR_1.2.tar.gz"
LICENSE='Apache-2.0'

DEPEND="sci-CRAN/seqinr
	sci-CRAN/doParallel
	sci-CRAN/foreach
"
RDEPEND="${DEPEND-}"
