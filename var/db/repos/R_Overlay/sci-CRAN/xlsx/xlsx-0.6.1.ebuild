# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Read, Write, Format Excel 2007 a... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/xlsx_0.6.1.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/rJava
	sci-CRAN/xlsxjars
"
RDEPEND="${DEPEND-} virtual/jdk"
