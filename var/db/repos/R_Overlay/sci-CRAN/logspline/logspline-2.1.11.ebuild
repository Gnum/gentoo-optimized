# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Routines for Logspline Density Estimation'
SRC_URI="http://cran.r-project.org/src/contrib/logspline_2.1.11.tar.gz"
LICENSE='Apache-2.0'
