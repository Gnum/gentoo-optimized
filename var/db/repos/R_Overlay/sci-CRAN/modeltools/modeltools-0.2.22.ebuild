# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Tools and Classes for Statistical Models'
SRC_URI="http://cran.r-project.org/src/contrib/modeltools_0.2-22.tar.gz"
LICENSE='GPL-2'
