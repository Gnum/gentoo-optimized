# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Local Projections Impulse Response Functions'
SRC_URI="http://cran.r-project.org/src/contrib/lpirfs_0.1.6.tar.gz"
LICENSE='GPL-2+'

IUSE="${IUSE-} r_suggests_httr r_suggests_knitr r_suggests_quantmod
	r_suggests_readxl r_suggests_rmarkdown r_suggests_testthat
	r_suggests_vars r_suggests_zoo"
R_SUGGESTS="
	r_suggests_httr? ( sci-CRAN/httr )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_quantmod? ( sci-CRAN/quantmod )
	r_suggests_readxl? ( sci-CRAN/readxl )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_testthat? ( sci-CRAN/testthat )
	r_suggests_vars? ( sci-CRAN/vars )
	r_suggests_zoo? ( sci-CRAN/zoo )
"
DEPEND=">=sci-CRAN/dplyr-0.7.4
	>=dev-lang/R-3.1.2
	>=sci-CRAN/gridExtra-2.3
	>=sci-CRAN/lmtest-0.9.36
	>=sci-CRAN/ggplot2-2.2.1
	>=sci-CRAN/ggpubr-0.2.3
	>=sci-CRAN/doParallel-1.0.11
	>=sci-CRAN/foreach-1.4.3
	>=sci-CRAN/plm-1.6.6
	>=sci-CRAN/sandwich-2.5.0
	>=sci-CRAN/Rcpp-0.12.17
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
	${R_SUGGESTS-}
"
