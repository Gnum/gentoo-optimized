# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Threshold Model and Unit Root Te... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/pdR_1.6.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/coefplot
	virtual/boot
	sci-CRAN/lmtest
	sci-CRAN/sandwich
	dev-lang/R[tk]
	sci-CRAN/car
	sci-CRAN/papeR
	sci-CRAN/plm
"
RDEPEND="${DEPEND-}"
