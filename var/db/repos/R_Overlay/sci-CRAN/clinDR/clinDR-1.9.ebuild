# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Simulation and Analysis Tools fo... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/clinDR_1.9.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/foreach
	>=dev-lang/R-3.0.1
	sci-CRAN/DoseFinding
	>=sci-CRAN/rstan-2.12
	sci-CRAN/ggplot2
	sci-CRAN/doParallel
"
RDEPEND="${DEPEND-}"
