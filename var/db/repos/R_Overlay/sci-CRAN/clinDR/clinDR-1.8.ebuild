# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Simulation and Analysis Tools fo... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/clinDR_1.8.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/ggplot2
	sci-CRAN/foreach
	>=sci-CRAN/rstan-2.12
	sci-CRAN/doParallel
	>=dev-lang/R-3.0.1
	sci-CRAN/DoseFinding
"
RDEPEND="${DEPEND-}"
