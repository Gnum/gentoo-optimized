# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Paleontological and Phylogenetic... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/paleotree_3.1.0.tar.gz"
LICENSE='CC0-1.0'

DEPEND=">=sci-CRAN/ape-4.1
	>=sci-CRAN/phangorn-2.0.0
	>=sci-CRAN/phytools-0.6.00
	>=dev-lang/R-3.0.0
"
RDEPEND="${DEPEND-}"
