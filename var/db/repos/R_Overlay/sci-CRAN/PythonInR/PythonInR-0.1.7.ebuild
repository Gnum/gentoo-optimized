# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Use Python from Within R'
SRC_URI="http://cran.r-project.org/src/contrib/PythonInR_0.1-7.tar.gz"
LICENSE='GPL-3'

DEPEND=">=dev-lang/R-2.15
	sci-CRAN/pack
	sci-CRAN/R6
"
RDEPEND="${DEPEND-} dev-lang/python"
