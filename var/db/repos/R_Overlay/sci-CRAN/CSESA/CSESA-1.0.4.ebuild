# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='CRISPR-Based Salmonella Enterica Serotype Analyzer'
SRC_URI="http://cran.r-project.org/src/contrib/CSESA_1.0.4.tar.gz"
LICENSE='GPL-2+'

IUSE="${IUSE-} r_suggests_testthat"
R_SUGGESTS="r_suggests_testthat? ( sci-CRAN/testthat )"
DEPEND="sci-BIOC/Biostrings"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
