# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Functions to Compute Composition... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/zetadiv_1.1.1.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/vegan
	sci-CRAN/car
	sci-CRAN/Imap
	sci-CRAN/scam
	sci-CRAN/nnls
	>=dev-lang/R-3.0.0
	virtual/mgcv
	sci-CRAN/glm2
"
RDEPEND="${DEPEND-}"
