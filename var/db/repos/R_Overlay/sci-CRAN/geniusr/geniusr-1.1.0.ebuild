# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Tools for Working with the Genius API'
SRC_URI="http://cran.r-project.org/src/contrib/geniusr_1.1.0.tar.gz"
LICENSE='MIT'

DEPEND="sci-CRAN/httr
	sci-CRAN/curl
	sci-CRAN/tibble
	sci-CRAN/rvest
	sci-CRAN/xml2
	sci-CRAN/attempt
	sci-CRAN/purrr
	sci-CRAN/stringr
	>=dev-lang/R-3.2.0
"
RDEPEND="${DEPEND-}"
