# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Spatial Variable Selection'
SRC_URI="http://cran.r-project.org/src/contrib/SpatialVS_1.0.tar.gz"
LICENSE='GPL-2'

DEPEND=">=dev-lang/R-3.3.0
	virtual/MASS
	virtual/nlme
	sci-CRAN/fields
"
RDEPEND="${DEPEND-}"
