# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Utility Functions for Exploratory Factor Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/EFAutilities_1.2.3.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/GPArotation
	sci-CRAN/mvtnorm
	sci-CRAN/plyr
"
RDEPEND="${DEPEND-}"
