# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Various Plotting Functions'
SRC_URI="http://cran.r-project.org/src/contrib/plotrix_3.7-5.tar.gz"
LICENSE='GPL-2+'
