# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Parametric Estimation and Sensit... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/sensmediation_0.2.0.tar.gz"
LICENSE='GPL-2'

DEPEND=">=dev-lang/R-3.5.0
	>=sci-CRAN/maxLik-1.3.4
	>=sci-CRAN/mvtnorm-1.0.8
"
RDEPEND="${DEPEND-}"
