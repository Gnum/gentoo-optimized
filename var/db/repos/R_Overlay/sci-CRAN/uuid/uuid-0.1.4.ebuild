# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Tools for Generating and Handling of UUIDs'
SRC_URI="http://cran.r-project.org/src/contrib/uuid_0.1-4.tar.gz"
LICENSE='MIT'
