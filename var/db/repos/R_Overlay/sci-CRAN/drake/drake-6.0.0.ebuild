# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='A Pipeline Toolkit for Reproduci... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/drake_6.0.0.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_abind r_suggests_bindr r_suggests_callr
	r_suggests_cluster r_suggests_codedepends r_suggests_crayon
	r_suggests_dbi r_suggests_downloader r_suggests_future_apply
	r_suggests_ggplot2 r_suggests_ggraph r_suggests_knitr
	r_suggests_lubridate r_suggests_mass r_suggests_networkd3
	r_suggests_prettycode r_suggests_rmarkdown r_suggests_rprojroot
	r_suggests_rsqlite r_suggests_styler r_suggests_testthat
	r_suggests_txtq r_suggests_visnetwork"
R_SUGGESTS="
	r_suggests_abind? ( sci-CRAN/abind )
	r_suggests_bindr? ( sci-CRAN/bindr )
	r_suggests_callr? ( sci-CRAN/callr )
	r_suggests_cluster? ( virtual/cluster )
	r_suggests_codedepends? ( sci-omegahat/CodeDepends )
	r_suggests_crayon? ( sci-CRAN/crayon )
	r_suggests_dbi? ( sci-CRAN/DBI )
	r_suggests_downloader? ( sci-CRAN/downloader )
	r_suggests_future_apply? ( sci-CRAN/future_apply )
	r_suggests_ggplot2? ( sci-CRAN/ggplot2 )
	r_suggests_ggraph? ( sci-CRAN/ggraph )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_lubridate? ( sci-CRAN/lubridate )
	r_suggests_mass? ( virtual/MASS )
	r_suggests_networkd3? ( sci-CRAN/networkD3 )
	r_suggests_prettycode? ( sci-CRAN/prettycode )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_rprojroot? ( sci-CRAN/rprojroot )
	r_suggests_rsqlite? ( sci-CRAN/RSQLite )
	r_suggests_styler? ( sci-CRAN/styler )
	r_suggests_testthat? ( sci-CRAN/testthat )
	r_suggests_txtq? ( sci-CRAN/txtq )
	r_suggests_visnetwork? ( sci-CRAN/visNetwork )
"
DEPEND="sci-CRAN/formatR
	sci-CRAN/magrittr
	>=sci-CRAN/storr-1.1.0
	sci-CRAN/igraph
	sci-CRAN/future
	virtual/codetools
	sci-CRAN/purrr
	sci-CRAN/evaluate
	sci-CRAN/R_utils
	sci-CRAN/withr
	sci-CRAN/R6
	sci-CRAN/tibble
	sci-CRAN/fs
	sci-CRAN/digest
	>=sci-CRAN/tidyselect-0.2.4
	sci-CRAN/stringi
	sci-CRAN/pkgconfig
	>=dev-lang/R-3.3.0
	sci-CRAN/dplyr
	>=sci-CRAN/rlang-0.2.0
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"

_UNRESOLVED_PACKAGES=( 'sci-CRAN/webshot' )
