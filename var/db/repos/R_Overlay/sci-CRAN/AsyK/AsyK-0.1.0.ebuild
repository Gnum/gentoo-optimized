# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Density Estimation by Using Asymmetrical Kernels'
SRC_URI="http://cran.r-project.org/src/contrib/AsyK_0.1.0.tar.gz"
LICENSE='GPL-2'
