# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Longitudinal Cascade'
SRC_URI="http://cran.r-project.org/src/contrib/longitudinalcascade_0.1.1.1.tar.gz"
LICENSE='MIT'

DEPEND="sci-CRAN/dplyr
	virtual/survival
	sci-CRAN/rlang
	sci-CRAN/zoo
	sci-CRAN/ggplot2
	sci-CRAN/scales
	sci-CRAN/tidyr
"
RDEPEND="${DEPEND-}"
