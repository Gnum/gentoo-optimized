# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Longitudinal Cascade'
SRC_URI="http://cran.r-project.org/src/contrib/longitudinalcascade_0.3.1.1.tar.gz"
LICENSE='MIT'

DEPEND="sci-CRAN/lubridate
	sci-CRAN/dplyr
	sci-CRAN/rlang
	virtual/survival
	sci-CRAN/tidyr
	sci-CRAN/zoo
	sci-CRAN/scales
	sci-CRAN/ggplot2
"
RDEPEND="${DEPEND-}"
