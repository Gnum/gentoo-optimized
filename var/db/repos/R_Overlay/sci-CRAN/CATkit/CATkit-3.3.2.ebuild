# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Chronomics Analysis Toolkit (CAT... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/CATkit_3.3.2.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/Hmisc
	sci-CRAN/CombMSC
	sci-CRAN/assertr
	sci-CRAN/magic
	sci-CRAN/png
	sci-CRAN/rtf
	sci-CRAN/season
	virtual/MASS
	sci-CRAN/signal
"
RDEPEND="${DEPEND-}"
