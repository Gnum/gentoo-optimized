# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Psychometric Networks for Intens... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/psychNET_0.0.1.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/mgm
	sci-CRAN/bigtime
	sci-CRAN/imputeTS
	sci-CRAN/vars
	sci-CRAN/qgraph
	virtual/MASS
	>=dev-lang/R-3.5
	sci-CRAN/glmnet
	sci-CRAN/networktools
	sci-CRAN/SparseTSCGM
	sci-CRAN/igraph
	sci-CRAN/longitudinal
	sci-CRAN/mlVAR
	sci-CRAN/car
	sci-CRAN/fastDummies
	virtual/Matrix
	sci-CRAN/Hmisc
	sci-CRAN/gtools
	sci-CRAN/crayon
	sci-CRAN/graphicalVAR
	sci-CRAN/sparsevar
	sci-CRAN/ordinalNet
"
RDEPEND="${DEPEND-}"
