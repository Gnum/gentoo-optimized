# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Bayesian Analysis of Quantile Regression Models'
SRC_URI="http://cran.r-project.org/src/contrib/Brq_2.4.tar.gz"
LICENSE='GPL-2+'
