# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Univariate GARCH Models'
SRC_URI="http://cran.r-project.org/src/contrib/rugarch_1.4-1.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/zoo
	sci-CRAN/numDeriv
	sci-CRAN/Rsolnp
	sci-CRAN/Rcpp
	>=dev-lang/R-3.0.2
	sci-CRAN/chron
	sci-CRAN/SkewHyperbolic
	sci-CRAN/expm
	sci-CRAN/spd
	sci-CRAN/xts
	sci-CRAN/ks
	sci-CRAN/nloptr
"
RDEPEND="${DEPEND-}
	>=sci-CRAN/Rcpp-0.10.6
	>=sci-CRAN/RcppArmadillo-0.2.34
"
