# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Creates and Manages Folder Funct... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/folderfun_0.1.2.tar.gz"
LICENSE='BSD-2'

IUSE="${IUSE-} r_suggests_knitr r_suggests_testthat"
R_SUGGESTS="
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_testthat? ( sci-CRAN/testthat )
"
DEPEND=">dev-lang/R-3.0"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
