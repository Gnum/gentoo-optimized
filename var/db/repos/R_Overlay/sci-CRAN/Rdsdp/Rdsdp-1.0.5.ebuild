# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='R Interface to DSDP Semidefinite... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/Rdsdp_1.0.5.tar.gz"
LICENSE='GPL-3'
