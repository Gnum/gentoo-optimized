# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Data on Base Packages for Curren... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/rcheology_3.5.2.0.tar.gz"
LICENSE='CC0-1.0'
