# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Bayesian Network Structure Learning'
SRC_URI="http://cran.r-project.org/src/contrib/BNSL_0.1.4.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/bnlearn
	sci-CRAN/igraph
	>=sci-CRAN/Rcpp-0.12.0
"
RDEPEND="${DEPEND-} sci-CRAN/Rcpp"
