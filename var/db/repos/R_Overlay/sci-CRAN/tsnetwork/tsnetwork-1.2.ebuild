# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Constructing Dynamic Networks for Time-Series Data'
SRC_URI="http://cran.r-project.org/src/contrib/tsnetwork_1.2.tar.gz"
LICENSE='GPL-3+'

DEPEND=">=dev-lang/R-3.3.2
	sci-CRAN/QUIC
	sci-CRAN/igraph
	sci-CRAN/longitudinal
"
RDEPEND="${DEPEND-}"
