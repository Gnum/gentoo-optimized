# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Functions for Set-Theoretic Mult... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/SetMethods_2.4.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/ggplot2
	>=dev-lang/R-3.3.0
	sci-CRAN/QCA
	sci-CRAN/scatterplot3d
	virtual/lattice
	sci-CRAN/ggrepel
	sci-CRAN/betareg
	sci-CRAN/fmsb
"
RDEPEND="${DEPEND-}"
