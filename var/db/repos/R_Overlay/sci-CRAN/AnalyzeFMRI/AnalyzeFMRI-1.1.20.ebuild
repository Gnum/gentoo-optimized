# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Functions for Analysis of fMRI D... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/AnalyzeFMRI_1.1-20.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-3.6.0
	sci-CRAN/R_matlab
	sci-CRAN/fastICA
	dev-lang/R[tk]
	sci-CRAN/tkrplot
"
RDEPEND="${DEPEND-}"
