# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='LP Nonparametric High Dimensiona... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/LPKsample_2.0.tar.gz"
LICENSE='GPL-2'

DEPEND="virtual/cluster
	sci-CRAN/igraph
	sci-CRAN/mclust
"
RDEPEND="${DEPEND-}"
