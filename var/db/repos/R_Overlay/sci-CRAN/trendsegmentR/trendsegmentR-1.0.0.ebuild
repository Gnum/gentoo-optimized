# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Linear Trend Segmentation and Po... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/trendsegmentR_1.0.0.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-3.6.0"
RDEPEND="${DEPEND-}"
