# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Monotonic Optimal Binning'
SRC_URI="http://cran.r-project.org/src/contrib/mob_0.1.0.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-3.6.0
	sci-CRAN/gbm
	sci-CRAN/Rborist
	sci-CRAN/Hmisc
"
RDEPEND="${DEPEND-}"
