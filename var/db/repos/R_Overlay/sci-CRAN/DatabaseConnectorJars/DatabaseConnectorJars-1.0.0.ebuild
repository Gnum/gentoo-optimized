# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='JAR Dependencies for the DatabaseConnector Package'
SRC_URI="http://cran.r-project.org/src/contrib/DatabaseConnectorJars_1.0.0.tar.gz"

DEPEND="sci-CRAN/rJava"
RDEPEND="${DEPEND-}"
