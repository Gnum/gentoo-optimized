# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Pathwise Calibrated Sparse Shooting Algorithm'
SRC_URI="http://cran.r-project.org/src/contrib/picasso_1.3.1.tar.gz"
LICENSE='GPL-3'

DEPEND=">=dev-lang/R-2.15.0
	virtual/MASS
	virtual/Matrix
"
RDEPEND="${DEPEND-}"
