# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Tools for Tensor Analysis and Decomposition'
SRC_URI="http://cran.r-project.org/src/contrib/rTensor_1.4.tar.gz"
LICENSE='GPL-2+'
