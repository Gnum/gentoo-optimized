# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Image Data Used in the Loon Package Demos'
SRC_URI="http://cran.r-project.org/src/contrib/RnavGraphImageData_0.0.4.tar.gz"
LICENSE='GPL-2'
