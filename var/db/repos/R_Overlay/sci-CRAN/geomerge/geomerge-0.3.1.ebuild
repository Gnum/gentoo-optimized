# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Geospatial Data Integration'
SRC_URI="http://cran.r-project.org/src/contrib/geomerge_0.3.1.tar.gz"
LICENSE='LGPL-3'

DEPEND="sci-CRAN/inlmisc
	sci-CRAN/lubridate
	sci-CRAN/gridExtra
	sci-CRAN/geosphere
	sci-CRAN/sp
	sci-CRAN/ggplot2
	sci-CRAN/raster
	sci-CRAN/spdep
	sci-CRAN/scales
"
RDEPEND="${DEPEND-}"
