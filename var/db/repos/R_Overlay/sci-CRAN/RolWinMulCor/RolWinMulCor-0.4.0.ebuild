# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Subroutines to Estimate Rolling ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/RolWinMulCor_0.4.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/colorspace
	sci-CRAN/pracma
	>=dev-lang/R-3.5.0
	sci-CRAN/zoo
	sci-CRAN/gtools
"
RDEPEND="${DEPEND-}"
