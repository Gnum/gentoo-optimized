# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Colour Schemes for Archaeologica... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/khroma_1.1.0.tar.gz"
LICENSE='GPL-3'

DEPEND=">=dev-lang/R-3.4
	>=sci-CRAN/ggplot2-3.0.0
	sci-CRAN/scales
"
RDEPEND="${DEPEND-}"
