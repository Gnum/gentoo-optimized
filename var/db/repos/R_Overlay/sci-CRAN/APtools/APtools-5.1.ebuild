# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Average Positive Predictive Valu... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/APtools_5.1.tar.gz"
LICENSE='LGPL-3'

DEPEND="virtual/survival
	sci-CRAN/cmprsk
"
RDEPEND="${DEPEND-}"
