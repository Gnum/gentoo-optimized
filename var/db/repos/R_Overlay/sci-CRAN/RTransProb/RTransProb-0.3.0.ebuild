# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Analyze and Forecast Credit Migrations'
SRC_URI="http://cran.r-project.org/src/contrib/RTransProb_0.3.0.tar.gz"
LICENSE='GPL-2'

DEPEND="virtual/Matrix
	dev-lang/R[tk]
	sci-CRAN/caret
	virtual/MASS
	virtual/nnet
	sci-CRAN/neuralnet
	virtual/Matrix
	sci-CRAN/chron
	sci-CRAN/zoo
	sci-CRAN/expm
	>=dev-lang/R-3.5.0
	sci-CRAN/pracma
	sci-CRAN/e1071
	>=sci-CRAN/Rcpp-0.12.11
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
"
