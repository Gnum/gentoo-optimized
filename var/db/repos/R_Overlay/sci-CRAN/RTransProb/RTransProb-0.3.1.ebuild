# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Analyze and Forecast Credit Migrations'
SRC_URI="http://cran.r-project.org/src/contrib/RTransProb_0.3.1.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/neuralnet
	sci-CRAN/chron
	virtual/Matrix
	>=sci-CRAN/Rcpp-0.12.11
	sci-CRAN/expm
	dev-lang/R[tk]
	virtual/Matrix
	sci-CRAN/e1071
	virtual/nnet
	sci-CRAN/zoo
	virtual/MASS
	sci-CRAN/caret
	sci-CRAN/pracma
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
"
