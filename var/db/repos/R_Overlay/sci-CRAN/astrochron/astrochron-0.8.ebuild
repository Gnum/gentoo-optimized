# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='A Computational Tool for Astrochronology'
SRC_URI="http://cran.r-project.org/src/contrib/astrochron_0.8.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/foreach
	sci-CRAN/multitaper
	sci-CRAN/doParallel
	sci-CRAN/IDPmisc
	sci-CRAN/fields
	sci-CRAN/iterators
"
RDEPEND="${DEPEND-}"
