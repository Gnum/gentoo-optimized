# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Detection of Univariate Outliers'
SRC_URI="http://cran.r-project.org/src/contrib/univOutl_0.2.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/robustbase
	sci-CRAN/Hmisc
"
RDEPEND="${DEPEND-}"
