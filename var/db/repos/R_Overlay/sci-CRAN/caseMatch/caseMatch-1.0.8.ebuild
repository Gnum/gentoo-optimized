# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Identify Similar Cases for Quali... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/caseMatch_1.0.8.tar.gz"
LICENSE='GPL-2+'
