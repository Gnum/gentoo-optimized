# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Tropical Forest Gaps Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/ForestGapR_0.0.1.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/igraph
	sci-CRAN/VGAM
	sci-CRAN/raster
	sci-CRAN/viridis
	>=dev-lang/R-3.4.0
	sci-CRAN/sp
"
RDEPEND="${DEPEND-}"
