# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Robust Regression with Data Driv... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/rlmDataDriven_0.2.1.tar.gz"
LICENSE='GPL-2+'

DEPEND="virtual/MASS
	sci-CRAN/tseries
"
RDEPEND="${DEPEND-}"
