# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Variant Quality Investigation Helper'
SRC_URI="http://cran.r-project.org/src/contrib/vanquish_1.0.0.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/changepoint
	sci-CRAN/VGAM
	>=dev-lang/R-3.4.0
	sci-CRAN/ggplot2
	sci-CRAN/e1071
"
RDEPEND="${DEPEND-}"
