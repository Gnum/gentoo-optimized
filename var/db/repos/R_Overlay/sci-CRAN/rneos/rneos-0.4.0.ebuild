# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='XML-RPC Interface to NEOS'
SRC_URI="http://cran.r-project.org/src/contrib/rneos_0.4-0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-omegahat/RCurl
	sci-omegahat/XML
"
RDEPEND="${DEPEND-}"
