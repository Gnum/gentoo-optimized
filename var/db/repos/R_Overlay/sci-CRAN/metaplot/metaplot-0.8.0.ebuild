# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Data-Driven Plot Design'
SRC_URI="http://cran.r-project.org/src/contrib/metaplot_0.8.0.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_csv r_suggests_nlme"
R_SUGGESTS="
	r_suggests_csv? ( sci-CRAN/csv )
	r_suggests_nlme? ( virtual/nlme )
"
DEPEND=">=sci-CRAN/encode-0.3.4
	sci-CRAN/rlang
	sci-CRAN/ggplot2
	sci-CRAN/gtable
	sci-CRAN/scales
	sci-CRAN/gridExtra
	sci-CRAN/tidyr
	sci-CRAN/magrittr
	virtual/lattice
	>=sci-CRAN/dplyr-0.7.1
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
