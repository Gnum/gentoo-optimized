# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='A Tool for Downloading Functiona... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/TR8_0.9.19.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/plyr
	sci-omegahat/RCurl
	sci-omegahat/XML
	sci-CRAN/gWidgets
	sci-CRAN/reshape
	sci-CRAN/taxize
	sci-CRAN/rappdirs
	sci-CRAN/readxl
	dev-lang/R[tk]
"
RDEPEND="${DEPEND-}"
