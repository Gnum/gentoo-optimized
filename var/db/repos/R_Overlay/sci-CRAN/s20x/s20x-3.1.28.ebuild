# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Functions for University of Auck... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/s20x_3.1-28.tar.gz"
LICENSE='GPL-2'
