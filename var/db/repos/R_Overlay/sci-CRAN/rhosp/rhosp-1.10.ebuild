# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Side Effect Risks in Hospital : ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/rhosp_1.10.tar.gz"
LICENSE='GPL-2+'
