# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Graphics in the Context of Analy... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/wrGraph_1.0.1.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_factoextra r_suggests_factominer r_suggests_knitr
	r_suggests_limma r_suggests_rmarkdown r_suggests_sm"
R_SUGGESTS="
	r_suggests_factoextra? ( sci-CRAN/factoextra )
	r_suggests_factominer? ( sci-CRAN/FactoMineR )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_limma? ( sci-BIOC/limma )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_sm? ( sci-CRAN/sm )
"
DEPEND="sci-CRAN/wrMisc
	sci-CRAN/RColorBrewer
	>=dev-lang/R-3.1.0
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
