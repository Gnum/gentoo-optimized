# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Psychometric Meta-Analysis Toolkit'
SRC_URI="http://cran.r-project.org/src/contrib/psychmeta_2.1.7.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/data_table
	sci-CRAN/purrr
	sci-CRAN/dplyr
	sci-CRAN/rmarkdown
	sci-CRAN/cli
	sci-CRAN/rlang
	sci-CRAN/stringr
	sci-CRAN/tidyr
	sci-CRAN/reshape2
	sci-CRAN/xml2
	sci-CRAN/nor1mix
	sci-CRAN/knitr
	virtual/MASS
	>=dev-lang/R-3.4.0
	sci-CRAN/ggplot2
	virtual/boot
	sci-CRAN/crayon
	sci-omegahat/RCurl
	sci-CRAN/tibble
	sci-CRAN/stringi
	sci-CRAN/metafor
	sci-CRAN/RefManageR
	sci-CRAN/progress
	sci-CRAN/tmvtnorm
"
RDEPEND="${DEPEND-}"
