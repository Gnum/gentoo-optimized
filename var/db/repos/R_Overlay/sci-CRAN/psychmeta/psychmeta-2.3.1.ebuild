# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Psychometric Meta-Analysis Toolkit'
SRC_URI="http://cran.r-project.org/src/contrib/psychmeta_2.3.1.tar.gz"
LICENSE='GPL-3+'

DEPEND="virtual/MASS
	>=dev-lang/R-3.4.0
	sci-CRAN/knitr
	sci-CRAN/reshape2
	sci-CRAN/xml2
	sci-CRAN/RefManageR
	sci-omegahat/RCurl
	sci-CRAN/rlang
	sci-CRAN/ggplot2
	sci-CRAN/nor1mix
	sci-CRAN/stringi
	sci-CRAN/metafor
	sci-CRAN/rmarkdown
	sci-CRAN/crayon
	sci-CRAN/dplyr
	sci-CRAN/purrr
	virtual/boot
	sci-CRAN/stringr
	sci-CRAN/cli
	sci-CRAN/data_table
	sci-CRAN/tmvtnorm
	sci-CRAN/progress
	sci-CRAN/tidyr
	sci-CRAN/tibble
"
RDEPEND="${DEPEND-}"
