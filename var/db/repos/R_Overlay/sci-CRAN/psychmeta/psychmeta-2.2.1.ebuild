# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Psychometric Meta-Analysis Toolkit'
SRC_URI="http://cran.r-project.org/src/contrib/psychmeta_2.2.1.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-omegahat/RCurl
	virtual/MASS
	sci-CRAN/rmarkdown
	sci-CRAN/knitr
	sci-CRAN/purrr
	sci-CRAN/dplyr
	sci-CRAN/reshape2
	sci-CRAN/metafor
	sci-CRAN/ggplot2
	virtual/boot
	sci-CRAN/tibble
	>=dev-lang/R-3.4.0
	sci-CRAN/data_table
	sci-CRAN/cli
	sci-CRAN/progress
	sci-CRAN/xml2
	sci-CRAN/crayon
	sci-CRAN/stringr
	sci-CRAN/tidyr
	sci-CRAN/stringi
	sci-CRAN/nor1mix
	sci-CRAN/RefManageR
	sci-CRAN/rlang
	sci-CRAN/tmvtnorm
"
RDEPEND="${DEPEND-}"
