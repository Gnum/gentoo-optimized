# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Psychometric Meta-Analysis Toolkit'
SRC_URI="http://cran.r-project.org/src/contrib/psychmeta_2.1.1.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/knitr
	sci-CRAN/dplyr
	sci-CRAN/cli
	sci-CRAN/crayon
	sci-CRAN/ggplot2
	sci-CRAN/nor1mix
	virtual/MASS
	sci-CRAN/stringr
	sci-CRAN/rmarkdown
	sci-CRAN/fungible
	sci-CRAN/data_table
	sci-CRAN/tidyr
	sci-CRAN/RefManageR
	sci-omegahat/RCurl
	sci-CRAN/purrr
	virtual/boot
	sci-CRAN/stringi
	sci-CRAN/xml2
	sci-CRAN/progress
	sci-CRAN/tmvtnorm
	sci-CRAN/reshape2
	sci-CRAN/tibble
	sci-CRAN/metafor
	sci-CRAN/rlang
	>=dev-lang/R-3.4.0
"
RDEPEND="${DEPEND-}"
