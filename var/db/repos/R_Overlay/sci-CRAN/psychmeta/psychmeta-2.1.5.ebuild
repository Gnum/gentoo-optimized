# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Psychometric Meta-Analysis Toolkit'
SRC_URI="http://cran.r-project.org/src/contrib/psychmeta_2.1.5.tar.gz"
LICENSE='GPL-3+'

DEPEND=">=dev-lang/R-3.4.0
	sci-CRAN/tmvtnorm
	sci-CRAN/stringr
	sci-CRAN/RefManageR
	sci-CRAN/data_table
	sci-CRAN/tibble
	sci-CRAN/reshape2
	sci-CRAN/rlang
	sci-CRAN/cli
	sci-CRAN/ggplot2
	sci-CRAN/metafor
	sci-CRAN/crayon
	sci-CRAN/nor1mix
	sci-CRAN/rmarkdown
	sci-CRAN/tidyr
	sci-CRAN/knitr
	sci-CRAN/stringi
	sci-CRAN/dplyr
	sci-CRAN/fungible
	virtual/boot
	sci-omegahat/RCurl
	sci-CRAN/xml2
	virtual/MASS
	sci-CRAN/purrr
	sci-CRAN/progress
"
RDEPEND="${DEPEND-}"
