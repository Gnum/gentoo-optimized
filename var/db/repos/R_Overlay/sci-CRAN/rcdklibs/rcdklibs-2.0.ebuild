# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='The CDK Libraries Packaged for R'
SRC_URI="http://cran.r-project.org/src/contrib/rcdklibs_2.0.tar.gz"
LICENSE='LGPL-3+'

DEPEND=">=sci-CRAN/rJava-0.9.8"
RDEPEND="${DEPEND-}"
