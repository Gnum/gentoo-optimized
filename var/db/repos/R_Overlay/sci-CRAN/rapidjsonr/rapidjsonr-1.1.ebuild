# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Rapidjson C++ Header Files'
SRC_URI="http://cran.r-project.org/src/contrib/rapidjsonr_1.1.tar.gz"
LICENSE='MIT'
