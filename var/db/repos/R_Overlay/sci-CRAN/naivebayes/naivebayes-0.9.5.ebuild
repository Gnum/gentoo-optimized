# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='High Performance Implementation ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/naivebayes_0.9.5.tar.gz"
LICENSE='GPL-2'
