# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Sierpinski Pedal Triangle'
SRC_URI="http://cran.r-project.org/src/contrib/spt_2.5.1.tar.gz"
