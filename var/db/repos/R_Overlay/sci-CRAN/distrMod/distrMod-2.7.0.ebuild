# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Object Oriented Implementation o... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/distrMod_2.7.0.tar.gz"
LICENSE='LGPL-3'

IUSE="${IUSE-} r_suggests_evd r_suggests_ismev"
R_SUGGESTS="
	r_suggests_evd? ( sci-CRAN/evd )
	r_suggests_ismev? ( sci-CRAN/ismev )
"
DEPEND=">=sci-CRAN/RandVar-0.6.3
	sci-CRAN/startupmsg
	>=sci-CRAN/distr-2.5.2
	sci-CRAN/sfsmisc
	>=sci-CRAN/distrEx-2.4
	virtual/MASS
	>=dev-lang/R-2.14.0
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
