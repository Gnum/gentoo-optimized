# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Quantile Regression Neural Network'
SRC_URI="http://cran.r-project.org/src/contrib/qrnn_2.0.5.tar.gz"
LICENSE='GPL-2'
