# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Multi-Locus Random-SNP-Effect Mi... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/mrMLM_3.1.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/doParallel
	sci-CRAN/openxlsx
	sci-CRAN/lars
	sci-CRAN/ggplot2
	sci-CRAN/bigmemory
	sci-CRAN/qqman
	sci-CRAN/data_table
	sci-CRAN/coin
	sci-CRAN/stringr
	sci-CRAN/sampling
	sci-CRAN/foreach
	virtual/MASS
	sci-CRAN/ncvreg
"
RDEPEND="${DEPEND-}"
