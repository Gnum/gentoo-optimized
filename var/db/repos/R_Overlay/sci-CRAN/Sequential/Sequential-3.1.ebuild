# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Exact Sequential Analysis for Po... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/Sequential_3.1.tar.gz"
LICENSE='GPL-2'

DEPEND="virtual/boot"
RDEPEND="${DEPEND-}"
