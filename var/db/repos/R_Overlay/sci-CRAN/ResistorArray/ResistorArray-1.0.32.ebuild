# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Electrical Properties of Resistor Networks'
SRC_URI="http://cran.r-project.org/src/contrib/ResistorArray_1.0-32.tar.gz"
LICENSE='GPL-2'
