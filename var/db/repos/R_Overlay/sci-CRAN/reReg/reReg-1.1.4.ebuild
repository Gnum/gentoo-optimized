# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Recurrent Event Regression'
SRC_URI="http://cran.r-project.org/src/contrib/reReg_1.1.4.tar.gz"
LICENSE='GPL-3+'

DEPEND=">=sci-CRAN/BB-2014.10.1
	>=sci-CRAN/tidyr-0.8.0
	>=sci-CRAN/dplyr-0.7.4
	virtual/survival
	>=sci-CRAN/purrr-0.2.4
	virtual/MASS
	>=sci-CRAN/nleqslv-3.3.1
	>=sci-CRAN/ggplot2-2.2.1
	>=sci-CRAN/plyr-1.8.4
	>=sci-CRAN/SQUAREM-2017.10.1
	>=dev-lang/R-3.4.0
"
RDEPEND="${DEPEND-}"
