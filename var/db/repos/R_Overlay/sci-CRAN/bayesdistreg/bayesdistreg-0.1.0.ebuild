# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Bayesian Distribution Regression'
SRC_URI="http://cran.r-project.org/src/contrib/bayesdistreg_0.1.0.tar.gz"
LICENSE='GPL-2'

DEPEND="virtual/MASS
	sci-CRAN/sandwich
"
RDEPEND="${DEPEND-}"
