# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Substitution and Indel Distances... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/sidier_4.0.3.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/ape
	>=dev-lang/R-3.5.0
	sci-CRAN/gridBase
	sci-CRAN/ggplot2
	sci-CRAN/igraph
	sci-CRAN/ggmap
	sci-CRAN/network
"
RDEPEND="${DEPEND-}"
