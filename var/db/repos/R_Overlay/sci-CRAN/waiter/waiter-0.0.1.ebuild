# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Loading Screen for Shiny'
SRC_URI="http://cran.r-project.org/src/contrib/waiter_0.0.1.tar.gz"
LICENSE='MIT'

DEPEND="sci-CRAN/shiny"
RDEPEND="${DEPEND-}"
