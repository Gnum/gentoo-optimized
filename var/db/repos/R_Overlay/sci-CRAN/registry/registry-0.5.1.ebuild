# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Infrastructure for R Package Registries'
SRC_URI="http://cran.r-project.org/src/contrib/registry_0.5-1.tar.gz"
LICENSE='GPL-2'
