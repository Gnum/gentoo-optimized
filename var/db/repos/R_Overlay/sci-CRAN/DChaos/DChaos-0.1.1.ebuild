# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Chaotic Time Series Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/DChaos_0.1-1.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/zoo
	sci-CRAN/xts
	virtual/nnet
	sci-CRAN/NeuralNetTools
	sci-CRAN/sandwich
	sci-CRAN/entropy
	sci-CRAN/pracma
	sci-CRAN/outliers
"
RDEPEND="${DEPEND-}"
