# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Spatial Covariance Functions'
SRC_URI="http://cran.r-project.org/src/contrib/ncf_1.2-6.tar.gz"
LICENSE='GPL-3'
