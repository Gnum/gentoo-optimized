# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Single-Case Visual Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/SCVA_1.3.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/scales
	sci-CRAN/ggplot2
	sci-CRAN/ggExtra
	sci-CRAN/plotly
"
RDEPEND="${DEPEND-}"
