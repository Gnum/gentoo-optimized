# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Random Orthonormal Matrix Genera... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/rstiefel_0.20.tar.gz"
LICENSE='GPL-3'
