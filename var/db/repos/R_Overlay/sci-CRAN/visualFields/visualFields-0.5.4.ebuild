# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Statistical Methods for Visual Fields'
SRC_URI="http://cran.r-project.org/src/contrib/visualFields_0.5.4.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/Hmisc
	>=dev-lang/R-2.14.0
	sci-CRAN/spatstat
	sci-CRAN/gtools
	>=sci-CRAN/flip-2.1
	sci-CRAN/deldir
	virtual/Matrix
"
RDEPEND="${DEPEND-}"
