# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Pseudo-Ranks'
SRC_URI="http://cran.r-project.org/src/contrib/pseudorank_0.1.0.tar.gz"
LICENSE='GPL-3'

DEPEND=">=dev-lang/R-3.4.0
	>=sci-CRAN/Rcpp-0.12.16
"
RDEPEND="${DEPEND-} sci-CRAN/Rcpp"
