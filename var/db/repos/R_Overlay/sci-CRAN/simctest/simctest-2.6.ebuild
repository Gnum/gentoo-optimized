# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Safe Implementation of Monte Carlo Tests'
SRC_URI="http://cran.r-project.org/src/contrib/simctest_2.6.tar.gz"
LICENSE='GPL-2+'
