# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Operations to Ease Data Analyses... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/naijR_0.1.0.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_covr r_suggests_knitr r_suggests_readxl
	r_suggests_rmarkdown r_suggests_testthat"
R_SUGGESTS="
	r_suggests_covr? ( sci-CRAN/covr )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_readxl? ( sci-CRAN/readxl )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_testthat? ( sci-CRAN/testthat )
"
DEPEND=">=sci-CRAN/RColorBrewer-1.1.2
	>=sci-CRAN/rgdal-1.4.4
	>=sci-CRAN/magrittr-1.5
	>=sci-CRAN/maps-3.3.0
	>=sci-CRAN/rlang-0.4.0
	>=sci-CRAN/mapdata-2.3.0
	>=dev-lang/R-3.6
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
