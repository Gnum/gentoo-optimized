# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Data Sets Used Useful for Modeling Packages'
SRC_URI="http://cran.r-project.org/src/contrib/modeldata_0.0.1.tar.gz"
LICENSE='MIT'
