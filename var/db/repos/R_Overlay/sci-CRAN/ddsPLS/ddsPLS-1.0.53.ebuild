# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Multi-Data-Driven Sparse PLS Rob... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/ddsPLS_1.0.53.tar.gz"
LICENSE='MIT'

IUSE="${IUSE-} r_suggests_knitr r_suggests_rmarkdown"
R_SUGGESTS="
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
"
DEPEND="sci-CRAN/RColorBrewer
	sci-CRAN/foreach
	sci-CRAN/doParallel
	sci-CRAN/Rdpack
	virtual/MASS
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
