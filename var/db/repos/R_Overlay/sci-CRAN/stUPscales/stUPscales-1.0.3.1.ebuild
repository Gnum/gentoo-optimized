# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Spatio-Temporal Uncertainty Prop... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/stUPscales_1.0.3.1.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/doParallel
	sci-CRAN/data_table
	sci-CRAN/zoo
	sci-CRAN/EmiStatR
	sci-CRAN/mAr
	sci-CRAN/lmom
	sci-CRAN/ggplot2
	sci-CRAN/foreach
	sci-CRAN/msm
	sci-CRAN/moments
	virtual/lattice
	sci-CRAN/xts
	sci-CRAN/hydroGOF
"
RDEPEND="${DEPEND-}"
