# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Continuous Glucose Monitoring Data Analyzer'
SRC_URI="http://cran.r-project.org/src/contrib/CGManalyzer_1.2.tar.gz"
LICENSE='MIT'
