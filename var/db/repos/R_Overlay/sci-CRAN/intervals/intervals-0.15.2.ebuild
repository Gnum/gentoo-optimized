# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Tools for Working with Points and Intervals'
SRC_URI="http://cran.r-project.org/src/contrib/intervals_0.15.2.tar.gz"
LICENSE='Artistic-2'
