# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Exploratory Data Analysis System'
SRC_URI="http://cran.r-project.org/src/contrib/discoveR_1.1.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/shiny
	sci-CRAN/rstudioapi
	sci-CRAN/shinyAce
	sci-CRAN/ggplot2
	sci-CRAN/shinydashboardPlus
	sci-CRAN/colourpicker
	sci-CRAN/rmarkdown
	sci-CRAN/zip
	sci-CRAN/DT
	sci-CRAN/factoextra
	>=dev-lang/R-3.5
	sci-CRAN/stringi
	sci-CRAN/shinydashboard
	sci-CRAN/shinyjs
"
RDEPEND="${DEPEND-}"
