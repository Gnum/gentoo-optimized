# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Exploratory Data Analysis System'
SRC_URI="http://cran.r-project.org/src/contrib/discoveR_1.0.1.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/reshape
	sci-CRAN/shiny
	sci-CRAN/promises
	sci-CRAN/shinydashboard
	>=dev-lang/R-3.5
	sci-CRAN/ggplot2
	sci-CRAN/DT
	sci-CRAN/stringr
	sci-CRAN/future
	sci-CRAN/shinyWidgets
	sci-CRAN/factoextra
	sci-CRAN/shinyAce
	sci-CRAN/colourpicker
	sci-CRAN/knitr
	sci-CRAN/FactoMineR
	sci-CRAN/rstudioapi
	sci-CRAN/scatterplot3d
	sci-CRAN/corrplot
	sci-CRAN/dendextend
	sci-CRAN/raster
	sci-CRAN/rmarkdown
	sci-CRAN/shinyjs
	sci-CRAN/ggdendro
	sci-CRAN/rgdal
	sci-CRAN/shinydashboardPlus
	sci-CRAN/stringi
	sci-CRAN/zip
"
RDEPEND="${DEPEND-}"
