# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Partially Overlapping Samples t-Tests'
SRC_URI="http://cran.r-project.org/src/contrib/Partiallyoverlapping_1.1.tar.gz"
LICENSE='GPL-3'

DEPEND=">=dev-lang/R-3.1.3"
RDEPEND="${DEPEND-}"
