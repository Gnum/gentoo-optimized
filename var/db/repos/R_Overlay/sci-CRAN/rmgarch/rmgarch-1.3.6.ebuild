# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Multivariate GARCH Models'
SRC_URI="http://cran.r-project.org/src/contrib/rmgarch_1.3-6.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/shape
	virtual/Matrix
	>=dev-lang/R-3.0.2
	sci-CRAN/zoo
	sci-CRAN/ff
	sci-CRAN/Rsolnp
	sci-CRAN/spd
	sci-CRAN/rugarch
	sci-CRAN/Bessel
	sci-CRAN/pcaPP
	virtual/MASS
	sci-CRAN/xts
	sci-CRAN/Rcpp
	sci-CRAN/corpcor
"
RDEPEND="${DEPEND-}
	>=sci-CRAN/Rcpp-0.10.6
	>=sci-CRAN/RcppArmadillo-0.2.34
"
