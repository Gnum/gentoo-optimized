# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Distribution Optimization'
SRC_URI="http://cran.r-project.org/src/contrib/DistributionOptimization_1.2.1.tar.gz"
LICENSE='MIT'

DEPEND="sci-CRAN/GA
	sci-CRAN/ggplot2
	sci-CRAN/AdaptGauss
"
RDEPEND="${DEPEND-}"
