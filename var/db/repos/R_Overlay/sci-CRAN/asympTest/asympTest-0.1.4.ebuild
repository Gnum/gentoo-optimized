# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='A Simple R Package for Classical... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/asympTest_0.1.4.tar.gz"
LICENSE='GPL-2+'
