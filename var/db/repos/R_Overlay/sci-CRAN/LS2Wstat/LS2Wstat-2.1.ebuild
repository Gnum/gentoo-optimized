# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='A Multiscale Test of Spatial Sta... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/LS2Wstat_2.1.tar.gz"
LICENSE='GPL-2'

DEPEND=">=sci-CRAN/LS2W-1.3.1
	sci-CRAN/matrixStats
	sci-CRAN/RandomFields
	sci-CRAN/geoR
	sci-CRAN/spdep
"
RDEPEND="${DEPEND-}"
