# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Moment and Inverse Moment Bayes Factors'
SRC_URI="http://cran.r-project.org/src/contrib/mombf_2.1.2.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/mvtnorm
	virtual/survival
	sci-CRAN/mclust
	sci-CRAN/ncvreg
	virtual/mgcv
	>=dev-lang/R-2.14.0
"
RDEPEND="${DEPEND-}"
