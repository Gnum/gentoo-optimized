# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Tables of Descriptive Statistics in HTML'
SRC_URI="http://cran.r-project.org/src/contrib/table1_1.1.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_boot r_suggests_matchit"
R_SUGGESTS="
	r_suggests_boot? ( virtual/boot )
	r_suggests_matchit? ( sci-CRAN/MatchIt )
"
DEPEND="sci-CRAN/Formula
	sci-CRAN/htmltools
	sci-CRAN/knitr
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
