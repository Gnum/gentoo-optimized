# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Statistical Tools for Covariance Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/CovTools_0.3.1.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/Rcpp
	sci-CRAN/foreach
	sci-CRAN/pracma
	sci-CRAN/expm
	sci-CRAN/geigen
	sci-CRAN/shapes
	sci-CRAN/Rdpack
	sci-CRAN/mvtnorm
	virtual/Matrix
	sci-CRAN/doParallel
	>=dev-lang/R-2.14.0
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
"
