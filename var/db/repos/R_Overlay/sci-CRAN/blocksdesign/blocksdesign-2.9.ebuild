# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Nested and Crossed Block Designs... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/blocksdesign_2.9.tar.gz"
LICENSE='GPL-2+'

IUSE="${IUSE-} r_suggests_knitr r_suggests_rmarkdown"
R_SUGGESTS="
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
"
DEPEND=">=dev-lang/R-3.1.0
	sci-CRAN/lme4
	sci-CRAN/crossdes
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
