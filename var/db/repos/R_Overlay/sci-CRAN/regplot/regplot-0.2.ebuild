# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Enhanced Regression Nomogram Plot'
SRC_URI="http://cran.r-project.org/src/contrib/regplot_0.2.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/sm
	sci-CRAN/beanplot
	virtual/survival
	sci-CRAN/vioplot
"
RDEPEND="${DEPEND-}"
