# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Segmentation Methods for Array CGH Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/cghseg_1.0.5.tar.gz"
LICENSE='GPL-2+'
