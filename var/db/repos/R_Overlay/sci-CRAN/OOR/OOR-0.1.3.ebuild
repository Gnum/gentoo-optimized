# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Optimistic Optimization in R'
SRC_URI="http://cran.r-project.org/src/contrib/OOR_0.1.3.tar.gz"
LICENSE='LGPL-3+'
