# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Read and write JPEG images'
SRC_URI="http://cran.r-project.org/src/contrib/jpeg_0.1-8.1.tar.gz"
LICENSE='|| ( GPL-2 GPL-3 )'

RDEPEND="${DEPEND-} virtual/jpeg"
