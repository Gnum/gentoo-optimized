# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Copy Number Profile Curve-Based Association Test'
SRC_URI="http://cran.r-project.org/src/contrib/CONCUR_1.2.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/CompQuadForm
	sci-CRAN/dplyr
	virtual/mgcv
"
RDEPEND="${DEPEND-}"
