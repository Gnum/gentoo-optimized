# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Phylogenetic Tree Search Using C... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/TreeSearch_0.3.0.tar.gz"
LICENSE='GPL-3+'

IUSE="${IUSE-} r_suggests_knitr r_suggests_rcpp r_suggests_rmarkdown
	r_suggests_shiny r_suggests_testthat r_suggests_xlsx"
R_SUGGESTS="
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_rcpp? ( sci-CRAN/Rcpp )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_shiny? ( sci-CRAN/shiny )
	r_suggests_testthat? ( sci-CRAN/testthat )
	r_suggests_xlsx? ( sci-CRAN/xlsx )
"
DEPEND=">=sci-CRAN/phangorn-2.2.1
	>=dev-lang/R-3.4.0
	sci-CRAN/clue
	>=sci-CRAN/ape-5.0
	sci-CRAN/colorspace
	sci-CRAN/R_cache
	sci-CRAN/memoise
	sci-CRAN/Rdpack
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	${R_SUGGESTS-}
"
