# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Composite-Likelihood Based Analy... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/CompRandFld_1.0.3-5.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/fields
	sci-CRAN/mapproj
	sci-CRAN/scatterplot3d
	sci-CRAN/RandomFields
	sci-CRAN/maps
	>=dev-lang/R-2.13
	sci-CRAN/spam
"
RDEPEND="${DEPEND-}"
