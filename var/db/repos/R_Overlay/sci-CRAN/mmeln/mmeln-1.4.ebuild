# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Estimation of Multinormal Mixture Distribution'
SRC_URI="http://cran.r-project.org/src/contrib/mmeln_1.4.tar.gz"
LICENSE='GPL-3'
