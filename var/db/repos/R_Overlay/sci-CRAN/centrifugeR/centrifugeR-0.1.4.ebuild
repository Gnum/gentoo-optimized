# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Non-Trivial Balance of Centrifuge Rotors'
SRC_URI="http://cran.r-project.org/src/contrib/centrifugeR_0.1.4.tar.gz"
LICENSE='GPL-3'

DEPEND=">=dev-lang/R-3.6.0
	>=sci-CRAN/pracma-2.2.9
"
RDEPEND="${DEPEND-}"
