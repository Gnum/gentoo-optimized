# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Interface to the XGobi and XGvis... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/xgobi_1.2-15.tar.gz"
