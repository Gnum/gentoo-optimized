# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='String Functions for Compact R Code'
SRC_URI="http://cran.r-project.org/src/contrib/yasp_0.2.0.tar.gz"
LICENSE='MIT'
