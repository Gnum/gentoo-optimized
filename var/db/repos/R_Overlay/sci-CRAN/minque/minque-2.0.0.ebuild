# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Various Linear Mixed Model Analyses'
SRC_URI="http://cran.r-project.org/src/contrib/minque_2.0.0.tar.gz"
LICENSE='GPL-3'
