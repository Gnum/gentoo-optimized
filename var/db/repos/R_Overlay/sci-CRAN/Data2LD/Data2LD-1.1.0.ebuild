# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Functional Data Analysis with Li... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/Data2LD_1.1.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/fda
	sci-CRAN/deSolve
	virtual/Matrix
	sci-CRAN/R_cache
"
RDEPEND="${DEPEND-}"
