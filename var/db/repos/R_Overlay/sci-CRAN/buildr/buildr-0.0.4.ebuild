# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Comfort Way to Run Build Scripts'
SRC_URI="http://cran.r-project.org/src/contrib/buildr_0.0.4.tar.gz"
LICENSE='GPL-3'
