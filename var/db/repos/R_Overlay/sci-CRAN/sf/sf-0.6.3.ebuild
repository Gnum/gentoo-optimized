# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Simple Features for R'
SRC_URI="http://cran.r-project.org/src/contrib/sf_0.6-3.tar.gz"

IUSE="${IUSE-} r_suggests_covr r_suggests_dplyr r_suggests_ggplot2
	r_suggests_knitr r_suggests_lwgeom r_suggests_maps
	r_suggests_maptools r_suggests_microbenchmark r_suggests_odbc
	r_suggests_pillar r_suggests_raster r_suggests_rgdal r_suggests_rgeos
	r_suggests_rlang r_suggests_rmarkdown r_suggests_rpostgres
	r_suggests_rpostgresql r_suggests_rsqlite r_suggests_sp
	r_suggests_spatstat r_suggests_testthat r_suggests_tibble
	r_suggests_tidyr r_suggests_tidyselect"
R_SUGGESTS="
	r_suggests_covr? ( sci-CRAN/covr )
	r_suggests_dplyr? ( >=sci-CRAN/dplyr-0.7.0 )
	r_suggests_ggplot2? ( sci-CRAN/ggplot2 )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_lwgeom? ( >=sci-CRAN/lwgeom-0.1.2 )
	r_suggests_maps? ( sci-CRAN/maps )
	r_suggests_maptools? ( sci-CRAN/maptools )
	r_suggests_microbenchmark? ( sci-CRAN/microbenchmark )
	r_suggests_odbc? ( sci-CRAN/odbc )
	r_suggests_pillar? ( sci-CRAN/pillar )
	r_suggests_raster? ( sci-CRAN/raster )
	r_suggests_rgdal? ( sci-CRAN/rgdal )
	r_suggests_rgeos? ( sci-CRAN/rgeos )
	r_suggests_rlang? ( sci-CRAN/rlang )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_rpostgres? ( >=sci-CRAN/RPostgres-1.1.0 )
	r_suggests_rpostgresql? ( sci-CRAN/RPostgreSQL )
	r_suggests_rsqlite? ( sci-CRAN/RSQLite )
	r_suggests_sp? ( >=sci-CRAN/sp-1.2.4 )
	r_suggests_spatstat? ( sci-CRAN/spatstat )
	r_suggests_testthat? ( sci-CRAN/testthat )
	r_suggests_tibble? ( >=sci-CRAN/tibble-1.4.1 )
	r_suggests_tidyr? ( >=sci-CRAN/tidyr-0.7.2 )
	r_suggests_tidyselect? ( sci-CRAN/tidyselect )
"
DEPEND=">=sci-CRAN/DBI-0.8
	>=sci-CRAN/units-0.5.1
	>=dev-lang/R-3.3.0
	sci-CRAN/Rcpp
	virtual/class
	sci-CRAN/magrittr
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-libs/gdal
	sci-libs/proj
	sci-libs/geos
	${R_SUGGESTS-}
"

_UNRESOLVED_PACKAGES=(
	'sci-CRAN/mapview'
	'sci-CRAN/tmap'
)
