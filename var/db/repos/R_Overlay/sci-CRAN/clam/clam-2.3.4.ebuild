# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Classical Age-Depth Modelling of... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/clam_2.3.4.tar.gz"
LICENSE='GPL-2+'
