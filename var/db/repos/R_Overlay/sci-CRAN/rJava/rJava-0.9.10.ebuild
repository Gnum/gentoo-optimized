# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Low-Level R to Java Interface'
SRC_URI="http://cran.r-project.org/src/contrib/rJava_0.9-10.tar.gz"
LICENSE='GPL-2'

RDEPEND="${DEPEND-} virtual/jdk"
