# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Interface Between R and the Open... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/osrm_3.3.0.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_cartography"
R_SUGGESTS="r_suggests_cartography? ( sci-CRAN/cartography )"
DEPEND="sci-CRAN/jsonlite
	>=dev-lang/R-3.3.0
	sci-CRAN/lwgeom
	sci-CRAN/isoband
	sci-omegahat/RCurl
	sci-CRAN/gepaf
	sci-CRAN/sf
	sci-CRAN/sp
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
