# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Support Points'
SRC_URI="http://cran.r-project.org/src/contrib/support_0.1.3.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/nloptr
	sci-CRAN/randtoolbox
	virtual/Matrix
	>=sci-CRAN/Rcpp-0.12.4
	sci-CRAN/doParallel
	sci-CRAN/MHadaptive
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
	sci-CRAN/BH
"
