# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Robust Methods for High-Dimensional Data'
SRC_URI="http://cran.r-project.org/src/contrib/robustHD_0.6.0.tar.gz"
LICENSE='GPL-2+'

IUSE="${IUSE-} r_suggests_mvtnorm"
R_SUGGESTS="r_suggests_mvtnorm? ( sci-CRAN/mvtnorm )"
DEPEND=">=sci-CRAN/ggplot2-0.9.2
	virtual/MASS
	>=sci-CRAN/robustbase-0.9.5
	>=sci-CRAN/perry-0.2.0
	>=dev-lang/R-3.2.0
	>=sci-CRAN/Rcpp-0.9.10
"
RDEPEND="${DEPEND-}
	>=sci-CRAN/Rcpp-0.9.10
	>=sci-CRAN/RcppArmadillo-0.3.0
	${R_SUGGESTS-}
"
