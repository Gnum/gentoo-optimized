# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Single-Cell RNA-Seq Gene Expression Recovery'
SRC_URI="http://cran.r-project.org/src/contrib/SAVER_1.1.2.tar.gz"
LICENSE='GPL-2'

IUSE="${IUSE-} r_suggests_knitr r_suggests_rmarkdown"
R_SUGGESTS="
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
"
DEPEND=">=dev-lang/R-3.0.1
	sci-CRAN/doParallel
	sci-CRAN/foreach
	sci-CRAN/glmnet
	sci-CRAN/iterators
	virtual/Matrix
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
