# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Provides Easy Access to Complex Systems Datasets'
SRC_URI="http://cran.r-project.org/src/contrib/ICON_0.2.0.tar.gz"
LICENSE='CC0-1.0'
