# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Informatic Sequence Classification Trees'
SRC_URI="http://cran.r-project.org/src/contrib/insect_1.2.0.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_knitr r_suggests_rmarkdown r_suggests_testthat"
R_SUGGESTS="
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_testthat? ( sci-CRAN/testthat )
"
DEPEND="sci-CRAN/RANN
	>=sci-CRAN/kmer-1.1.0
	sci-CRAN/seqinr
	>=sci-CRAN/aphid-1.3.1
	sci-CRAN/xml2
	sci-CRAN/openssl
	>=sci-CRAN/ape-3.0.0
	>=sci-CRAN/phylogram-2.0.0
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
