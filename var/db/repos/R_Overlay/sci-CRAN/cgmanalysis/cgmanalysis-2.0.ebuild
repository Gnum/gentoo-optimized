# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Clean and Analyze Continuous Glucose Monitor Data'
SRC_URI="http://cran.r-project.org/src/contrib/cgmanalysis_2.0.tar.gz"
LICENSE='CC0-1.0'

DEPEND="sci-CRAN/pracma
	sci-CRAN/lubridate
	sci-CRAN/pastecs
	>=dev-lang/R-3.5.0
	sci-CRAN/ggplot2
	sci-CRAN/zoo
	sci-CRAN/readr
	sci-CRAN/gdata
"
RDEPEND="${DEPEND-}"
