# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Clean and Analyze Continuous Glucose Monitor Data'
SRC_URI="http://cran.r-project.org/src/contrib/cgmanalysis_2.2.tar.gz"
LICENSE='CC0-1.0'

DEPEND="sci-CRAN/zoo
	sci-CRAN/gdata
	sci-CRAN/pastecs
	sci-CRAN/lubridate
	sci-CRAN/ggplot2
	>=dev-lang/R-3.4.0
	sci-CRAN/pracma
	sci-CRAN/readr
"
RDEPEND="${DEPEND-}"
