# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Modeling and Assessing Similarit... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/SimDissolution_0.1.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/dplyr
	sci-CRAN/alabama
	sci-CRAN/mvtnorm
"
RDEPEND="${DEPEND-}"
