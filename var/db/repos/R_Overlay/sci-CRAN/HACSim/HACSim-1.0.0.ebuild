# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Iterative Extrapolation of Speci... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/HACSim_1.0.0.tar.gz"
LICENSE='GPL-3'

DEPEND=">=sci-CRAN/Rcpp-1.0.0
	>=sci-CRAN/ape-5.2
	>=sci-CRAN/pegas-0.11
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
"
