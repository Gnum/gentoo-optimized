# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Data-Limited Methods Toolkit'
SRC_URI="http://cran.r-project.org/src/contrib/DLMtool_5.4.0.tar.gz"
LICENSE='GPL-2'

IUSE="${IUSE-} r_suggests_testthat"
R_SUGGESTS="r_suggests_testthat? ( sci-CRAN/testthat )"
DEPEND="sci-CRAN/rfishbase
	sci-CRAN/ggplot2
	sci-CRAN/crayon
	sci-CRAN/mvtnorm
	sci-CRAN/ggrepel
	sci-CRAN/broom
	sci-CRAN/readxl
	sci-CRAN/snowfall
	sci-CRAN/rmarkdown
	sci-CRAN/shiny
	sci-CRAN/DT
	>=dev-lang/R-3.5.0
	virtual/boot
	sci-CRAN/tidyr
	virtual/MASS
	sci-CRAN/abind
	sci-CRAN/purrr
	sci-CRAN/devtools
	sci-CRAN/openxlsx
	sci-CRAN/knitr
	sci-CRAN/fmsb
	sci-CRAN/gridExtra
	sci-CRAN/kableExtra
	sci-CRAN/dplyr
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
	${R_SUGGESTS-}
"
