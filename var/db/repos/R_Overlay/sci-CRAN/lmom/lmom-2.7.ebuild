# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='L-Moments'
SRC_URI="http://cran.r-project.org/src/contrib/lmom_2.7.tar.gz"
LICENSE='CPL-0.5'

DEPEND=">=dev-lang/R-3.0.0"
RDEPEND="${DEPEND-}"
