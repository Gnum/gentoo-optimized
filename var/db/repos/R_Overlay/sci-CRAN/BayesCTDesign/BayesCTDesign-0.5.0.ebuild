# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Two Arm Bayesian Clinical Trial ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/BayesCTDesign_0.5.0.tar.gz"
LICENSE='GPL-3'

DEPEND=">=sci-CRAN/eha-2.5.1
	virtual/survival
	>=dev-lang/R-3.5.0
	>=sci-CRAN/ggplot2-2.2.1
	>=sci-CRAN/reshape2-1.4.3
"
RDEPEND="${DEPEND-}"
