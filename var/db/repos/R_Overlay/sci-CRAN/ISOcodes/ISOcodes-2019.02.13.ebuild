# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Selected ISO Codes'
SRC_URI="http://cran.r-project.org/src/contrib/ISOcodes_2019.02.13.tar.gz"
LICENSE='GPL-2'
