# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Functions for Elementary Bayesian Inference'
SRC_URI="http://cran.r-project.org/src/contrib/Bolstad_0.2-39.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-3.2.0
	sci-CRAN/mvtnorm
"
RDEPEND="${DEPEND-}"
