# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Supervised Weight of Evidence Bi... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/woeBinning_0.1.6.tar.gz"
LICENSE='GPL-2+'
