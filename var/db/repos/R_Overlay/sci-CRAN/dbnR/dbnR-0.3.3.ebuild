# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Dynamic Bayesian Network Learning and Inference'
SRC_URI="http://cran.r-project.org/src/contrib/dbnR_0.3.3.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_testthat r_suggests_visnetwork"
R_SUGGESTS="
	r_suggests_testthat? ( >=sci-CRAN/testthat-2.1.0 )
	r_suggests_visnetwork? ( >=sci-CRAN/visNetwork-2.0.8 )
"
DEPEND=">=dev-lang/R-3.6.0
	>=sci-CRAN/bnlearn-4.5
	>=sci-CRAN/data_table-1.12.4
	>=sci-CRAN/Rcpp-1.0.2
	>=sci-CRAN/magrittr-1.5
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	${R_SUGGESTS-}
"
