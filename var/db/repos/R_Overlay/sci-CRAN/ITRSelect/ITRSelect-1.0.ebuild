# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Variable Selection for Optimal I... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/ITRSelect_1.0.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/Formula
	sci-CRAN/Rglpk
	virtual/Matrix
	sci-CRAN/svMisc
	sci-CRAN/ncvreg
"
RDEPEND="${DEPEND-}"
