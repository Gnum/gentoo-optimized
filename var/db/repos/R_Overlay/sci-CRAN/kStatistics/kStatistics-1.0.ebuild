# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Unbiased Estimators for Cumulant Products'
SRC_URI="http://cran.r-project.org/src/contrib/kStatistics_1.0.tar.gz"
LICENSE='GPL-2+'
