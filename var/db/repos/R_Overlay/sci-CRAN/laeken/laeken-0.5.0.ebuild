# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Estimation of Indicators on Soci... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/laeken_0.5.0.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-3.2.0
	virtual/boot
	virtual/MASS
"
RDEPEND="${DEPEND-}"
