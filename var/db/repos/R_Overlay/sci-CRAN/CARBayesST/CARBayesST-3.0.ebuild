# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Spatio-Temporal Generalised Line... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/CARBayesST_3.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/truncdist
	>=dev-lang/R-3.0.0
	sci-CRAN/leaflet
	virtual/Matrix
	sci-CRAN/rgdal
	sci-CRAN/testthat
	virtual/MASS
	virtual/Matrix
	sci-CRAN/sp
	>=sci-CRAN/Rcpp-0.11.5
	sci-CRAN/coda
	sci-CRAN/dplyr
	sci-CRAN/gtools
	sci-CRAN/CARBayesdata
	sci-CRAN/truncnorm
	sci-CRAN/spam
	sci-CRAN/spdep
"
RDEPEND="${DEPEND-} sci-CRAN/Rcpp"
