# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Spatio-Temporal Generalised Line... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/CARBayesST_3.0.1.tar.gz"
LICENSE='GPL-2+'

DEPEND="virtual/MASS
	>=dev-lang/R-3.0.0
	sci-CRAN/truncnorm
	sci-CRAN/gtools
	sci-CRAN/leaflet
	virtual/Matrix
	sci-CRAN/CARBayesdata
	sci-CRAN/dplyr
	sci-CRAN/rgdal
	sci-CRAN/spdep
	sci-CRAN/testthat
	sci-CRAN/truncdist
	sci-CRAN/spam
	virtual/Matrix
	sci-CRAN/sp
	sci-CRAN/coda
	>=sci-CRAN/Rcpp-0.11.5
"
RDEPEND="${DEPEND-} sci-CRAN/Rcpp"
