# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Qualitative Comparative Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/QCA_3.5.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/shiny
	>sci-CRAN/admisc-0.1
	>=dev-lang/R-3.0.0
	sci-CRAN/fastdigest
	>=sci-CRAN/venn-1.2
"
RDEPEND="${DEPEND-}"
