# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='A Lightweight and Flexible Web Framework'
SRC_URI="http://cran.r-project.org/src/contrib/fiery_1.1.1.tar.gz"
LICENSE='MIT'

IUSE="${IUSE-} r_suggests_covr r_suggests_testthat"
R_SUGGESTS="
	r_suggests_covr? ( sci-CRAN/covr )
	r_suggests_testthat? ( sci-CRAN/testthat )
"
DEPEND="sci-CRAN/reqres
	sci-CRAN/httpuv
	sci-CRAN/uuid
	sci-CRAN/future
	sci-CRAN/assertthat
	sci-CRAN/crayon
	sci-CRAN/glue
	sci-CRAN/stringi
	sci-CRAN/R6
	sci-CRAN/later
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
