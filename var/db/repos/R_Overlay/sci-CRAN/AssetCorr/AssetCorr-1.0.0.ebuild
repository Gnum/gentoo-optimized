# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Estimating Asset Correlations from Default Data'
SRC_URI="http://cran.r-project.org/src/contrib/AssetCorr_1.0.0.tar.gz"
LICENSE='GPL-3'

DEPEND="virtual/boot
	sci-CRAN/mvQuad
	virtual/boot
	sci-CRAN/ggplot2
	sci-CRAN/knitr
	sci-CRAN/Rdpack
	sci-CRAN/mvtnorm
	sci-CRAN/numDeriv
	sci-CRAN/VineCopula
"
RDEPEND="${DEPEND-}"
