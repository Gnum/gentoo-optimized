# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Analysis of Codon Data under Sta... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/AnaCoDa_0.1.2.2.tar.gz"
LICENSE='GPL-2+'

IUSE="${IUSE-} r_suggests_coda r_suggests_hmisc r_suggests_knitr
	r_suggests_lmodel2 r_suggests_testthat r_suggests_vgam"
R_SUGGESTS="
	r_suggests_coda? ( sci-CRAN/coda )
	r_suggests_hmisc? ( sci-CRAN/Hmisc )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_lmodel2? ( sci-CRAN/lmodel2 )
	r_suggests_testthat? ( sci-CRAN/testthat )
	r_suggests_vgam? ( sci-CRAN/VGAM )
"
DEPEND=">=dev-lang/R-3.3.0
	>=sci-CRAN/Rcpp-0.11.3
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	${R_SUGGESTS-}
"
