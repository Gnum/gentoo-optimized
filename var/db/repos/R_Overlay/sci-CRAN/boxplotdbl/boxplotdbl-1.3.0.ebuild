# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Double Box Plot for Two-Axes Correlation'
SRC_URI="http://cran.r-project.org/src/contrib/boxplotdbl_1.3.0.tar.gz"
LICENSE='MIT'
