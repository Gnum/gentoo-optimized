# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Selection of Linear Estimators'
SRC_URI="http://cran.r-project.org/src/contrib/LINselect_1.1.1.tar.gz"
LICENSE='GPL-3+'

DEPEND=">=dev-lang/R-3.6.0
	sci-CRAN/mvtnorm
	sci-CRAN/elasticnet
	sci-CRAN/randomForest
	virtual/MASS
	sci-CRAN/pls
	sci-CRAN/gtools
"
RDEPEND="${DEPEND-}"
