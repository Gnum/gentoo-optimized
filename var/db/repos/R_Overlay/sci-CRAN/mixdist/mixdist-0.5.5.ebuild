# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Finite Mixture Distribution Models'
SRC_URI="http://cran.r-project.org/src/contrib/mixdist_0.5-5.tar.gz"
LICENSE='GPL-2+'
