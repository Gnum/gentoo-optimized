# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Continuous-Time Fractionally Int... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/carfima_2.0.1.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/numDeriv
	sci-CRAN/truncnorm
	sci-CRAN/Rdpack
	sci-CRAN/DEoptim
	sci-CRAN/Rcpp
	virtual/MASS
	sci-CRAN/cubature
	sci-CRAN/invgamma
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
	sci-CRAN/cubature
"
