# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Fast Calculation of Spillover Measures'
SRC_URI="http://cran.r-project.org/src/contrib/fastSOM_1.0.1.tar.gz"
LICENSE='GPL-2+'
