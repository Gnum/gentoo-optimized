# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Multiple Testing Procedures with... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/DiscreteFDR_1.2.tar.gz"
LICENSE='MIT'
