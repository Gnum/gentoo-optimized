# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Solving Least Squares or Quadrat... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/lsei_1.2-0.1.tar.gz"
LICENSE='GPL-2+'
