# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Common Functions for the Dynverse Packages'
SRC_URI="http://cran.r-project.org/src/contrib/dynutils_0.1.0.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/tibble
	sci-CRAN/crayon
	sci-CRAN/stringr
	>=dev-lang/R-3.0.0
	sci-CRAN/devtools
	sci-CRAN/Rcpp
	sci-CRAN/dplyr
	sci-CRAN/magrittr
	sci-CRAN/glue
	sci-CRAN/purrr
	sci-CRAN/processx
	sci-CRAN/desc
	sci-CRAN/testthat
	sci-CRAN/tidyr
"
RDEPEND="${DEPEND-} sci-CRAN/Rcpp"
