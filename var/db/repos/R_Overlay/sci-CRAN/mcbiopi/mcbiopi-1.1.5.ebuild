# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Matrix Computation Based Identif... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/mcbiopi_1.1.5.tar.gz"
LICENSE='LGPL-2+'
