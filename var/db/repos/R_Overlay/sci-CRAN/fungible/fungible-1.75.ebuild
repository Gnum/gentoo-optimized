# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Psychometric Functions from the Waller Lab'
SRC_URI="http://cran.r-project.org/src/contrib/fungible_1.75.tar.gz"
LICENSE='GPL-2+'

DEPEND="virtual/MASS
	sci-CRAN/RSpectra
	virtual/lattice
	>=dev-lang/R-3.0
	sci-CRAN/clue
	sci-CRAN/GPArotation
	sci-CRAN/psych
	sci-CRAN/nleqslv
	sci-CRAN/mvtnorm
	sci-CRAN/stringr
	sci-CRAN/e1071
	sci-CRAN/Rcsdp
"
RDEPEND="${DEPEND-}"
