# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Psychometric Functions from the Waller Lab'
SRC_URI="http://cran.r-project.org/src/contrib/fungible_1.76.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/clue
	sci-CRAN/e1071
	virtual/MASS
	sci-CRAN/nleqslv
	>=dev-lang/R-3.0
	sci-CRAN/psych
	sci-CRAN/GPArotation
	sci-CRAN/mvtnorm
	sci-CRAN/stringr
	virtual/lattice
	sci-CRAN/Rcsdp
	sci-CRAN/RSpectra
"
RDEPEND="${DEPEND-}"
