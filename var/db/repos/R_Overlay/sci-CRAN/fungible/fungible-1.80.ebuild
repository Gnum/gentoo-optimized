# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Psychometric Functions from the Waller Lab'
SRC_URI="http://cran.r-project.org/src/contrib/fungible_1.80.tar.gz"
LICENSE='GPL-2+'

DEPEND="virtual/MASS
	>=dev-lang/R-3.0
	sci-CRAN/nleqslv
	sci-CRAN/clue
	virtual/lattice
	sci-CRAN/GPArotation
	sci-CRAN/Rcsdp
	sci-CRAN/mvtnorm
	sci-CRAN/RSpectra
"
RDEPEND="${DEPEND-}"
