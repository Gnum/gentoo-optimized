# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Create Bipartite Networks Such a... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/economiccomplexity_0.3.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_ggraph r_suggests_knitr r_suggests_rdpack
	r_suggests_testthat"
R_SUGGESTS="
	r_suggests_ggraph? ( sci-CRAN/ggraph )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_rdpack? ( sci-CRAN/Rdpack )
	r_suggests_testthat? ( >=sci-CRAN/testthat-2.1.0 )
"
DEPEND="sci-CRAN/tibble
	virtual/Matrix
	sci-CRAN/tidyr
	sci-CRAN/magrittr
	sci-CRAN/dplyr
	sci-CRAN/rlang
	sci-CRAN/igraph
	>=dev-lang/R-3.5
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
