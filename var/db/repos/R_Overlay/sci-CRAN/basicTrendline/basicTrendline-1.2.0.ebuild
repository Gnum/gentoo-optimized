# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Add Trendline of Basic Regression Models to Plot'
SRC_URI="http://cran.r-project.org/src/contrib/basicTrendline_1.2.0.tar.gz"
LICENSE='GPL-3'
