# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Percentile Estimation of Fetal W... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/PTwins_0.1.1.tar.gz"
LICENSE='GPL-3'
