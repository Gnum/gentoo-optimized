# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Evaluation of Surrogate Endpoint... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/Surrogate_0.8.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/OrdinalLogisticBiplot
	virtual/survival
	sci-CRAN/logistf
	sci-CRAN/rms
	sci-CRAN/mixtools
	sci-CRAN/msm
	virtual/lattice
	virtual/lattice
	virtual/nlme
	virtual/MASS
	sci-CRAN/rgl
	sci-CRAN/lme4
"
RDEPEND="${DEPEND-}"
