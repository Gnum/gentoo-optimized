# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Evaluation of Surrogate Endpoint... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/Surrogate_1.2.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/logistf
	sci-CRAN/OrdinalLogisticBiplot
	virtual/lattice
	virtual/survival
	sci-CRAN/mixtools
	virtual/lattice
	sci-CRAN/msm
	sci-CRAN/rgl
	sci-CRAN/extraDistr
	sci-CRAN/ks
	sci-CRAN/rms
	virtual/nlme
	sci-CRAN/rootSolve
	sci-CRAN/lme4
	virtual/MASS
"
RDEPEND="${DEPEND-}"
