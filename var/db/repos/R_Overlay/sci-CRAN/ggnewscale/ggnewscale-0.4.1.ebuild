# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Multiple Fill and Colour Scales in ggplot2'
SRC_URI="http://cran.r-project.org/src/contrib/ggnewscale_0.4.1.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_testthat"
R_SUGGESTS="r_suggests_testthat? ( sci-CRAN/testthat )"
DEPEND=">=sci-CRAN/ggplot2-3.0.0"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
