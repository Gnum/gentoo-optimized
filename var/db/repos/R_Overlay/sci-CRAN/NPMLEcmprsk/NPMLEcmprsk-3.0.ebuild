# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Type-Specific Failure Rate and H... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/NPMLEcmprsk_3.0.tar.gz"
LICENSE='Artistic-2'
