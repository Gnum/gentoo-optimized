# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Sealing the R Objects Test and Assert Conditions'
SRC_URI="http://cran.r-project.org/src/contrib/sealr_0.1.0.tar.gz"
LICENSE='MIT'

IUSE="${IUSE-} r_suggests_covr r_suggests_knitr r_suggests_rmarkdown
	r_suggests_testthat"
R_SUGGESTS="
	r_suggests_covr? ( >=sci-CRAN/covr-3.0.1 )
	r_suggests_knitr? ( >=sci-CRAN/knitr-1.20 )
	r_suggests_rmarkdown? ( >=sci-CRAN/rmarkdown-1.9 )
	r_suggests_testthat? ( >=sci-CRAN/testthat-2.0.0 )
"
DEPEND=">=sci-CRAN/rstudioapi-0.7
	>=sci-CRAN/styler-1.0.0
	>=sci-CRAN/purrr-0.2.4
	>=sci-CRAN/rlang-0.2.0
	>=sci-CRAN/digest-0.6.15
	>=sci-CRAN/clisymbols-1.2.0
	>=sci-CRAN/clipr-0.4.0
	>=sci-CRAN/dplyr-0.7.4
	>=sci-CRAN/glue-1.2.0
	>=sci-CRAN/tidyr-0.8.0
	>=sci-CRAN/magrittr-1.5
	>=sci-CRAN/tibble-1.4.2
	>=dev-lang/R-3.1.0
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
