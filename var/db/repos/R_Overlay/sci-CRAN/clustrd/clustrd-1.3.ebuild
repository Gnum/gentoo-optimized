# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Methods for Joint Dimension Redu... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/clustrd_1.3.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/dummies
	virtual/cluster
	sci-CRAN/fpc
	sci-CRAN/ca
	sci-CRAN/dplyr
	sci-CRAN/ggrepel
	sci-CRAN/rARPACK
	sci-CRAN/corpcor
	sci-CRAN/tibble
	sci-CRAN/plyr
	sci-CRAN/GGally
	sci-CRAN/ggplot2
"
RDEPEND="${DEPEND-}"
