# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Bayesian Exponential Random Graph Models'
SRC_URI="http://cran.r-project.org/src/contrib/Bergm_4.2.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/network
	sci-CRAN/mvtnorm
	sci-CRAN/coda
	virtual/Matrix
	sci-CRAN/MCMCpack
	sci-CRAN/ergm
"
RDEPEND="${DEPEND-}"
