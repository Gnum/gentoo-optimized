# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Distribution-Free Confidence Region'
SRC_URI="http://cran.r-project.org/src/contrib/distfree.cr_1.5.1.tar.gz"
LICENSE='GPL-2+'
