# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='leaflet Extensions for mapview'
SRC_URI="http://cran.r-project.org/src/contrib/leafem_0.0.1.tar.gz"
LICENSE='MIT'

IUSE="${IUSE-} r_suggests_gdalutils r_suggests_plainview r_suggests_png"
R_SUGGESTS="
	r_suggests_gdalutils? ( sci-CRAN/gdalUtils )
	r_suggests_plainview? ( sci-CRAN/plainview )
	r_suggests_png? ( sci-CRAN/png )
"
DEPEND=">=sci-CRAN/htmltools-0.3
	sci-CRAN/sp
	sci-CRAN/htmlwidgets
	>=sci-CRAN/leaflet-2.0.1
	>=dev-lang/R-3.1.0
	sci-CRAN/raster
	sci-CRAN/sf
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
