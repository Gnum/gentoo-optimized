# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Solving 1D Advection Bi-Flux Diffusion Equation'
SRC_URI="http://cran.r-project.org/src/contrib/AdvDif4_0.7.18.tar.gz"
LICENSE='GPL-3'
