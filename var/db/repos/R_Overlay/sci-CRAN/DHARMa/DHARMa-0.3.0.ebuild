# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Residual Diagnostics for Hierarc... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/DHARMa_0.3.0.tar.gz"
LICENSE='GPL-3+'

IUSE="${IUSE-} r_suggests_kernsmooth r_suggests_knitr r_suggests_testthat"
R_SUGGESTS="
	r_suggests_kernsmooth? ( virtual/KernSmooth )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_testthat? ( sci-CRAN/testthat )
"
DEPEND="sci-CRAN/sfsmisc
	>=sci-CRAN/qgam-1.3.2
	>=sci-CRAN/glmmTMB-1.0.0
	sci-CRAN/gap
	sci-CRAN/foreach
	sci-CRAN/lmtest
	virtual/MASS
	>=sci-CRAN/spaMM-2.6.0
	>=dev-lang/R-3.0.2
	sci-CRAN/doParallel
	virtual/mgcv
	sci-CRAN/lme4
	sci-CRAN/ape
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
