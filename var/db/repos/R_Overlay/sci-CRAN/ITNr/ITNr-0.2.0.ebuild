# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Analysis of the International Trade Network'
SRC_URI="http://cran.r-project.org/src/contrib/ITNr_0.2.0.tar.gz"
LICENSE='GPL-3'

DEPEND="virtual/Matrix
	sci-CRAN/circlize
	sci-CRAN/igraph
	sci-CRAN/reshape2
	sci-CRAN/network
	sci-CRAN/ndtv
	sci-CRAN/sna
	sci-CRAN/cowplot
	sci-CRAN/GGally
	sci-CRAN/maps
	sci-CRAN/plyr
	sci-CRAN/dplyr
	sci-CRAN/GoodmanKruskal
	sci-CRAN/blockmodeling
	sci-CRAN/WDI
	sci-CRAN/xergm_common
	sci-CRAN/intergraph
	sci-CRAN/fastmatch
	sci-CRAN/ggplot2
	sci-CRAN/tnet
	sci-CRAN/statnet
	sci-CRAN/RColorBrewer
	sci-CRAN/animation
	sci-CRAN/comtradr
	sci-CRAN/xergm
	sci-CRAN/networkDynamic
"
RDEPEND="${DEPEND-}"
