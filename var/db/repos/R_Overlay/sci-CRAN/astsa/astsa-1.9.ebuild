# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Applied Statistical Time Series Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/astsa_1.9.tar.gz"
LICENSE='GPL-3'
