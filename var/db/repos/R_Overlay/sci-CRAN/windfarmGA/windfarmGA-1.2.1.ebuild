# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Genetic Algorithm for Wind Farm ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/windfarmGA_1.2.1.tar.gz"
LICENSE='MIT'

DEPEND=">=sci-CRAN/raster-2.5.2
	>=sci-CRAN/foreach-1.4.3
	>=dev-lang/R-3.2.3
	>=sci-CRAN/doParallel-1.0.11
	>=sci-CRAN/RColorBrewer-1.1.2
	>=sci-CRAN/sp-1.2.2
	>=sci-CRAN/rgdal-1.0.4
	>=sci-CRAN/geoR-1.7.5.1
	>=sci-CRAN/leaflet-1.1.0
	>=sci-CRAN/maptools-0.8.36
	>=sci-CRAN/gtools-3.5.0
	>=sci-CRAN/calibrate-1.7.2
	>=sci-CRAN/spatstat-1.42.2
	>=sci-CRAN/gstat-1.0.26
	>=sci-CRAN/RandomFields-3.1.50
	>=sci-CRAN/data_table-1.9.6
	>=sci-CRAN/magrittr-1.5
	>=sci-CRAN/rgl-0.96.0
	>=sci-CRAN/ggplot2-2.2.0
	>=sci-CRAN/dplyr-0.5.0
	>=sci-CRAN/rgeos-0.3.11
	>=sci-CRAN/RgoogleMaps-1.2.0.7
	>=sci-CRAN/googleVis-0.5.9
"
RDEPEND="${DEPEND-}"
