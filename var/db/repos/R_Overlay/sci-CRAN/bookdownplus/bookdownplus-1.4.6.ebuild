# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Generate Assorted Books and Docu... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/bookdownplus_1.4.6.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-3.1.0
	>=sci-CRAN/knitr-1.15
	>=sci-CRAN/yaml-2.1.14
	>=sci-CRAN/bookdown-0.3.17
"
RDEPEND="${DEPEND-}"
