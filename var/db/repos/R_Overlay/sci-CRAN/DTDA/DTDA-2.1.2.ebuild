# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Doubly Truncated Data Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/DTDA_2.1-2.tar.gz"
LICENSE='GPL-2'
