# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Causal Inference and Prediction ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/RISCA_0.8.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/statmod
	>=dev-lang/R-3.6.0
	sci-CRAN/relsurv
	sci-CRAN/mvtnorm
	virtual/survival
	virtual/nlme
	sci-CRAN/date
	sci-CRAN/riskRegression
	virtual/MASS
"
RDEPEND="${DEPEND-}"
