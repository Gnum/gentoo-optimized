# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='The Spike-and-Slab LASSO'
SRC_URI="http://cran.r-project.org/src/contrib/SSLASSO_1.2-2.tar.gz"
LICENSE='GPL-3'
