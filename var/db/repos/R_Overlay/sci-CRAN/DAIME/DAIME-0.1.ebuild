# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Effects of Changing Deposition Rates'
SRC_URI="http://cran.r-project.org/src/contrib/DAIME_0.1.tar.gz"
