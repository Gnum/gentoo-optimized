# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Converts ISCO-08 to Job Prestige... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/ISCO08ConveRsions_0.1.0.tar.gz"
LICENSE='GPL-2+'
