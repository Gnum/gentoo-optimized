# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Analysis of Variance and Other I... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/easyanova_6.0.tar.gz"
LICENSE='GPL-2'

DEPEND=">=dev-lang/R-3.0.0
	sci-CRAN/lmerTest
	sci-CRAN/car
	sci-CRAN/emmeans
	sci-CRAN/lme4
"
RDEPEND="${DEPEND-}"
