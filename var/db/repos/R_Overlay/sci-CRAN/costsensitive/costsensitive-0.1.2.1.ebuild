# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Cost-Sensitive Multi-Class Classification'
SRC_URI="http://cran.r-project.org/src/contrib/costsensitive_0.1.2.1.tar.gz"
LICENSE='BSD-2'
