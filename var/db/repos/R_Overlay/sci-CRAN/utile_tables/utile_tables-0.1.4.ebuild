# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Tools for Building Tables for Publication'
SRC_URI="http://cran.r-project.org/src/contrib/utile.tables_0.1.4.tar.gz"
LICENSE='LGPL-2+'

DEPEND="sci-CRAN/purrr
	sci-CRAN/tibble
	virtual/survival
	sci-CRAN/rlang
	>=sci-CRAN/utile_tools-0.1.2
	sci-CRAN/tidyr
	sci-CRAN/dplyr
	>=dev-lang/R-3.4.0
"
RDEPEND="${DEPEND-}"
