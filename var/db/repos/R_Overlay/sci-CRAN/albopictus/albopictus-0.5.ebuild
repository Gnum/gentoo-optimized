# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Age-Structured Population Dynamics Model'
SRC_URI="http://cran.r-project.org/src/contrib/albopictus_0.5.tar.gz"
LICENSE='GPL-3+'
