# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Convex Hull'
SRC_URI="http://cran.r-project.org/src/contrib/cxhull_0.2.0.tar.gz"
LICENSE='GPL-3'
