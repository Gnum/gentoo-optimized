# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Root Mean Square Curvature Calculation'
SRC_URI="http://cran.r-project.org/src/contrib/IPEC_0.1.0.tar.gz"
LICENSE='GPL-2'

DEPEND=">=sci-CRAN/numDeriv-2016.8.1
	virtual/MASS
"
RDEPEND="${DEPEND-}"
