# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Magnetic Resonance Binning, Inte... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/mrbin_1.4.1.tar.gz"
LICENSE='GPL-3'
