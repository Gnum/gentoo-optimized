# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Classical Test Theory Functions'
SRC_URI="http://cran.r-project.org/src/contrib/CTT_2.3.3.tar.gz"
LICENSE='GPL-2+'
