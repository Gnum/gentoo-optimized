# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Connect R to Bloomberg Data License'
SRC_URI="http://cran.r-project.org/src/contrib/RblDataLicense_0.1.2.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/curl
	sci-omegahat/RCurl
	>=dev-lang/R-3.0
	sci-CRAN/xts
"
RDEPEND="${DEPEND-}"
