# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Bivariate Alternating Recurrent ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/BivRec_1.0.0.tar.gz"
LICENSE='GPL-3'

DEPEND=">=dev-lang/R-3.2.0
	sci-CRAN/Rcpp
	sci-CRAN/knitr
	virtual/survival
	sci-CRAN/rmarkdown
	sci-CRAN/stringr
	virtual/MASS
"
RDEPEND="${DEPEND-} sci-CRAN/Rcpp"
