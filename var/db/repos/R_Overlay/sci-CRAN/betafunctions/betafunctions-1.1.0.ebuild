# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Functions for Working with Two- ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/betafunctions_1.1.0.tar.gz"
LICENSE='CC0-1.0'
