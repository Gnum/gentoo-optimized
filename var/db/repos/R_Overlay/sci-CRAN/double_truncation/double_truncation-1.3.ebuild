# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Analysis of Doubly-Truncated Data'
SRC_URI="http://cran.r-project.org/src/contrib/double.truncation_1.3.tar.gz"
LICENSE='GPL-2'
