# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Replicate Tariff Method for Verbal Autopsy'
SRC_URI="http://cran.r-project.org/src/contrib/Tariff_1.0.5.tar.gz"
LICENSE='GPL-2'
