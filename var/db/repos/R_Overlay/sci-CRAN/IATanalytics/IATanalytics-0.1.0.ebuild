# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Compute Effect Sizes and Reliabi... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/IATanalytics_0.1.0.tar.gz"
LICENSE='MIT'
