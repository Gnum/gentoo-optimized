# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Read or Write any Format from Anywhere'
SRC_URI="http://cran.r-project.org/src/contrib/flyio_0.1.4.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/stringr
	sci-CRAN/assertthat
	>=dev-lang/R-3.1.2
	sci-CRAN/googleCloudStorageR
	sci-CRAN/aws_s3
"
RDEPEND="${DEPEND-}"
