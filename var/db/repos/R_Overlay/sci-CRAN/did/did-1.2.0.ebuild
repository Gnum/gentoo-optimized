# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Treatment Effects with Multiple Periods and Groups'
SRC_URI="http://cran.r-project.org/src/contrib/did_1.2.0.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/knitr
	virtual/MASS
	sci-CRAN/pbapply
	sci-CRAN/BMisc
	sci-CRAN/ggplot2
	sci-CRAN/gridExtra
"
RDEPEND="${DEPEND-}"
