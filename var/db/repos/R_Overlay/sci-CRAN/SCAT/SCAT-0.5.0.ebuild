# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Summary Based Conditional Association Test'
SRC_URI="http://cran.r-project.org/src/contrib/SCAT_0.5.0.tar.gz"
LICENSE='|| ( GPL-2 GPL-3 )'
