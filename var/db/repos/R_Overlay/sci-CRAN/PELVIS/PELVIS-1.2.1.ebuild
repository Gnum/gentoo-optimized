# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Probabilistic Sex Estimate using... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/PELVIS_1.2.1.tar.gz"
LICENSE='CeCILL-2'

DEPEND=">=dev-lang/R-3.4.0
	sci-CRAN/DT
	virtual/MASS
	sci-CRAN/shiny
"
RDEPEND="${DEPEND-}"
