# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Bayesian Group Sparse Multi-Task Regression'
SRC_URI="http://cran.r-project.org/src/contrib/bgsmtr_0.5.tar.gz"
LICENSE='GPL-2'

DEPEND="virtual/Matrix
	>=sci-CRAN/miscTools-0.6.22
	>=sci-CRAN/EDISON-1.1.1
	>=sci-CRAN/LaplacesDemon-16.1.0
	>=sci-CRAN/mnormt-1.5.4
	>=sci-CRAN/CholWishart-0.9.3
	>=sci-CRAN/mvtnorm-1.0.5
	virtual/Matrix
	>=sci-CRAN/inline-0.3.15
	>=sci-CRAN/statmod-1.4.26
	>=sci-CRAN/Rcpp-0.12.14
	>=dev-lang/R-3.3.0
	>=sci-CRAN/coda-0.18.1
	>=sci-CRAN/sparseMVN-0.2.0
"
RDEPEND="${DEPEND-}"
