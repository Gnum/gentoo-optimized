# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Functions for Spatial Thinning o... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/spThin_0.1.0.1.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/spam
	sci-CRAN/knitr
	sci-CRAN/fields
"
RDEPEND="${DEPEND-}"
