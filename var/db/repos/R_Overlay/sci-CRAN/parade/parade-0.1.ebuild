# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Pens Income Parades'
SRC_URI="http://cran.r-project.org/src/contrib/parade_0.1.tar.gz"
LICENSE='GPL-2+'
