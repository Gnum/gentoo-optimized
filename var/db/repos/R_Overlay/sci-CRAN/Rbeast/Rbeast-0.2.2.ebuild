# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Bayesian Change-Point Detection ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/Rbeast_0.2.2.tar.gz"
LICENSE='GPL-2+'
