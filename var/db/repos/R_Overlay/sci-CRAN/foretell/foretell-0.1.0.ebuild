# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Projecting Customer Retention Ba... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/foretell_0.1.0.tar.gz"
LICENSE='MIT'

DEPEND=">=dev-lang/R-3.0.1"
RDEPEND="${DEPEND-}"
