# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Decision Tree Analysis for Proba... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/psica_1.0.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/randomForest
	sci-CRAN/gridBase
	sci-CRAN/partykit
	sci-CRAN/BayesTree
	sci-CRAN/party
	virtual/rpart
"
RDEPEND="${DEPEND-}"
