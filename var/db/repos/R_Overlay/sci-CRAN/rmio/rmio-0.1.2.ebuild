# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Provides mio C++11 Header Files'
SRC_URI="http://cran.r-project.org/src/contrib/rmio_0.1.2.tar.gz"
LICENSE='GPL-3'
