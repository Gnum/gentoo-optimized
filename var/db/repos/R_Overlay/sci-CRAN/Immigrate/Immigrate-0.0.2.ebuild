# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Iterative Max-Min Entropy Margin... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/Immigrate_0.0.2.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-3.0.0
	sci-CRAN/Rcpp
	sci-CRAN/pROC
"
RDEPEND="${DEPEND-} sci-CRAN/Rcpp"
