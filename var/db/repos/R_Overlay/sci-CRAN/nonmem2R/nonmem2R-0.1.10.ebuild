# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Loading NONMEM Output Files and ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/nonmem2R_0.1.10.tar.gz"

DEPEND="virtual/lattice
	>=dev-lang/R-3.0.0
	virtual/lattice
	sci-CRAN/ggplot2
	virtual/MASS
	sci-CRAN/mvtnorm
	sci-CRAN/splines2
"
RDEPEND="${DEPEND-}"
