# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Generate Lero Lero Quotes'
SRC_URI="http://cran.r-project.org/src/contrib/lero.lero_0.2.tar.gz"
LICENSE='GPL-2+'
