# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Variance Estimation for Sample S... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/vardpoor_0.15.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/stringr
	>=sci-CRAN/data_table-1.11.4
	sci-CRAN/plyr
	sci-CRAN/surveyplanning
	sci-CRAN/pracma
	sci-CRAN/laeken
	sci-CRAN/ggplot2
	virtual/MASS
	sci-CRAN/foreach
	>=dev-lang/R-3.2.3
"
RDEPEND="${DEPEND-}"
