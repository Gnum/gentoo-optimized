# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Dirichlet Process Weibull Mixtur... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/DPWeibull_1.3.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=sci-CRAN/Rcpp-0.12.4
	sci-CRAN/truncdist
	>=sci-CRAN/DPpackage-1.1.7.4
	virtual/Matrix
"
RDEPEND="${DEPEND-} sci-CRAN/Rcpp"
