# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Tools for Behavior Change Resear... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/behaviorchange_0.0.1.tar.gz"
LICENSE='GPL-3+'

DEPEND=">=sci-CRAN/data_tree-0.7.5
	>=sci-CRAN/gridExtra-2.3
	>=dev-lang/R-3.0.0
	>=sci-CRAN/viridis-0.5.1
	>=sci-CRAN/magrittr-0.1.5
	>=sci-CRAN/userfriendlyscience-0.7.1
	>=sci-CRAN/DiagrammeR-1.0.0
	>=sci-CRAN/googlesheets-0.3.0
	>=sci-CRAN/ufs-0.0.1
	>=sci-CRAN/ggplot2-2.2.1
	>=sci-CRAN/gtable-0.2.0
"
RDEPEND="${DEPEND-}"
