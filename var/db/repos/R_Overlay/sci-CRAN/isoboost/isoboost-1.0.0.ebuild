# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Isotonic Boosting Classification Rules'
SRC_URI="http://cran.r-project.org/src/contrib/isoboost_1.0.0.tar.gz"
LICENSE='|| ( GPL-2 GPL-3 )'

DEPEND="sci-CRAN/Iso
	sci-CRAN/isotone
	virtual/rpart
"
RDEPEND="${DEPEND-}"
