# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='CUDA Parallel Implementation of ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/cudaBayesreg_0.3-16.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-3.0.0
	sci-CRAN/cudaBayesregData
	sci-CRAN/oro_nifti
"
RDEPEND="${DEPEND-} dev-util/nvidia-cuda-toolkit"
