# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Computing Optimal Cutpoints in Diagnostic Tests'
SRC_URI="http://cran.r-project.org/src/contrib/OptimalCutpoints_1.1-4.tar.gz"
LICENSE='GPL-2+'
