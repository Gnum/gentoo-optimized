# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Narrowest-Over-Threshold Change-Point Detection'
SRC_URI="http://cran.r-project.org/src/contrib/not_1.2.tar.gz"
LICENSE='GPL-2'
