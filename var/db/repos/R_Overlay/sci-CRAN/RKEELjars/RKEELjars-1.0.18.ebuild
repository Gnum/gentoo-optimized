# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Java Executable .jar Files for RKEEL'
SRC_URI="http://cran.r-project.org/src/contrib/RKEELjars_1.0.18.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/downloader"
RDEPEND="${DEPEND-}"
