# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='A Graphical User Interface for N... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/statnetWeb_0.5.0.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/network
	>=dev-lang/R-3.1.0
	sci-CRAN/RColorBrewer
	>=sci-CRAN/shiny-0.12.1
	virtual/lattice
	virtual/lattice
	>=sci-CRAN/ergm-3.2.4
	sci-CRAN/sna
"
RDEPEND="${DEPEND-}"
