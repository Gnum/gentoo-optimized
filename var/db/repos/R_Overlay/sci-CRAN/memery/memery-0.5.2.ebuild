# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Internet Memes for Data Analysts'
SRC_URI="http://cran.r-project.org/src/contrib/memery_0.5.2.tar.gz"
LICENSE='MIT'

IUSE="${IUSE-} r_suggests_covr r_suggests_gifski r_suggests_knitr
	r_suggests_lintr r_suggests_magick r_suggests_rmarkdown
	r_suggests_testthat"
R_SUGGESTS="
	r_suggests_covr? ( sci-CRAN/covr )
	r_suggests_gifski? ( sci-CRAN/gifski )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_lintr? ( sci-CRAN/lintr )
	r_suggests_magick? ( sci-CRAN/magick )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_testthat? ( sci-CRAN/testthat )
"
DEPEND="sci-CRAN/sysfonts
	sci-CRAN/showtext
	sci-CRAN/colourpicker
	sci-CRAN/cowplot
	sci-CRAN/shinyBS
	sci-CRAN/purrr
	sci-CRAN/shinycssloaders
	sci-CRAN/png
	sci-CRAN/shiny
	sci-CRAN/ggplot2
	sci-CRAN/Cairo
	sci-CRAN/magrittr
	sci-CRAN/jpeg
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
