# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Independent Component Analysis for Grouped Data'
SRC_URI="http://cran.r-project.org/src/contrib/groupICA_0.1.1.tar.gz"
LICENSE='AGPL-3'

DEPEND=">=dev-lang/R-3.2.3
	virtual/MASS
"
RDEPEND="${DEPEND-}"
