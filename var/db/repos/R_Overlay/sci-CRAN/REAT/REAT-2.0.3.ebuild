# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Regional Economic Analysis Toolbox'
SRC_URI="http://cran.r-project.org/src/contrib/REAT_2.0.3.tar.gz"
LICENSE='GPL-2+'
