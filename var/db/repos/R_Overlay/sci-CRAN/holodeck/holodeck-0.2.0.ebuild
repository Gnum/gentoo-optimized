# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='A Tidy Interface for Simulating Multivariate Data'
SRC_URI="http://cran.r-project.org/src/contrib/holodeck_0.2.0.tar.gz"
LICENSE='MIT'

IUSE="${IUSE-} r_suggests_covr r_suggests_ggplot2 r_suggests_iheatmapr
	r_suggests_knitr r_suggests_mice r_suggests_rmarkdown
	r_suggests_ropls r_suggests_testthat"
R_SUGGESTS="
	r_suggests_covr? ( sci-CRAN/covr )
	r_suggests_ggplot2? ( sci-CRAN/ggplot2 )
	r_suggests_iheatmapr? ( sci-CRAN/iheatmapr )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_mice? ( sci-CRAN/mice )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_ropls? ( sci-BIOC/ropls )
	r_suggests_testthat? ( sci-CRAN/testthat )
"
DEPEND="sci-CRAN/tibble
	virtual/MASS
	sci-CRAN/dplyr
	sci-CRAN/purrr
	sci-CRAN/assertthat
	sci-CRAN/rlang
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
