# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Read MNIST Dataset'
SRC_URI="http://cran.r-project.org/src/contrib/readmnist_1.0.6.tar.gz"
LICENSE='GPL-2+'
