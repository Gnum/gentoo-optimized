# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='R Interface to Geochemical Modeling Software'
SRC_URI="http://cran.r-project.org/src/contrib/phreeqc_3.4.9.tar.gz"
LICENSE='GPL-3'
