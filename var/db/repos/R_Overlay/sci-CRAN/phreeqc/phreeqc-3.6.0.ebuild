# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='R Interface to Geochemical Modeling Software'
SRC_URI="http://cran.r-project.org/src/contrib/phreeqc_3.6.0.tar.gz"
LICENSE='GPL-3'
