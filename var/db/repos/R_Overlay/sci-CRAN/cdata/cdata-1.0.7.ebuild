# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Fluid Data Transformations'
SRC_URI="http://cran.r-project.org/src/contrib/cdata_1.0.7.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_dbi r_suggests_knitr r_suggests_rqdatatable
	r_suggests_rsqlite r_suggests_runit"
R_SUGGESTS="
	r_suggests_dbi? ( sci-CRAN/DBI )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_rqdatatable? ( >=sci-CRAN/rqdatatable-1.1.4 )
	r_suggests_rsqlite? ( sci-CRAN/RSQLite )
	r_suggests_runit? ( sci-CRAN/RUnit )
"
DEPEND=">=dev-lang/R-3.4.0
	>=sci-CRAN/wrapr-1.8.4
	>=sci-CRAN/rquery-1.3.2
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
