# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='End-Member Modelling of Grain-Size Data'
SRC_URI="http://cran.r-project.org/src/contrib/EMMAgeo_0.9.6.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/limSolve
	>=dev-lang/R-3.2.1
	sci-CRAN/caTools
	sci-CRAN/shiny
	sci-CRAN/GPArotation
	virtual/Matrix
"
RDEPEND="${DEPEND-}"
