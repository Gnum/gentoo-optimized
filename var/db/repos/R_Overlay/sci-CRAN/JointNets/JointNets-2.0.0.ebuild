# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='End-to-End Sparse Gaussian Graph... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/JointNets_2.0.0.tar.gz"
LICENSE='GPL-2'

DEPEND=">=dev-lang/R-3.4.4
	sci-CRAN/lpSolve
	sci-CRAN/pcaPP
	sci-CRAN/brainR
	virtual/MASS
	sci-CRAN/igraph
	sci-CRAN/oro_nifti
	sci-CRAN/JGL
	sci-CRAN/rgl
	sci-CRAN/misc3d
	sci-CRAN/shiny
"
RDEPEND="${DEPEND-}"
