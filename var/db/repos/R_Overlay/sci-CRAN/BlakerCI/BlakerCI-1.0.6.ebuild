# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Blakers Binomial and Poisson Confidence Limits'
SRC_URI="http://cran.r-project.org/src/contrib/BlakerCI_1.0-6.tar.gz"
LICENSE='GPL-3'
