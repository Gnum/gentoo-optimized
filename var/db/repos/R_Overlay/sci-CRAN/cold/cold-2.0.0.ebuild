# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Count Longitudinal Data'
SRC_URI="http://cran.r-project.org/src/contrib/cold_2.0-0.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-3.1.0
	sci-CRAN/cubature
"
RDEPEND="${DEPEND-}"
