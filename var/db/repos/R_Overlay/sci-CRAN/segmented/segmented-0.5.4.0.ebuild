# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Regression Models with Break-Poi... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/segmented_0.5-4.0.tar.gz"
LICENSE='GPL-2+'
