# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Better ALignment CONsensus analYsis'
SRC_URI="http://cran.r-project.org/src/contrib/BALCONY_0.2.7.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/dplyr
	sci-CRAN/progress
	sci-CRAN/scales
	sci-CRAN/seqinr
	sci-CRAN/Rpdb
	sci-BIOC/Biostrings
	sci-CRAN/readr
"
RDEPEND="${DEPEND-}"
