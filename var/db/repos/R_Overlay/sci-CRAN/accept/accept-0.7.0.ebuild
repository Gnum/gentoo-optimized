# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='The Acute COPD Exacerbation Pred... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/accept_0.7.0.tar.gz"
LICENSE='GPL-3'

DEPEND="virtual/MASS
	>=dev-lang/R-3.4.0
	sci-CRAN/stringr
	sci-CRAN/dplyr
	sci-CRAN/extrafont
	sci-CRAN/viridis
	sci-CRAN/plotly
"
RDEPEND="${DEPEND-}"
