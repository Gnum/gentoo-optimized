# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Interface to REDCap'
SRC_URI="http://cran.r-project.org/src/contrib/redcapAPI_2.1.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/DBI
	sci-CRAN/stringr
	sci-CRAN/httr
	>=dev-lang/R-3.0.0
	sci-CRAN/tidyr
	sci-CRAN/checkmate
	sci-CRAN/labelVector
	sci-CRAN/chron
	sci-CRAN/lubridate
"
RDEPEND="${DEPEND-}"
