# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Fitting Nonlinear Models with Sc... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/nlsmsn_0.0-5.tar.gz"
LICENSE='GPL-3+'
