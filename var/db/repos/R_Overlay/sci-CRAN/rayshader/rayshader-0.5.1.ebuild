# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Create and Visualize Hillshaded ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/rayshader_0.5.1.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/doParallel
	sci-CRAN/raster
	sci-CRAN/magrittr
	sci-CRAN/scales
	sci-CRAN/png
	>=dev-lang/R-3.0.2
	sci-CRAN/progress
	sci-CRAN/foreach
	sci-CRAN/rgl
	sci-CRAN/imager
	sci-CRAN/Rcpp
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/progress
"
