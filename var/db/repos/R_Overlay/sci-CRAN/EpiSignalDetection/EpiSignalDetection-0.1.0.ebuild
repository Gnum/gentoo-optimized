# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Signal Detection Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/EpiSignalDetection_0.1.0.tar.gz"
LICENSE='EUPL-1.1'

DEPEND="sci-CRAN/ggplot2
	sci-CRAN/dplyr
	sci-CRAN/DT
	sci-CRAN/rmarkdown
	sci-CRAN/ISOweek
	sci-CRAN/pander
	sci-CRAN/surveillance
	sci-CRAN/shiny
	>=dev-lang/R-3.4.0
	>=sci-CRAN/knitr-1.20
"
RDEPEND="${DEPEND-}"
