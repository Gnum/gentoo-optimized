# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='API Client for the ClimMob Platform'
SRC_URI="http://cran.r-project.org/src/contrib/ClimMobTools_0.3.2.tar.gz"
LICENSE='MIT'

IUSE="${IUSE-} r_suggests_knitr r_suggests_plackettluce r_suggests_rmarkdown
	r_suggests_testthat"
R_SUGGESTS="
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_plackettluce? ( >=sci-CRAN/PlackettLuce-0.2.8 )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_testthat? ( >=sci-CRAN/testthat-2.1.0 )
"
DEPEND="sci-CRAN/jsonlite
	sci-CRAN/RSpectra
	sci-CRAN/httr
	sci-CRAN/climatrends
	>=dev-lang/R-3.5.0
	sci-CRAN/tibble
	virtual/Matrix
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
