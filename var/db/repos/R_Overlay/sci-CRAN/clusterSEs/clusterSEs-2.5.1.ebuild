# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Calculate Cluster-Robust p-Value... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/clusterSEs_2.5.1.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/mlogit
	sci-CRAN/plm
	sci-CRAN/AER
	sci-CRAN/lmtest
	>=dev-lang/R-3.3.0
	sci-CRAN/Formula
	sci-CRAN/sandwich
"
RDEPEND="${DEPEND-}"
