# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Dynamic Stochastic Block Models'
SRC_URI="http://cran.r-project.org/src/contrib/dynsbm_0.6.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/riverplot
	sci-CRAN/Rcpp
	sci-CRAN/RColorBrewer
"
RDEPEND="${DEPEND-} sci-CRAN/Rcpp"
