# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Instrumental Variables'
SRC_URI="http://cran.r-project.org/src/contrib/ivtools_2.2.0.tar.gz"
LICENSE='LGPL-3+'

DEPEND="sci-CRAN/numDeriv
	sci-CRAN/nleqslv
	sci-CRAN/data_table
	virtual/survival
	sci-CRAN/Rcpp
	sci-CRAN/ahaz
"
RDEPEND="${DEPEND-} sci-CRAN/Rcpp"
