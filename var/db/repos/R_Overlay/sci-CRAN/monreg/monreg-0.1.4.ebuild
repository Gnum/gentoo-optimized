# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Nonparametric Monotone Regression'
SRC_URI="http://cran.r-project.org/src/contrib/monreg_0.1.4.tar.gz"
LICENSE='GPL-2+'
