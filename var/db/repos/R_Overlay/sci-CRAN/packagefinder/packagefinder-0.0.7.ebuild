# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Comfortable Search for R Packages on CRAN'
SRC_URI="http://cran.r-project.org/src/contrib/packagefinder_0.0.7.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/jsonlite
	sci-CRAN/pander
	sci-CRAN/httr
	sci-CRAN/formattable
	sci-CRAN/stringr
"
RDEPEND="${DEPEND-}"
