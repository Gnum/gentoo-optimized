# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Create Rich Command Line Applications'
SRC_URI="http://cran.r-project.org/src/contrib/cliapp_0.1.0.tar.gz"
LICENSE='MIT'

IUSE="${IUSE-} r_suggests_callr r_suggests_covr r_suggests_rstudioapi
	r_suggests_testthat"
R_SUGGESTS="
	r_suggests_callr? ( sci-CRAN/callr )
	r_suggests_covr? ( sci-CRAN/covr )
	r_suggests_rstudioapi? ( sci-CRAN/rstudioapi )
	r_suggests_testthat? ( sci-CRAN/testthat )
"
DEPEND="sci-CRAN/crayon
	>=sci-CRAN/progress-1.2.0
	sci-CRAN/cli
	sci-CRAN/withr
	sci-CRAN/prettycode
	sci-CRAN/fansi
	>=sci-CRAN/glue-1.3.0
	sci-CRAN/xml2
	sci-CRAN/R6
	sci-CRAN/selectr
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
