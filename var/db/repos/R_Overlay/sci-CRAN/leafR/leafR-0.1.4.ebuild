# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Calculates the Leaf Area Index (... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/leafR_0.1.4.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/lidR
	sci-CRAN/sp
	sci-CRAN/raster
	sci-CRAN/lazyeval
	sci-CRAN/data_table
"
RDEPEND="${DEPEND-}"
