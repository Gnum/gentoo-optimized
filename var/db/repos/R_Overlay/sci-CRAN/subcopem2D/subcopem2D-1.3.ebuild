# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Bivariate Empirical Subcopula'
SRC_URI="http://cran.r-project.org/src/contrib/subcopem2D_1.3.tar.gz"
LICENSE='GPL-3'
