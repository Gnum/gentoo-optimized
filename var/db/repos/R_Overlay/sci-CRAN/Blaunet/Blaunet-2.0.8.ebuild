# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Calculate and Analyze Blau Statu... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/Blaunet_2.0.8.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/network
	>=dev-lang/R-3.0.0
	sci-CRAN/plot3D
	sci-CRAN/haven
	sci-CRAN/gWidgets
	sci-CRAN/ergm
	sci-CRAN/gWidgetsRGtk2
	sci-CRAN/statnet_common
	sci-CRAN/plot3Drgl
	sci-CRAN/rgl
	sci-CRAN/sna
	virtual/foreign
	sci-CRAN/RGtk2
	sci-CRAN/cairoDevice
"
RDEPEND="${DEPEND-}"
