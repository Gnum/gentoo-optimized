# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Latent Variable Models for Networks'
SRC_URI="http://cran.r-project.org/src/contrib/lvm4net_0.2.1.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/igraph
	sci-CRAN/ergm
	>=dev-lang/R-3.1.1
	virtual/MASS
	sci-CRAN/network
	sci-CRAN/ellipse
"
RDEPEND="${DEPEND-}"
