# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Datasets for Stat2'
SRC_URI="http://cran.r-project.org/src/contrib/Stat2Data_2.0.0.tar.gz"
LICENSE='GPL-3'
