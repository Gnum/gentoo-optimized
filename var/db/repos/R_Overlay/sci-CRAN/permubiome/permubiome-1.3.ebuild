# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='A Permutation Based Test for Bio... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/permubiome_1.3.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/ggplot2
	sci-CRAN/dabestr
	sci-CRAN/gridExtra
	virtual/Matrix
"
RDEPEND="${DEPEND-}"
