# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Centrality-Based Pathway Enrichment'
SRC_URI="http://cran.r-project.org/src/contrib/CePa_0.7.0.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=sci-CRAN/igraph-0.6
	sci-BIOC/Rgraphviz
	>=dev-lang/R-3.0.0
	sci-BIOC/graph
"
RDEPEND="${DEPEND-}"
