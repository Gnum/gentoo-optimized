# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Estimation of Life Expectancies ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/elect_1.1.3.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/msm
	virtual/nnet
"
RDEPEND="${DEPEND-}"
