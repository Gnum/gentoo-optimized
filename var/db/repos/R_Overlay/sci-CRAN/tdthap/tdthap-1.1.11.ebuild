# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='TDT Tests for Extended Haplotypes'
SRC_URI="http://cran.r-project.org/src/contrib/tdthap_1.1-11.tar.gz"
LICENSE='Artistic-2'
