# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Compositional Data Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/Compositional_3.1.tar.gz"
LICENSE='GPL-2+'

DEPEND="virtual/MASS
	sci-CRAN/fields
	sci-CRAN/mixture
	>=dev-lang/R-3.2.2
	sci-CRAN/sn
	sci-CRAN/doParallel
	sci-CRAN/foreach
	sci-CRAN/emplik
	sci-CRAN/Rfast
"
RDEPEND="${DEPEND-}"
