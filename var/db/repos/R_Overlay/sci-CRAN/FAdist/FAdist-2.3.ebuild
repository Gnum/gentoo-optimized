# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Distributions that are Sometimes Used in Hydrology'
SRC_URI="http://cran.r-project.org/src/contrib/FAdist_2.3.tar.gz"
LICENSE='GPL-2'
