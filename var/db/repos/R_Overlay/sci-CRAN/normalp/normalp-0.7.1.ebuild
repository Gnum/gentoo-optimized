# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Routines for Exponential Power Distribution'
SRC_URI="http://cran.r-project.org/src/contrib/normalp_0.7.1.tar.gz"
LICENSE='GPL-2+'
