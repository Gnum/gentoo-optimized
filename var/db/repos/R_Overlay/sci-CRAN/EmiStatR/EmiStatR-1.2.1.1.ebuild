# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Emissions and Statistics in R fo... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/EmiStatR_1.2.1.1.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/foreach
	virtual/lattice
	sci-CRAN/zoo
	sci-CRAN/doParallel
	sci-CRAN/xts
	sci-CRAN/shiny
"
RDEPEND="${DEPEND-}"
