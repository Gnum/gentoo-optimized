# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Emissions and Statistics in R fo... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/EmiStatR_1.2.1.2.tar.gz"
LICENSE='GPL-3+'

DEPEND="virtual/lattice
	sci-CRAN/doParallel
	sci-CRAN/foreach
	sci-CRAN/xts
	sci-CRAN/shiny
	sci-CRAN/zoo
"
RDEPEND="${DEPEND-}"
