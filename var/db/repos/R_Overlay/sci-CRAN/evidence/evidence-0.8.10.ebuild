# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Analysis of Scientific Evidence ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/evidence_0.8.10.tar.gz"
LICENSE='GPL-2+'

DEPEND="virtual/lattice
	sci-CRAN/LaplacesDemon
	sci-CRAN/rstanarm
	sci-CRAN/rstan
	sci-CRAN/loo
	sci-CRAN/LearnBayes
"
RDEPEND="${DEPEND-}"
