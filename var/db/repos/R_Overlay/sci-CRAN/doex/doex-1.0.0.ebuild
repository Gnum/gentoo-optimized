# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='One-Way Heteroscedastic ANOVA Tests'
SRC_URI="http://cran.r-project.org/src/contrib/doex_1.0.0.tar.gz"
LICENSE='GPL-2+'
