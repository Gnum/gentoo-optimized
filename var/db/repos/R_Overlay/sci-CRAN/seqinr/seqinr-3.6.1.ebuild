# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Biological Sequences Retrieval and Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/seqinr_3.6-1.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/ade4
	sci-CRAN/segmented
"
RDEPEND="${DEPEND-}"
