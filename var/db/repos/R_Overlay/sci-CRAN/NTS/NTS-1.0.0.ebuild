# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Nonlinear Time Series Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/NTS_1.0.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/tensor
	virtual/MASS
	sci-CRAN/dlm
	sci-CRAN/Rdpack
	>=dev-lang/R-3.5.0
	sci-CRAN/MSwM
"
RDEPEND="${DEPEND-}"
