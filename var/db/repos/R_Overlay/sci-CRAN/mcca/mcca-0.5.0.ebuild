# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Multi-Category Classification Accuracy'
SRC_URI="http://cran.r-project.org/src/contrib/mcca_0.5.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="virtual/MASS
	virtual/nnet
	sci-CRAN/pROC
	virtual/rpart
	sci-CRAN/e1071
	sci-CRAN/caret
"
RDEPEND="${DEPEND-}"
