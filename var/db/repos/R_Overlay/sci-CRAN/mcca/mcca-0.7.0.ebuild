# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Multi-Category Classification Accuracy'
SRC_URI="http://cran.r-project.org/src/contrib/mcca_0.7.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/e1071
	virtual/MASS
	virtual/nnet
	sci-CRAN/rgl
	virtual/rpart
	sci-CRAN/pROC
	sci-CRAN/caret
"
RDEPEND="${DEPEND-}"
