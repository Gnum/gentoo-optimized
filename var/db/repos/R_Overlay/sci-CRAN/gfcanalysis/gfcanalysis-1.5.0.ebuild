# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Tools for Working with Hansen et... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/gfcanalysis_1.5.0.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/rasterVis
	sci-CRAN/raster
	sci-CRAN/rgdal
	sci-CRAN/sp
	sci-CRAN/plyr
	sci-omegahat/RCurl
	sci-CRAN/animation
	sci-CRAN/stringr
	sci-CRAN/rgeos
	sci-CRAN/geosphere
	sci-CRAN/ggplot2
"
RDEPEND="${DEPEND-}"
