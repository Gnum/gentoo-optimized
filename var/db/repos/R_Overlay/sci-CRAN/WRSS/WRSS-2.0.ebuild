# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Water Resources System Simulator'
SRC_URI="http://cran.r-project.org/src/contrib/WRSS_2.0.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/nloptr
	>=dev-lang/R-3.0.0
	sci-CRAN/ggplot2
	sci-CRAN/GGally
	sci-CRAN/network
	sci-CRAN/Hmisc
"
RDEPEND="${DEPEND-}"
