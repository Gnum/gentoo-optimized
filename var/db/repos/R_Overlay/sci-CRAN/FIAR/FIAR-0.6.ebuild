# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Functional Integration Analysis in R'
SRC_URI="http://cran.r-project.org/src/contrib/FIAR_0.6.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/lavaan
	virtual/Matrix
	sci-CRAN/np
"
RDEPEND="${DEPEND-}"
