# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Damped Anderson Acceleration wit... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/daarem_0.2.tar.gz"
LICENSE='GPL-2'
