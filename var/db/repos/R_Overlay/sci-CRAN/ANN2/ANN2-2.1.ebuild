# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Artificial Neural Networks for Anomaly Detection'
SRC_URI="http://cran.r-project.org/src/contrib/ANN2_2.1.tar.gz"
LICENSE='GPL-3+'

DEPEND=">=sci-CRAN/Rcpp-0.12.18
	>=sci-CRAN/reshape2-1.4.3
	>=sci-CRAN/ggplot2-3.0.0
	>=sci-CRAN/viridisLite-0.3.0
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
	sci-CRAN/Rcereal
"
