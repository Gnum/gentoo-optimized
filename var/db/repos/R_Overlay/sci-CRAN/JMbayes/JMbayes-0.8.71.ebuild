# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Joint Modeling of Longitudinal a... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/JMbayes_0.8-71.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/jagsUI
	sci-CRAN/xtable
	virtual/MASS
	sci-CRAN/shiny
	virtual/survival
	virtual/nlme
	sci-CRAN/Rcpp
	sci-CRAN/rstan
	sci-CRAN/foreach
	sci-CRAN/Hmisc
	sci-CRAN/doParallel
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
	sci-mathematics/jags
"
