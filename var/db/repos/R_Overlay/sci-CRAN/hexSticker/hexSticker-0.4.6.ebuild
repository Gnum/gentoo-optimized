# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Create Hexagon Sticker in R'
SRC_URI="http://cran.r-project.org/src/contrib/hexSticker_0.4.6.tar.gz"
LICENSE='Artistic-2'

DEPEND="sci-CRAN/ggimage
	>=dev-lang/R-3.4.0
	sci-CRAN/showtext
	sci-CRAN/ggplot2
	sci-CRAN/hexbin
	sci-CRAN/sysfonts
"
RDEPEND="${DEPEND-}"
