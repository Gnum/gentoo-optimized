# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Create Hexagon Sticker in R'
SRC_URI="http://cran.r-project.org/src/contrib/hexSticker_0.4.5.tar.gz"
LICENSE='Artistic-2'

DEPEND="sci-CRAN/sysfonts
	>=dev-lang/R-3.4.0
	sci-CRAN/ggimage
	sci-CRAN/hexbin
	sci-CRAN/showtext
	sci-CRAN/ggplot2
"
RDEPEND="${DEPEND-}"
