# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='PX-Web Data by API'
SRC_URI="http://cran.r-project.org/src/contrib/PxWebApiData_0.2.0.tar.gz"
LICENSE='Apache-2.0'

DEPEND=">=dev-lang/R-3.0.0
	sci-CRAN/httr
	sci-CRAN/rjstat
	sci-CRAN/jsonlite
"
RDEPEND="${DEPEND-}"
