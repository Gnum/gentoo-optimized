# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='A Lasso for Hierarchical Interactions'
SRC_URI="http://cran.r-project.org/src/contrib/hierNet_1.9.tar.gz"
LICENSE='GPL-2'
