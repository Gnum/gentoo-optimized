# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Unconstrained Optimization using... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/subplex_1.6.tar.gz"
LICENSE='GPL-3'
