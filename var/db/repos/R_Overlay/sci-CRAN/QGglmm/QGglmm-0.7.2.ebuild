# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Estimate Quantitative Genetics P... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/QGglmm_0.7.2.tar.gz"
LICENSE='GPL-2'

DEPEND=">=sci-CRAN/cubature-1.4"
RDEPEND="${DEPEND-}"
