# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Methods for Quantitative Magneti... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/qMRI_1.0.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=sci-CRAN/awsMethods-1.0
	>=dev-lang/R-3.5
	>=sci-CRAN/dti-1.4
	>=sci-CRAN/adimpro-0.9
	>=sci-CRAN/oro_nifti-0.9
	sci-CRAN/stringr
	>=sci-CRAN/aws-2.2
"
RDEPEND="${DEPEND-}"
