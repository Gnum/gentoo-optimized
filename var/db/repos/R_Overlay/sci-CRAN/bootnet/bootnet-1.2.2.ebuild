# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Bootstrap Methods for Various Ne... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/bootnet_1.2.2.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/pbapply
	sci-CRAN/relaimpo
	sci-CRAN/BDgraph
	sci-CRAN/graphicalVAR
	sci-CRAN/IsingSampler
	>=dev-lang/R-3.0.0
	>=sci-CRAN/dplyr-0.3.0.2
	sci-CRAN/IsingFit
	sci-CRAN/gtools
	sci-CRAN/mvtnorm
	sci-CRAN/igraph
	sci-CRAN/lavaan
	sci-CRAN/networktools
	sci-CRAN/huge
	sci-CRAN/qgraph
	sci-CRAN/parcor
	virtual/Matrix
	sci-CRAN/glasso
	sci-CRAN/psych
	>=sci-CRAN/mgm-1.2
	sci-CRAN/ggplot2
	sci-CRAN/abind
	>=sci-CRAN/NetworkToolbox-1.1.0
	sci-CRAN/corpcor
	sci-CRAN/tidyr
"
RDEPEND="${DEPEND-}"
