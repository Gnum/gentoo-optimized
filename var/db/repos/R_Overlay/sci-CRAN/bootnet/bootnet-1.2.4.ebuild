# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Bootstrap Methods for Various Ne... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/bootnet_1.2.4.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/psychTools
	>=sci-CRAN/NetworkToolbox-1.1.0
	sci-CRAN/IsingFit
	sci-CRAN/gtools
	sci-CRAN/igraph
	>=sci-CRAN/mgm-1.2
	virtual/Matrix
	sci-CRAN/huge
	>=sci-CRAN/dplyr-0.3.0.2
	sci-CRAN/relaimpo
	sci-CRAN/corpcor
	sci-CRAN/tidyr
	sci-CRAN/pbapply
	sci-CRAN/qgraph
	sci-CRAN/BDgraph
	sci-CRAN/graphicalVAR
	sci-CRAN/mvtnorm
	sci-CRAN/ggplot2
	sci-CRAN/networktools
	sci-CRAN/parcor
	sci-CRAN/glasso
	>=dev-lang/R-3.0.0
	sci-CRAN/abind
	sci-CRAN/IsingSampler
	sci-CRAN/lavaan
"
RDEPEND="${DEPEND-}"
