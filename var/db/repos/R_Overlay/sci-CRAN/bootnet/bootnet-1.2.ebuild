# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Bootstrap Methods for Various Ne... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/bootnet_1.2.tar.gz"
LICENSE='GPL-2'

DEPEND=">=sci-CRAN/mgm-1.2
	sci-CRAN/relaimpo
	sci-CRAN/ggplot2
	>=sci-CRAN/NetworkToolbox-1.1.0
	sci-CRAN/abind
	sci-CRAN/igraph
	sci-CRAN/pbapply
	sci-CRAN/IsingFit
	sci-CRAN/psych
	sci-CRAN/networktools
	sci-CRAN/qgraph
	sci-CRAN/IsingSampler
	sci-CRAN/corpcor
	>=sci-CRAN/dplyr-0.3.0.2
	sci-CRAN/gtools
	sci-CRAN/tidyr
	sci-CRAN/BDgraph
	sci-CRAN/lavaan
	sci-CRAN/mvtnorm
	>=dev-lang/R-3.0.0
	sci-CRAN/graphicalVAR
	virtual/Matrix
	sci-CRAN/parcor
	sci-CRAN/huge
"
RDEPEND="${DEPEND-}"
