# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Bootstrap Methods for Various Ne... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/bootnet_1.1.0.tar.gz"
LICENSE='GPL-2'

DEPEND=">=sci-CRAN/mgm-1.2
	sci-CRAN/ggplot2
	sci-CRAN/corpcor
	sci-CRAN/gtools
	sci-CRAN/pbapply
	sci-CRAN/parcor
	sci-CRAN/IsingFit
	sci-CRAN/BDgraph
	sci-CRAN/psych
	sci-CRAN/mvtnorm
	sci-CRAN/IsingSampler
	sci-CRAN/abind
	sci-CRAN/qgraph
	>=dev-lang/R-3.0.0
	sci-CRAN/graphicalVAR
	virtual/Matrix
	>=sci-CRAN/NetworkToolbox-1.1.0
	sci-CRAN/relaimpo
	sci-CRAN/huge
	>=sci-CRAN/dplyr-0.3.0.2
	sci-CRAN/igraph
	sci-CRAN/tidyr
"
RDEPEND="${DEPEND-}"
