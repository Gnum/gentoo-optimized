# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Reproducibility of Gene Expression Clusters'
SRC_URI="http://cran.r-project.org/src/contrib/clusterRepro_0.9.tar.gz"
LICENSE='GPL-2'
