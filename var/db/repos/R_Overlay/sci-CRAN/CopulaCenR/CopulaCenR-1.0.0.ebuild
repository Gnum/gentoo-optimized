# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Copula-Based Regression Models f... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/CopulaCenR_1.0.0.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/pracma
	sci-CRAN/caret
	sci-CRAN/corpcor
	sci-CRAN/icenReg
	sci-CRAN/copula
	virtual/survival
	sci-CRAN/magrittr
	sci-CRAN/plotly
	sci-CRAN/flexsurv
"
RDEPEND="${DEPEND-}"
