# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Copula-Based Regression Models f... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/CopulaCenR_0.1.0.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/magrittr
	sci-CRAN/plotly
	sci-CRAN/icenReg
	virtual/survival
	sci-CRAN/pracma
	sci-CRAN/corpcor
	sci-CRAN/copula
	sci-CRAN/flexsurv
"
RDEPEND="${DEPEND-}"
