# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='R Interface to the Giphy API'
SRC_URI="http://cran.r-project.org/src/contrib/giphyr_0.1.3.tar.gz"
LICENSE='MIT'

DEPEND="sci-CRAN/httr
	sci-CRAN/dplyr
	sci-CRAN/miniUI
	sci-CRAN/shiny
	sci-CRAN/purrr
	>=dev-lang/R-3.2.2
	sci-CRAN/rstudioapi
	sci-CRAN/tibble
"
RDEPEND="${DEPEND-}"
