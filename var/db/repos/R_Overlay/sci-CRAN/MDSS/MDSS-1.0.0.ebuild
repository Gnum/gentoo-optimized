# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Modeling Human Dentin Serial Sectioning'
SRC_URI="http://cran.r-project.org/src/contrib/MDSS_1.0-0.tar.gz"
LICENSE='GPL-3+'
