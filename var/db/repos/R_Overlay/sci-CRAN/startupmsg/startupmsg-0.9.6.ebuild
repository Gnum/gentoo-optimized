# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Utilities for Start-Up Messages'
SRC_URI="http://cran.r-project.org/src/contrib/startupmsg_0.9.6.tar.gz"
LICENSE='LGPL-3'
