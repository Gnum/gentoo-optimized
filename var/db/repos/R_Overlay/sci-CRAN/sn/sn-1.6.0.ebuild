# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='The Skew-Normal and Related Dist... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/sn_1.6-0.tar.gz"
LICENSE='|| ( GPL-2 GPL-3 )'

DEPEND="sci-CRAN/numDeriv
	>=sci-CRAN/mnormt-1.5.4
	sci-CRAN/quantreg
	>=dev-lang/R-2.15.3
"
RDEPEND="${DEPEND-}"
