# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Fishing Effort Standardisation'
SRC_URI="http://cran.r-project.org/src/contrib/FESta_1.0.0.tar.gz"
LICENSE='GPL-2+'
