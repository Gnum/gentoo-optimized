# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Radiation Safety'
SRC_URI="http://cran.r-project.org/src/contrib/radsafer_2.0.0.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_beepr r_suggests_scatterplot3d r_suggests_testthat
	r_suggests_tidyverse"
R_SUGGESTS="
	r_suggests_beepr? ( sci-CRAN/beepr )
	r_suggests_scatterplot3d? ( sci-CRAN/scatterplot3d )
	r_suggests_testthat? ( sci-CRAN/testthat )
	r_suggests_tidyverse? ( sci-CRAN/tidyverse )
"
DEPEND="sci-CRAN/magrittr
	sci-CRAN/readr
	sci-CRAN/stringr
	sci-CRAN/ggplot2
	>=dev-lang/R-3.3
	sci-CRAN/rlang
	sci-CRAN/dplyr
	sci-CRAN/RadData
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
