# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Tcl/Tk Quick and Simple Function GUI'
SRC_URI="http://cran.r-project.org/src/contrib/miniGUI_0.8-1.tar.gz"
