# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Asio C++ Header Files'
SRC_URI="http://cran.r-project.org/src/contrib/AsioHeaders_1.12.2-1.tar.gz"
LICENSE='Boost-1.0'
