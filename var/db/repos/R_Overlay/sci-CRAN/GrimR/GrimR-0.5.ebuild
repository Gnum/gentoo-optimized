# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Calculate Optical Parameters fro... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/GrimR_0.5.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/car"
RDEPEND="${DEPEND-}"
