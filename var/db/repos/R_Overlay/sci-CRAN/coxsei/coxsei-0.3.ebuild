# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Fitting a CoxSEI Model'
SRC_URI="http://cran.r-project.org/src/contrib/coxsei_0.3.tar.gz"
LICENSE='GPL-2+'
