# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Pena-Yohai Initial Estimator for... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/pyinit_1.0.3.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/robustbase"
RDEPEND="${DEPEND-}"
