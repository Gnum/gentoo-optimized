# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Read Data Stored by Minitab, S, ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/foreign_0.8-79.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-4.0.0
	dev-lang/R[minimal]
"
RDEPEND="${DEPEND-}"
