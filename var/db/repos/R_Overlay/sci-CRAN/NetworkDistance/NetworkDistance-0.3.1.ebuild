# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Distance Measures for Networks'
SRC_URI="http://cran.r-project.org/src/contrib/NetworkDistance_0.3.1.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/igraph
	virtual/Matrix
	sci-CRAN/RSpectra
	sci-CRAN/Rcpp
	>=dev-lang/R-3.0.0
	sci-CRAN/Rdpack
	sci-CRAN/foreach
	sci-CRAN/pracma
	sci-CRAN/CovTools
	sci-CRAN/network
	sci-CRAN/doParallel
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
"
