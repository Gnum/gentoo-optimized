# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Distance Measures for Networks'
SRC_URI="http://cran.r-project.org/src/contrib/NetworkDistance_0.3.0.tar.gz"
LICENSE='GPL-3+'

DEPEND="virtual/Matrix
	sci-CRAN/RSpectra
	sci-CRAN/doParallel
	sci-CRAN/Rdpack
	sci-CRAN/Rcpp
	sci-CRAN/igraph
	sci-CRAN/foreach
	sci-CRAN/network
	sci-CRAN/pracma
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
"
