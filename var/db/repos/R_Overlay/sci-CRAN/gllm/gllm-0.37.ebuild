# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Generalised log-Linear Model'
SRC_URI="http://cran.r-project.org/src/contrib/gllm_0.37.tar.gz"
LICENSE='|| ( GPL-2 GPL-3 )'
