# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Integrated Text Mining Solution'
SRC_URI="http://cran.r-project.org/src/contrib/R.temis_0.1.1.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/NLP
	sci-CRAN/tm_plugin_factiva
	sci-CRAN/wordcloud
	sci-CRAN/FactoMineR
	sci-CRAN/tm_plugin_europresse
	sci-CRAN/SnowballC
	sci-CRAN/tm_plugin_lexisnexis
	sci-CRAN/slam
	sci-CRAN/testthat
	>=sci-CRAN/tm-0.6
	sci-CRAN/crayon
	sci-CRAN/stringi
	sci-CRAN/igraph
	sci-CRAN/tm_plugin_alceste
	sci-CRAN/explor
"
RDEPEND="${DEPEND-}"
