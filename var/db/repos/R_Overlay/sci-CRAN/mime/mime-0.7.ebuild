# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Map Filenames to MIME Types'
SRC_URI="http://cran.r-project.org/src/contrib/mime_0.7.tar.gz"
LICENSE='GPL-2+'
