# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Latent Markov Models with and without Covariates'
SRC_URI="http://cran.r-project.org/src/contrib/LMest_2.4.5.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/mmm
	virtual/MASS
	sci-CRAN/mix
	sci-CRAN/mvtnorm
	sci-CRAN/MultiLCIRT
"
RDEPEND="${DEPEND-}"
