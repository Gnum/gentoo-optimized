# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Deterministic Blockmodeling of S... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/dBlockmodeling_0.2.0.tar.gz"
LICENSE='GPL-2+'
