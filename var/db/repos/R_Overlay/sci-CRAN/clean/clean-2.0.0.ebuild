# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Fast and Easy Data Cleaning'
SRC_URI="http://cran.r-project.org/src/contrib/clean_2.0.0.tar.gz"
LICENSE='GPL-2'

DEPEND=">=dev-lang/R-3.0.0
	>=sci-CRAN/cleaner-1.2.0
"
RDEPEND="${DEPEND-}"
