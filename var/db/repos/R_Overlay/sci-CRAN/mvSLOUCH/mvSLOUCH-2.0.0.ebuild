# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Multivariate Stochastic Linear O... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/mvSLOUCH_2.0.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/ouch
	sci-CRAN/mvtnorm
	>=dev-lang/R-3.1.2
	sci-CRAN/PCMBase
	sci-CRAN/abind
	virtual/Matrix
	virtual/Matrix
	sci-CRAN/ape
"
RDEPEND="${DEPEND-}"
