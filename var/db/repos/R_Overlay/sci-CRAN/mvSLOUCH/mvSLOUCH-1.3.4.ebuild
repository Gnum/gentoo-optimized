# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Multivariate Stochastic Linear O... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/mvSLOUCH_1.3.4.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/ouch
	sci-CRAN/corpcor
	sci-CRAN/mvtnorm
	sci-CRAN/ape
	sci-CRAN/numDeriv
"
RDEPEND="${DEPEND-}"
