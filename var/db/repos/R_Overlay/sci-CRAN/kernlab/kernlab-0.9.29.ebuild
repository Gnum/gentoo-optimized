# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Kernel-Based Machine Learning Lab'
SRC_URI="http://cran.r-project.org/src/contrib/kernlab_0.9-29.tar.gz"
LICENSE='GPL-2'
