# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Summarizing OTU Table Regarding ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/otuSummary_0.1.0.tar.gz"
LICENSE='GPL-3+'

DEPEND=">=dev-lang/R-3.1.0
	>=sci-CRAN/vegan-2.0.7
	>=sci-CRAN/reshape2-1.4
	>=sci-CRAN/fossil-0.3.7
	>=sci-CRAN/reldist-1.6.6
"
RDEPEND="${DEPEND-}"
