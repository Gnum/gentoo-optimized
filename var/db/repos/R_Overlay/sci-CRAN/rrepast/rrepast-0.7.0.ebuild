# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Invoke Repast Simphony Simulation Models'
SRC_URI="http://cran.r-project.org/src/contrib/rrepast_0.7.0.tar.gz"
LICENSE='MIT'

DEPEND="sci-CRAN/xlsx
	sci-CRAN/sensitivity
	sci-CRAN/digest
	sci-CRAN/foreach
	sci-CRAN/lhs
	sci-CRAN/ggplot2
	sci-CRAN/doParallel
	sci-CRAN/gridExtra
	sci-CRAN/rJava
"
RDEPEND="${DEPEND-}"
