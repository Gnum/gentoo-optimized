# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Finding Feedback Effects in SEM ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/SIRE_1.0.2.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/igraph
	sci-CRAN/stringr
	sci-CRAN/systemfit
	virtual/MASS
	sci-CRAN/numDeriv
	virtual/Matrix
	virtual/Matrix
	sci-CRAN/psych
	>=dev-lang/R-3.1.0
"
RDEPEND="${DEPEND-}"
