# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Simulation Tools for Planning Vitamin D Studies'
SRC_URI="http://cran.r-project.org/src/contrib/SimVitD_0.1.2.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/poisson"
RDEPEND="${DEPEND-}"
