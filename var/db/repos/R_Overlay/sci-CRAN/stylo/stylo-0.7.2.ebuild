# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Stylometric Multivariate Analyses'
SRC_URI="http://cran.r-project.org/src/contrib/stylo_0.7.2.tar.gz"
LICENSE='GPL-3+'

IUSE="${IUSE-} r_suggests_networkd3 r_suggests_readr r_suggests_stringi"
R_SUGGESTS="
	r_suggests_networkd3? ( sci-CRAN/networkD3 )
	r_suggests_readr? ( sci-CRAN/readr )
	r_suggests_stringi? ( sci-CRAN/stringi )
"
DEPEND="dev-lang/R[tk]
	virtual/class
	sci-CRAN/ape
	sci-CRAN/tsne
	sci-CRAN/pamr
	sci-CRAN/e1071
	dev-lang/R[tk]
	>=dev-lang/R-2.14
	virtual/lattice
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
