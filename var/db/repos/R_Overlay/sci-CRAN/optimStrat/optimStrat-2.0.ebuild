# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Choosing the Sample Strategy'
SRC_URI="http://cran.r-project.org/src/contrib/optimStrat_2.0.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/shiny
	sci-CRAN/mvtnorm
	sci-CRAN/cubature
"
RDEPEND="${DEPEND-}"
