# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Detect Small Changes in Process ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/vMask_1.0.tar.gz"
LICENSE='LGPL-3+'
