# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Infers Novel Immunoglobulin Alle... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/tigger_0.3.0.tar.gz"

IUSE="${IUSE-} r_suggests_knitr r_suggests_testthat"
R_SUGGESTS="
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_testthat? ( sci-CRAN/testthat )
"
DEPEND="sci-CRAN/stringi
	>=sci-CRAN/ggplot2-2.0.0
	sci-CRAN/lazyeval
	sci-CRAN/foreach
	>=sci-CRAN/shazam-0.1.10
	>=sci-CRAN/alakazam-0.2.11
	sci-CRAN/gridExtra
	>=sci-CRAN/dplyr-0.7.0
	sci-CRAN/doParallel
	>=dev-lang/R-3.2.5
	sci-CRAN/rlang
	sci-CRAN/iterators
	sci-CRAN/gtools
	sci-CRAN/tidyr
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
