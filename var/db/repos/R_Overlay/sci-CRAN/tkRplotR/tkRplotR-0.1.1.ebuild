# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Display Resizable Plots'
SRC_URI="http://cran.r-project.org/src/contrib/tkRplotR_0.1.1.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-3.3
	dev-lang/R[tk]
"
RDEPEND="${DEPEND-} dev-lang/R[tk]"
