# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Estimating Dark Diversity and Si... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/DarkDiv_0.2.0.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_testthat"
R_SUGGESTS="r_suggests_testthat? ( sci-CRAN/testthat )"
DEPEND="sci-CRAN/vegan"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
