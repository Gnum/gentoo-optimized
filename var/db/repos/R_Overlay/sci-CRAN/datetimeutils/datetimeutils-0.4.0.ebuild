# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Utilities for Dates and Times'
SRC_URI="http://cran.r-project.org/src/contrib/datetimeutils_0.4-0.tar.gz"
LICENSE='GPL-3'
