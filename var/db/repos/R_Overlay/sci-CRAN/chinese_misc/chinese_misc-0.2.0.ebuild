# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Miscellaneous Tools for Chinese ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/chinese.misc_0.2.0.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/NLP
	>=dev-lang/R-3.3.0
	virtual/Matrix
	sci-CRAN/purrr
	sci-CRAN/jiebaR
	sci-CRAN/stringi
	>=sci-CRAN/tm-0.7
	>=sci-CRAN/slam-0.1.37
"
RDEPEND="${DEPEND-}"
