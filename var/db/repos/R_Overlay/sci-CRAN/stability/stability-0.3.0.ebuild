# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Stability Analysis of Genotype b... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/stability_0.3.0.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/dplyr
	sci-CRAN/magrittr
	sci-CRAN/lme4
	sci-CRAN/scales
	sci-CRAN/ggfortify
	sci-CRAN/tidyr
	sci-CRAN/rlang
	sci-CRAN/ggplot2
	sci-CRAN/reshape2
	>=dev-lang/R-3.1
	virtual/Matrix
	sci-CRAN/tibble
"
RDEPEND="${DEPEND-}"
