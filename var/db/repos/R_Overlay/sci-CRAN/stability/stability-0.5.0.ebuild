# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Stability Analysis of Genotype b... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/stability_0.5.0.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/tibble
	sci-CRAN/dplyr
	sci-CRAN/rlang
	sci-CRAN/tidyr
	sci-CRAN/magrittr
	sci-CRAN/ggfortify
	sci-CRAN/ggplot2
	sci-CRAN/scales
	virtual/Matrix
	>=dev-lang/R-3.1
	sci-CRAN/lme4
	sci-CRAN/reshape2
"
RDEPEND="${DEPEND-}"
