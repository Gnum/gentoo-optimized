# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Graphic Devices Based on AGG'
SRC_URI="http://cran.r-project.org/src/contrib/ragg_0.1.2.tar.gz"
LICENSE='MIT'

DEPEND="sci-CRAN/systemfonts"
RDEPEND="${DEPEND-} media-libs/libpng"
