# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Graphic Devices Based on AGG'
SRC_URI="http://cran.r-project.org/src/contrib/ragg_0.1.4.tar.gz"
LICENSE='MIT'

DEPEND="sci-CRAN/systemfonts"
RDEPEND="${DEPEND-} media-libs/libpng"
