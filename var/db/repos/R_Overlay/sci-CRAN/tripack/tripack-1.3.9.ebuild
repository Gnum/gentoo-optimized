# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Triangulation of Irregularly Spaced Data'
SRC_URI="http://cran.r-project.org/src/contrib/tripack_1.3-9.tar.gz"
