# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Classes and Methods for Lagged Objects'
SRC_URI="http://cran.r-project.org/src/contrib/lagged_0.2-0.tar.gz"
LICENSE='GPL-2+'
