# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='MSSQL Querying using R'
SRC_URI="http://cran.r-project.org/src/contrib/mssqlR_1.0.0.tar.gz"
LICENSE='GPL-3+'

DEPEND=">=dev-lang/R-3.4.0
	sci-CRAN/RODBC
	sci-CRAN/magrittr
"
RDEPEND="${DEPEND-}"
