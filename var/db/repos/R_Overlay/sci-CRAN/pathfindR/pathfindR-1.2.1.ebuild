# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Pathway Enrichment Analysis Util... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/pathfindR_1.2.1.tar.gz"
LICENSE='MIT'

DEPEND="sci-BIOC/org_Hs_eg_db
	sci-CRAN/ggplot2
	sci-CRAN/fpc
	sci-CRAN/shiny
	sci-CRAN/DBI
	>=dev-lang/R-3.4
	sci-CRAN/doParallel
	sci-CRAN/foreach
	sci-CRAN/knitr
	sci-CRAN/rmarkdown
	sci-BIOC/pathview
	sci-BIOC/AnnotationDbi
"
RDEPEND="${DEPEND-} virtual/jdk"
