# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Pathway Enrichment Analysis Util... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/pathfindR_1.3.0.tar.gz"
LICENSE='MIT'

DEPEND="sci-CRAN/knitr
	sci-CRAN/fpc
	sci-CRAN/ggplot2
	>=dev-lang/R-3.4
	sci-BIOC/AnnotationDbi
	sci-CRAN/igraph
	sci-BIOC/pathview
	sci-BIOC/org_Hs_eg_db
	sci-CRAN/foreach
	sci-CRAN/doParallel
	sci-CRAN/DBI
	sci-CRAN/rmarkdown
"
RDEPEND="${DEPEND-} virtual/jdk"
