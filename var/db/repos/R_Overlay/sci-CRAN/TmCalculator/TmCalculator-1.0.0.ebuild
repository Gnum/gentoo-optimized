# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Melting Temperature of Nucleic Acid Sequences'
SRC_URI="http://cran.r-project.org/src/contrib/TmCalculator_1.0.0.tar.gz"
LICENSE='GPL-2+'
