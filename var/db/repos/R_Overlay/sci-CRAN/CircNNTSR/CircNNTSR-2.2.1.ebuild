# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Statistical Analysis of Circular... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/CircNNTSR_2.2-1.tar.gz"
LICENSE='GPL-2+'
