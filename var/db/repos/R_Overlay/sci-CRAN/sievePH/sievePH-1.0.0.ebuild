# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Sieve Analysis Methods for Propo... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/sievePH_1.0.0.tar.gz"
LICENSE='GPL-2'

DEPEND="virtual/survival"
RDEPEND="${DEPEND-}"
