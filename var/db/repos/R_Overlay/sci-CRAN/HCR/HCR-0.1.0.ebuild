# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Causal Discovery on Discrete Dat... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/HCR_0.1.0.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=sci-CRAN/data_table-1.10.4"
RDEPEND="${DEPEND-}"
