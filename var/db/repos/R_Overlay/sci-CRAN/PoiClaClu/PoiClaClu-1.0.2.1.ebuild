# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Classification and Clustering of... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/PoiClaClu_1.0.2.1.tar.gz"
LICENSE='GPL-2'
