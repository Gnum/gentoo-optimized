# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Analyzing Archival Tagging Data'
SRC_URI="http://cran.r-project.org/src/contrib/RchivalTag_0.0.7.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/rgeos
	sci-CRAN/plyr
	sci-CRAN/ncdf4
	>=dev-lang/R-3.0.1
	sci-CRAN/maps
	sci-CRAN/maptools
	sci-CRAN/oceanmap
	sci-CRAN/mapdata
	sci-CRAN/sp
	sci-CRAN/raster
	sci-CRAN/PBSmapping
"
RDEPEND="${DEPEND-}"
