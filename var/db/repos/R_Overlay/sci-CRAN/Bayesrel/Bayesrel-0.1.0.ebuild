# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Bayesian Reliability Estimation'
SRC_URI="http://cran.r-project.org/src/contrib/Bayesrel_0.1.0.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/plotrix
	sci-CRAN/ggplot2
	sci-CRAN/coda
	sci-CRAN/LaplacesDemon
	sci-CRAN/ggridges
	sci-CRAN/Rcsdp
	sci-CRAN/Rdpack
	sci-CRAN/lavaan
	virtual/MASS
"
RDEPEND="${DEPEND-}"
