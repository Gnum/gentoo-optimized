# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='A Menu-Driven GUI for Analyzing ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/JFE_2.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/fPortfolio
	dev-lang/R[tk]
	sci-CRAN/fAssets
	sci-CRAN/xts
	virtual/MASS
	sci-CRAN/rugarch
	sci-CRAN/BurStFin
	sci-CRAN/iClick
	dev-lang/R[tk]
	sci-CRAN/PerformanceAnalytics
	sci-CRAN/fBasics
	sci-CRAN/timeSeries
	sci-CRAN/quantmod
	sci-CRAN/timeDate
"
RDEPEND="${DEPEND-}"
