# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Miscellaneous Functions from Alexey Shipunov'
SRC_URI="http://cran.r-project.org/src/contrib/shipunov_1.0.tar.gz"
LICENSE='GPL-2+'

IUSE="${IUSE-} r_suggests_adabag r_suggests_ape r_suggests_class
	r_suggests_cluster r_suggests_e1071 r_suggests_effsize
	r_suggests_gpclib r_suggests_ips r_suggests_mass r_suggests_neuralnet
	r_suggests_nnet r_suggests_pbsmapping r_suggests_randomforest
	r_suggests_rpart r_suggests_scales r_suggests_smirnov r_suggests_tree
	r_suggests_vegan"
R_SUGGESTS="
	r_suggests_adabag? ( sci-CRAN/adabag )
	r_suggests_ape? ( sci-CRAN/ape )
	r_suggests_class? ( virtual/class )
	r_suggests_cluster? ( virtual/cluster )
	r_suggests_e1071? ( sci-CRAN/e1071 )
	r_suggests_effsize? ( sci-CRAN/effsize )
	r_suggests_gpclib? ( sci-CRAN/gpclib )
	r_suggests_ips? ( sci-CRAN/ips )
	r_suggests_mass? ( virtual/MASS )
	r_suggests_neuralnet? ( sci-CRAN/neuralnet )
	r_suggests_nnet? ( virtual/nnet )
	r_suggests_pbsmapping? ( sci-CRAN/PBSmapping )
	r_suggests_randomforest? ( sci-CRAN/randomForest )
	r_suggests_rpart? ( virtual/rpart )
	r_suggests_scales? ( sci-CRAN/scales )
	r_suggests_smirnov? ( sci-CRAN/smirnov )
	r_suggests_tree? ( sci-CRAN/tree )
	r_suggests_vegan? ( sci-CRAN/vegan )
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
