# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Quantitative Exploration of Elas... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/eNetXplorer_1.0.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/gplots
	sci-CRAN/glmnet
	sci-CRAN/progress
	virtual/Matrix
	sci-CRAN/calibrate
	sci-CRAN/RColorBrewer
"
RDEPEND="${DEPEND-}"
