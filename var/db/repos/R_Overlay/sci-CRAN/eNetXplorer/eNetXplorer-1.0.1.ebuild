# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Quantitative Exploration of Elas... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/eNetXplorer_1.0.1.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/progress
	sci-CRAN/calibrate
	virtual/Matrix
	sci-CRAN/RColorBrewer
	sci-CRAN/glmnet
	sci-CRAN/gplots
"
RDEPEND="${DEPEND-}"
