# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Generalized Linear Models with Clustering'
SRC_URI="http://cran.r-project.org/src/contrib/glmmML_1.1.0.tar.gz"
LICENSE='GPL-3+'

DEPEND=">=dev-lang/R-2.13.0"
RDEPEND="${DEPEND-}"
