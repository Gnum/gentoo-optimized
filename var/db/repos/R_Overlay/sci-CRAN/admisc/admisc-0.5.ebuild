# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Adrian Dusas Miscellaneous'
SRC_URI="http://cran.r-project.org/src/contrib/admisc_0.5.tar.gz"
LICENSE='GPL-2+'

IUSE="${IUSE-} r_suggests_qca"
R_SUGGESTS="r_suggests_qca? ( sci-CRAN/QCA )"
DEPEND=">=dev-lang/R-3.5.0"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
