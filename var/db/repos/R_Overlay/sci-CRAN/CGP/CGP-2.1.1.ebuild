# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Composite Gaussian Process Models'
SRC_URI="http://cran.r-project.org/src/contrib/CGP_2.1-1.tar.gz"
LICENSE='LGPL-2.1'
