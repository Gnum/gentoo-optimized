# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Approximate Bayesian Computation... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/abcrf_1.8.1.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/readr
	sci-CRAN/doParallel
	>=sci-CRAN/Rcpp-0.11.2
	sci-CRAN/stringr
	virtual/MASS
	sci-CRAN/ranger
	virtual/Matrix
	sci-CRAN/foreach
	>=dev-lang/R-3.1
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
"
