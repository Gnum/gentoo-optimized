# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Time Uncertain Time Series Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/tuts_0.1.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/foreach
	>=dev-lang/R-3.4.0
	sci-CRAN/rjags
	sci-CRAN/coda
	sci-CRAN/lomb
	sci-CRAN/doParallel
	sci-CRAN/mcmcplots
	sci-CRAN/truncnorm
"
RDEPEND="${DEPEND-}"
