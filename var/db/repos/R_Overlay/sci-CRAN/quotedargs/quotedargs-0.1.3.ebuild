# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='A Way of Writing Functions that ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/quotedargs_0.1.3.tar.gz"
LICENSE='|| ( GPL-2 GPL-3 )'
