# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Sampling from Truncated Multivar... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/tmvmixnorm_1.0.2.tar.gz"
LICENSE='GPL-2'

DEPEND=">=dev-lang/R-3.2.0"
RDEPEND="${DEPEND-}"
