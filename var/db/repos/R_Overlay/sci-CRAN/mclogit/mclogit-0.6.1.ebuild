# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Mixed Conditional Logit Models'
SRC_URI="http://cran.r-project.org/src/contrib/mclogit_0.6.1.tar.gz"
LICENSE='GPL-2'

DEPEND="virtual/Matrix
	sci-CRAN/memisc
"
RDEPEND="${DEPEND-}"
