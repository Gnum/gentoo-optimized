# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Tools to Support Evidence Synthesis'
SRC_URI="http://cran.r-project.org/src/contrib/revtools_0.3.0.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/topicmodels
	>=dev-lang/R-3.5.0
	sci-CRAN/shiny
	sci-CRAN/ade4
	sci-CRAN/plotly
	sci-CRAN/stringdist
	sci-CRAN/viridisLite
	sci-CRAN/SnowballC
	sci-CRAN/shinydashboard
	sci-CRAN/tm
	sci-CRAN/modeltools
"
RDEPEND="${DEPEND-}"
