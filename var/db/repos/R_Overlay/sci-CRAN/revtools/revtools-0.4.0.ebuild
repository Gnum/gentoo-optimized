# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Tools to Support Evidence Synthesis'
SRC_URI="http://cran.r-project.org/src/contrib/revtools_0.4.0.tar.gz"
LICENSE='GPL-3'

DEPEND="sci-CRAN/ade4
	sci-CRAN/shinydashboard
	sci-CRAN/NLP
	sci-CRAN/stringdist
	sci-CRAN/tm
	sci-CRAN/viridisLite
	>=dev-lang/R-3.5.0
	sci-CRAN/modeltools
	sci-CRAN/plotly
	sci-CRAN/topicmodels
	sci-CRAN/shiny
	sci-CRAN/SnowballC
"
RDEPEND="${DEPEND-}"
