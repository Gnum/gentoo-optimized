# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Group Sequential Design Class for Clinical Trials'
SRC_URI="http://cran.r-project.org/src/contrib/seqmon_2.2.tar.gz"
LICENSE='MIT'
