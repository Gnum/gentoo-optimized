# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='The ScottKnott Clustering Algorithm'
SRC_URI="http://cran.r-project.org/src/contrib/ScottKnott_1.2-7.tar.gz"
LICENSE='GPL-2+'
