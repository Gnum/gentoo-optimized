# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Wild Binary Segmentation for Mul... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/wbs_1.4.tar.gz"
LICENSE='GPL-2'
