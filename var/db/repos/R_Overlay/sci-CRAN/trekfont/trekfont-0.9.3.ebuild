# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Star Trek Fonts Collection'
SRC_URI="http://cran.r-project.org/src/contrib/trekfont_0.9.3.tar.gz"
LICENSE='GPL-3'
