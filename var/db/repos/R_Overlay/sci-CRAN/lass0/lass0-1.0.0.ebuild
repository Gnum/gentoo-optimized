# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Lasso-Zero for (High-Dimensional... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/lass0_1.0.0.tar.gz"
LICENSE='GPL-2'

DEPEND=">=sci-CRAN/lpSolve-5.6.13
	>=sci-CRAN/ismev-1.42
	>=sci-CRAN/foreach-1.4.4
"
RDEPEND="${DEPEND-}"
