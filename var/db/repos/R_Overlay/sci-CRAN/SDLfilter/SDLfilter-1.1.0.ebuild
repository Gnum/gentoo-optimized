# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Filtering Satellite-Derived Locations'
SRC_URI="http://cran.r-project.org/src/contrib/SDLfilter_1.1.0.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/ggplot2
	sci-CRAN/raster
	sci-CRAN/trip
	sci-CRAN/geosphere
	sci-CRAN/data_table
	sci-CRAN/sp
	sci-CRAN/ggsn
	sci-CRAN/gridExtra
	sci-CRAN/ggmap
"
RDEPEND="${DEPEND-}"
