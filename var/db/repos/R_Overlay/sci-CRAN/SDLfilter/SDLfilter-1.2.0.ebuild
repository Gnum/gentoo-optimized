# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Filtering Satellite-Derived Locations'
SRC_URI="http://cran.r-project.org/src/contrib/SDLfilter_1.2.0.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/trip
	sci-CRAN/sp
	sci-CRAN/geosphere
	sci-CRAN/ggmap
	sci-CRAN/maps
	sci-CRAN/ggplot2
	sci-CRAN/gridExtra
	sci-CRAN/raster
	sci-CRAN/data_table
	sci-CRAN/ggsn
"
RDEPEND="${DEPEND-}"
