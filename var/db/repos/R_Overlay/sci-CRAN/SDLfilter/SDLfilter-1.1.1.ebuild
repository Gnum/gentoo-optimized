# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Filtering Satellite-Derived Locations'
SRC_URI="http://cran.r-project.org/src/contrib/SDLfilter_1.1.1.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/ggplot2
	sci-CRAN/sp
	sci-CRAN/data_table
	sci-CRAN/ggsn
	sci-CRAN/ggmap
	sci-CRAN/raster
	sci-CRAN/geosphere
	sci-CRAN/trip
	sci-CRAN/gridExtra
"
RDEPEND="${DEPEND-}"
