# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Tools for Analyzing Finite Mixture Models'
SRC_URI="http://cran.r-project.org/src/contrib/mixtools_1.2.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/kernlab
	sci-CRAN/segmented
	>=dev-lang/R-3.5.0
	virtual/survival
	virtual/MASS
"
RDEPEND="${DEPEND-}"
