# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Trust Region Optimization'
SRC_URI="http://cran.r-project.org/src/contrib/trust_0.1-8.tar.gz"
LICENSE='MIT'
