# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='A Semi-Supervised Category Ident... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/SCINA_1.1.0.tar.gz"
LICENSE='GPL-2'

DEPEND=">=dev-lang/R-2.15.0
	virtual/MASS
	sci-CRAN/gplots
"
RDEPEND="${DEPEND-}"
