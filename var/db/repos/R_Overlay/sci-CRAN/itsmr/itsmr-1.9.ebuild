# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Time Series Analysis Using the I... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/itsmr_1.9.tar.gz"
LICENSE='BSD-2'
