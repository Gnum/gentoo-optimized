# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Conditional Sensitivity Analysis... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/dstat_1.0.4.tar.gz"
LICENSE='GPL-2'
