# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Model and Analyse Interval Data'
SRC_URI="http://cran.r-project.org/src/contrib/MAINT.Data_2.0.0.tar.gz"
LICENSE='GPL-2'

DEPEND=">=sci-CRAN/Rcpp-0.11.0
	virtual/MASS
	sci-CRAN/rrcov
	sci-CRAN/robustbase
	sci-CRAN/pcaPP
	sci-CRAN/miscTools
	>=sci-CRAN/sn-1.3.0
	>=dev-lang/R-3.1.0
	sci-CRAN/mclust
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppEigen
"
