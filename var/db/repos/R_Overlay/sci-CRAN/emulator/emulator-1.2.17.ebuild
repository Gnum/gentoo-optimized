# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Bayesian Emulation of Computer Programs'
SRC_URI="http://cran.r-project.org/src/contrib/emulator_1.2-17.tar.gz"
LICENSE='GPL-2+'

DEPEND=">=dev-lang/R-3.0.1
	sci-CRAN/mvtnorm
"
RDEPEND="${DEPEND-}"
