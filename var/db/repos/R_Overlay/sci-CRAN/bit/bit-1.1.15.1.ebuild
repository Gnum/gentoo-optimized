# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='A Class for Vectors of 1-Bit Booleans'
SRC_URI="http://cran.r-project.org/src/contrib/bit_1.1-15.1.tar.gz"
LICENSE='GPL-2'
