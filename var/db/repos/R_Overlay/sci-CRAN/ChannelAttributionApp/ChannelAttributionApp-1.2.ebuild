# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Shiny Web Application for the Mu... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/ChannelAttributionApp_1.2.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/ChannelAttribution
	sci-CRAN/shiny
	sci-CRAN/ggplot2
	sci-CRAN/data_table
"
RDEPEND="${DEPEND-}"
