# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Approximate the Variance of the ... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/UPSvarApprox_0.1.1.tar.gz"
LICENSE='GPL-3'
