# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Interactive Q-Learning'
SRC_URI="http://cran.r-project.org/src/contrib/iqLearn_1.5.tar.gz"
LICENSE='GPL-2'
