# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Spectral Response of Water Wells... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/kitagawa_2.2-2.tar.gz"
LICENSE='GPL-2+'

IUSE="${IUSE-} r_suggests_dplyr r_suggests_knitr r_suggests_psd
	r_suggests_rcolorbrewer r_suggests_rmarkdown r_suggests_sapa
	r_suggests_signal r_suggests_testthat"
R_SUGGESTS="
	r_suggests_dplyr? ( sci-CRAN/dplyr )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_psd? ( sci-CRAN/psd )
	r_suggests_rcolorbrewer? ( sci-CRAN/RColorBrewer )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_sapa? ( sci-CRAN/sapa )
	r_suggests_signal? ( sci-CRAN/signal )
	r_suggests_testthat? ( sci-CRAN/testthat )
"
DEPEND=">=dev-lang/R-2.10.1
	>=sci-CRAN/kelvin-1.2.0
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
