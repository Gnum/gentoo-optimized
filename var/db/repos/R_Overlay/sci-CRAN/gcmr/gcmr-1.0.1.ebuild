# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit R-packages

DESCRIPTION='Gaussian Copula Marginal Regression'
SRC_URI="http://cran.r-project.org/src/contrib/gcmr_1.0.1.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/betareg
	sci-CRAN/sandwich
	virtual/nlme
	>=dev-lang/R-3.0.0
	sci-CRAN/lmtest
	sci-CRAN/sp
	sci-CRAN/geoR
	sci-CRAN/Formula
	sci-CRAN/car
"
RDEPEND="${DEPEND-}"
