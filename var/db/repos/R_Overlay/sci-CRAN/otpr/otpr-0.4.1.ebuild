# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='An R Wrapper for the OpenTripPlanner REST API'
SRC_URI="http://cran.r-project.org/src/contrib/otpr_0.4.1.tar.gz"
LICENSE='MIT'

IUSE="${IUSE-} r_suggests_testthat"
R_SUGGESTS="r_suggests_testthat? ( sci-CRAN/testthat )"
DEPEND="sci-CRAN/sf
	sci-CRAN/checkmate
	sci-CRAN/geojsonsf
	sci-CRAN/jsonlite
	sci-CRAN/httr
	sci-CRAN/janitor
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
