# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Meta-Analysis of Medians'
SRC_URI="http://cran.r-project.org/src/contrib/metamedian_0.1.0.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/metafor
	sci-CRAN/Hmisc
"
RDEPEND="${DEPEND-}"
