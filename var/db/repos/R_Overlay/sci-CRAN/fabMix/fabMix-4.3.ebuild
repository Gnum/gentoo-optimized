# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Overfitting Bayesian Mixtures of... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/fabMix_4.3.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/foreach
	virtual/MASS
	sci-CRAN/doParallel
	>=sci-CRAN/Rcpp-0.12.17
	sci-CRAN/mvtnorm
	sci-CRAN/doRNG
	sci-CRAN/coda
	sci-CRAN/label_switching
	sci-CRAN/mclust
	sci-CRAN/RColorBrewer
	sci-CRAN/corrplot
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
"
