# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Overfitting Bayesian Mixtures of... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/fabMix_4.4.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/doRNG
	>=sci-CRAN/Rcpp-0.12.17
	sci-CRAN/label_switching
	sci-CRAN/coda
	sci-CRAN/mclust
	sci-CRAN/doParallel
	virtual/MASS
	sci-CRAN/foreach
	sci-CRAN/RColorBrewer
	sci-CRAN/mvtnorm
	sci-CRAN/corrplot
	sci-CRAN/ggplot2
"
RDEPEND="${DEPEND-}
	sci-CRAN/Rcpp
	sci-CRAN/RcppArmadillo
"
