# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='PRIM Survival Regression Classification'
SRC_URI="http://cran.r-project.org/src/contrib/PRIMsrc_0.8.0.tar.gz"
LICENSE='GPL-3+'

DEPEND="sci-CRAN/Hmisc
	>=dev-lang/R-3.0.2
	virtual/survival
	sci-CRAN/quantreg
	sci-CRAN/superpc
	sci-CRAN/glmnet
"
RDEPEND="${DEPEND-}"
