# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Classical Word Pools Used in Stu... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/WordPools_1.1-1.tar.gz"
LICENSE='GPL-2'

IUSE="${IUSE-} r_suggests_dplyr"
R_SUGGESTS="r_suggests_dplyr? ( sci-CRAN/dplyr )"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
