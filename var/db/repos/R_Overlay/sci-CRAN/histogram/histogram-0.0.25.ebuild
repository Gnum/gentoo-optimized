# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Construction of Regular and Irre... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/histogram_0.0-25.tar.gz"
LICENSE='GPL-2+'
