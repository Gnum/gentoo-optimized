# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Complex-Valued Wavelet Lifting E... (see metadata)'
SRC_URI="http://cran.r-project.org/src/contrib/CliftLRD_0.1-1.tar.gz"
LICENSE='GPL-2'

IUSE="${IUSE-} r_suggests_fracdiff"
R_SUGGESTS="r_suggests_fracdiff? ( sci-CRAN/fracdiff )"
DEPEND="sci-CRAN/CNLTreg
	sci-CRAN/liftLRD
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
