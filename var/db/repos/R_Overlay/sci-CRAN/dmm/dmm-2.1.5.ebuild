# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Dyadic Mixed Model for Pedigree Data'
SRC_URI="http://cran.r-project.org/src/contrib/dmm_2.1-5.tar.gz"

DEPEND="virtual/Matrix
	sci-CRAN/robustbase
	sci-CRAN/pls
	sci-CRAN/nadiv
	virtual/MASS
"
RDEPEND="${DEPEND-}"
