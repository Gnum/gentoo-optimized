# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='A Generator of Multidimensional Noise'
SRC_URI="http://cran.r-project.org/src/contrib/ambient_0.1.0.tar.gz"
LICENSE='MIT'

DEPEND=">=sci-CRAN/Rcpp-0.12.18"
RDEPEND="${DEPEND-} sci-CRAN/Rcpp"
