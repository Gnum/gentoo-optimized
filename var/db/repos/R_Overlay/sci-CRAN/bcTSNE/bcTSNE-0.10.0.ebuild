# Copyright 1999-2020 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Projected t-SNE for Batch Correction'
SRC_URI="http://cran.r-project.org/src/contrib/bcTSNE_0.10.0.tar.gz"
LICENSE='GPL-3'

IUSE="${IUSE-} r_suggests_batchelor r_suggests_data_table r_suggests_knitr
	r_suggests_scater r_suggests_splatter r_suggests_xtable"
R_SUGGESTS="
	r_suggests_batchelor? ( sci-BIOC/batchelor )
	r_suggests_data_table? ( sci-CRAN/data_table )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_scater? ( sci-BIOC/scater )
	r_suggests_splatter? ( sci-BIOC/splatter )
	r_suggests_xtable? ( sci-CRAN/xtable )
"
DEPEND="sci-CRAN/RSpectra
	sci-CRAN/Rtsne
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"

_UNRESOLVED_PACKAGES=(
	'dlfUtils'
	'harmony'
	'kBET'
	'lisi'
)
