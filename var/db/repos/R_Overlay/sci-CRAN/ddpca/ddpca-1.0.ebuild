# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Diagonally Dominant Principal Component Analysis'
SRC_URI="http://cran.r-project.org/src/contrib/ddpca_1.0.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-CRAN/RSpectra
	virtual/Matrix
	sci-CRAN/quantreg
	virtual/MASS
"
RDEPEND="${DEPEND-}"
