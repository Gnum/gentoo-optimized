# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Multiple Factor Analysis (MFA)'
SRC_URI="http://cran.r-project.org/src/contrib/MFAg_1.5.tar.gz"
LICENSE='GPL-2+'
