# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='An R packaged zlib-1.2.5'
SRC_URI="http://master.bioconductor.org/packages/3.9/bioc/src/contrib/zlibbioc_1.30.0.tar.gz"
LICENSE='Artistic-2'
