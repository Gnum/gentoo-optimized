# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Dual KS Discriminant Analysis and Classification'
SRC_URI="http://master.bioconductor.org/packages/3.9/bioc/src/contrib/dualKS_1.44.0.tar.gz"
LICENSE='LGPL-2+'

DEPEND=">=sci-BIOC/Biobase-1.15.0
	sci-BIOC/affy
"
RDEPEND="${DEPEND-}"
