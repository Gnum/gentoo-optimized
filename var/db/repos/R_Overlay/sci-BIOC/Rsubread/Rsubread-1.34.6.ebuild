# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Subread Sequence Alignment and Counting for R'
SRC_URI="http://master.bioconductor.org/packages/3.9/bioc/src/contrib/Rsubread_1.34.6.tar.gz"
LICENSE='GPL-3'
