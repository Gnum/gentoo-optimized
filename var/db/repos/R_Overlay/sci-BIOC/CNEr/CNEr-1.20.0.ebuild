# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='CNE Detection and Visualization'
SRC_URI="http://master.bioconductor.org/packages/3.9/bioc/src/contrib/CNEr_1.20.0.tar.gz"
LICENSE='GPL-2'

IUSE="${IUSE-} r_suggests_biocstyle r_suggests_bsgenome_drerio_ucsc_danrer10
	r_suggests_bsgenome_ggallus_ucsc_galgal3
	r_suggests_bsgenome_hsapiens_ucsc_hg38 r_suggests_gviz
	r_suggests_knitr r_suggests_rmarkdown r_suggests_testthat
	r_suggests_txdb_drerio_ucsc_danrer10_refgene"
R_SUGGESTS="
	r_suggests_biocstyle? ( sci-BIOC/BiocStyle )
	r_suggests_bsgenome_drerio_ucsc_danrer10? ( sci-BIOC/BSgenome_Drerio_UCSC_danRer10 )
	r_suggests_bsgenome_ggallus_ucsc_galgal3? ( sci-BIOC/BSgenome_Ggallus_UCSC_galGal3 )
	r_suggests_bsgenome_hsapiens_ucsc_hg38? ( sci-BIOC/BSgenome_Hsapiens_UCSC_hg38 )
	r_suggests_gviz? ( >=sci-BIOC/Gviz-1.7.4 )
	r_suggests_knitr? ( sci-CRAN/knitr )
	r_suggests_rmarkdown? ( sci-CRAN/rmarkdown )
	r_suggests_testthat? ( sci-CRAN/testthat )
	r_suggests_txdb_drerio_ucsc_danrer10_refgene? ( sci-BIOC/TxDb_Drerio_UCSC_danRer10_refGene )
"
DEPEND=">=sci-CRAN/RSQLite-0.11.4
	>=sci-BIOC/Biostrings-2.33.4
	>=sci-BIOC/annotate-1.50.0
	>=sci-CRAN/reshape2-1.4.1
	>=sci-BIOC/S4Vectors-0.13.13
	>=sci-CRAN/R_utils-2.3.0
	>=sci-BIOC/GenomeInfoDb-1.1.3
	>=sci-CRAN/readr-0.2.2
	>=sci-BIOC/XVector-0.5.4
	>=sci-CRAN/ggplot2-2.1.0
	sci-BIOC/BiocGenerics
	>=sci-BIOC/IRanges-2.5.27
	>=sci-BIOC/GenomicAlignments-1.1.9
	>=sci-BIOC/rtracklayer-1.25.5
	>=sci-CRAN/DBI-0.7
	>=sci-BIOC/GenomicRanges-1.23.16
	>=sci-CRAN/poweRlaw-0.60.3
	>=dev-lang/R-3.4
	>=sci-BIOC/KEGGREST-1.14.0
	>=sci-BIOC/GO_db-3.3.0
"
RDEPEND="${DEPEND-}
	sci-BIOC/S4Vectors
	sci-BIOC/IRanges
	sci-BIOC/XVector
	${R_SUGGESTS-}
"

_UNRESOLVED_PACKAGES=( 'sci-R/BSgenome' )
