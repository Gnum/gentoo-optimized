# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='GUI for limma Package With Two Color Microarrays'
SRC_URI="http://master.bioconductor.org/packages/3.9/bioc/src/contrib/limmaGUI_1.60.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-CRAN/xtable
	sci-BIOC/limma
	sci-CRAN/tkrplot
	sci-CRAN/R2HTML
	dev-lang/R[tk]
"
RDEPEND="${DEPEND-}"
