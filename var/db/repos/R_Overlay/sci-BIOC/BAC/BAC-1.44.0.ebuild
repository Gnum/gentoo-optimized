# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Bayesian Analysis of Chip-chip experiment'
SRC_URI="http://master.bioconductor.org/packages/3.9/bioc/src/contrib/BAC_1.44.0.tar.gz"
LICENSE='Artistic-2'
