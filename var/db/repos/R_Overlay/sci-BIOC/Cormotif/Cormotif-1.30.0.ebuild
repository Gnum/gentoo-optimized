# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Correlation Motif Fit'
SRC_URI="http://master.bioconductor.org/packages/3.9/bioc/src/contrib/Cormotif_1.30.0.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-BIOC/affy
	>=dev-lang/R-2.12.0
	sci-BIOC/affy
	sci-BIOC/limma
"
RDEPEND="${DEPEND-}"
