# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Removal of unwanted variation fo... (see metadata)'
SRC_URI="http://master.bioconductor.org/packages/3.9/bioc/src/contrib/RUVcorr_1.16.0.tar.gz"
LICENSE='GPL-2'

IUSE="${IUSE-} r_suggests_biocstyle r_suggests_hgu133a2_db r_suggests_knitr"
R_SUGGESTS="
	r_suggests_biocstyle? ( sci-BIOC/BiocStyle )
	r_suggests_hgu133a2_db? ( sci-BIOC/hgu133a2_db )
	r_suggests_knitr? ( sci-CRAN/knitr )
"
DEPEND="sci-CRAN/snowfall
	sci-BIOC/BiocParallel
	sci-BIOC/bladderbatch
	virtual/MASS
	sci-CRAN/gridExtra
	virtual/lattice
	sci-CRAN/reshape2
	sci-CRAN/psych
	sci-CRAN/corrplot
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
