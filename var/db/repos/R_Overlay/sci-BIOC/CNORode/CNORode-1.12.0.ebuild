# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='ODE add-on to CellNOptR'
SRC_URI="http://master.bioconductor.org/packages/3.2/bioc/src/contrib/CNORode_1.12.0.tar.gz"
LICENSE='GPL-2'

DEPEND=">=sci-BIOC/CellNOptR-1.5.14
	sci-CRAN/genalg
"
RDEPEND="${DEPEND-}"
