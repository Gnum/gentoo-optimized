# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Functions for Multinomial Occupancy Distribution'
SRC_URI="http://master.bioconductor.org/packages/3.9/bioc/src/contrib/occugene_1.44.0.tar.gz"
LICENSE='GPL-2+'
