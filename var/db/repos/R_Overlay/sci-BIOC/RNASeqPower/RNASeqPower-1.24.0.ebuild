# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Sample size for RNAseq studies'
SRC_URI="http://master.bioconductor.org/packages/3.9/bioc/src/contrib/RNASeqPower_1.24.0.tar.gz"
LICENSE='LGPL-2+'
