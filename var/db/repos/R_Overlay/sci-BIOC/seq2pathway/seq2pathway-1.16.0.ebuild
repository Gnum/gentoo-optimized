# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='a novel tool for functional gene... (see metadata)'
SRC_URI="http://master.bioconductor.org/packages/3.9/bioc/src/contrib/seq2pathway_1.16.0.tar.gz"
LICENSE='GPL-2'

DEPEND="sci-BIOC/seq2pathway_data
	sci-CRAN/WGCNA
	virtual/nnet
	sci-CRAN/GSA
	sci-BIOC/GenomicRanges
	sci-BIOC/biomaRt
"
RDEPEND="${DEPEND-}"
