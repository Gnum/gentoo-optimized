# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='A collection of pre-processing functions'
SRC_URI="http://master.bioconductor.org/packages/3.9/bioc/src/contrib/preprocessCore_1.46.0.tar.gz"
LICENSE='LGPL-2+'
