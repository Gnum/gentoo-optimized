# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='FlowRepository R Interface'
SRC_URI="http://master.bioconductor.org/packages/3.9/bioc/src/contrib/FlowRepositoryR_1.16.0.tar.gz"
LICENSE='Artistic-2'

IUSE="${IUSE-} r_suggests_biocgenerics r_suggests_flowcore r_suggests_runit"
R_SUGGESTS="
	r_suggests_biocgenerics? ( sci-BIOC/BiocGenerics )
	r_suggests_flowcore? ( sci-BIOC/flowCore )
	r_suggests_runit? ( sci-CRAN/RUnit )
"
DEPEND="sci-omegahat/XML
	sci-CRAN/jsonlite
	>=dev-lang/R-3.2
	sci-omegahat/RCurl
"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
