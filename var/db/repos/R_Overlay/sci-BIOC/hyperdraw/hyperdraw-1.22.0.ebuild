# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Visualizing Hypergaphs'
SRC_URI="http://master.bioconductor.org/packages/3.2/bioc/src/contrib/hyperdraw_1.22.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-BIOC/hypergraph
	sci-BIOC/graph
	sci-BIOC/Rgraphviz
"
RDEPEND="${DEPEND-} media-gfx/graphviz"
