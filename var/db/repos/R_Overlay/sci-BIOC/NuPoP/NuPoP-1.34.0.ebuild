# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='An R package for nucleosome positioning prediction'
SRC_URI="http://master.bioconductor.org/packages/3.9/bioc/src/contrib/NuPoP_1.34.0.tar.gz"
LICENSE='GPL-2'
