# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Bayesian Hierarchical Clustering'
SRC_URI="http://master.bioconductor.org/packages/3.9/bioc/src/contrib/BHC_1.36.0.tar.gz"
LICENSE='GPL-3'
