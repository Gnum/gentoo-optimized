# Copyright 1999-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages

DESCRIPTION='Agreement of Differential Expression Analysis'
SRC_URI="http://master.bioconductor.org/packages/3.9/bioc/src/contrib/AGDEX_1.32.0.tar.gz"
LICENSE='GPL-2+'

DEPEND="sci-BIOC/Biobase
	sci-BIOC/GSEABase
"
RDEPEND="${DEPEND-}"
