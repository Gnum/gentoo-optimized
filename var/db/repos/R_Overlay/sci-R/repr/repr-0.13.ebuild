# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit R-packages
DESCRIPTION='String and byte representations for all kinds of R objects'
HOMEPAGE='http://irkernel.github.io'
SRC_URI="https://github.com/IRkernel/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"
LICENSE='GPL-2+'

R_SUGGESTS=""
DEPEND="sci-CRAN/htmltools
	sci-CRAN/base64enc"
RDEPEND="${DEPEND-} ${R_SUGGESTS-}"
