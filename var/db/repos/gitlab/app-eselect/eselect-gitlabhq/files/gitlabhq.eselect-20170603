# -*-eselect-*-  vim: ft=eselect
# Copyright 2017 Gentoo Foundation
# Distributed under the terms of the GNU GPL version 2 or later

DESCRIPTION="Manage the gitlabhq slot symlink(s)"
MAINTAINER="avschie@on.nl"
VERSION="@VERSION@"

# find a list of gitlabhq installations
find_targets() {
    local f
    for f in "${EROOT}"/opt/gitlabhq-[[:digit:]]*; do
        # ignore 'unmerged' installs
        [[ -f ${f}/VERSION ]] && basename "${f}"
    done
}

# remove the gitlabhq symlink
remove_symlink() {
    rm "${EROOT}/opt/gitlabhq" || die "failed to remove /opt/gitlabhq symlink"
    # openrc:
    rm -f "${EROOT}/etc/init.d/gitlabhq"
}

# set the gitlabhq symlinks
set_symlink() {
    local target=$1

    if is_number "${target}"; then
        local targets=( $(find_targets) )
        target=${targets[target-1]}
    fi

    [[ -z ${target} || ! -f ${EROOT}/opt/${target}/VERSION ]] \
        && die -q "Target \"$1\" doesn't appear to be valid!"

    local version=$(cat ${EROOT}/opt/${target}/VERSION)

    # Don't select a version that is not configured
    [[ -d "${EROOT}/opt/${target}/public/assets" ]] \
        || die -q "${target} is not configured, run:\n"\
                  "  emerge --config =www-apps/gitlabhq-${version}"

    # check if any config files are not merged
    local unmerged=$(find ${EROOT}/opt/${target}/config -name '._cfg*')
    if [ -n "$unmerged" ]; then
        die -q "${target} has still unmerged config files, run:\n" \
               "  CONFIG_PROTECT=${EROOT}/opt/${target}/config dispatch-conf"
    fi

    ln -s "${target}" "${EROOT}/opt/gitlabhq" || die -q "Failed to create /opt/gitlabhq symlink!"

    # Optional openrc symlink:
    if [ -x "${EROOT}/etc/init.d/${target}" ]; then
        ln -s "${target}" "${EROOT}/etc/init.d/gitlabhq"
    fi
}

### show action ###

describe_show() {
    echo "Show the current gitlabhq symlink"
}

do_show() {
    write_list_start "Current gitlabhq symlink:"
    if [[ -L ${EROOT}/opt/gitlabhq ]]; then
        local gitlabhq=$(canonicalise "${EROOT}/opt/gitlabhq")
        write_kv_list_entry "${gitlabhq%/}" ""
    else
        write_kv_list_entry "(unset)" ""
    fi
}

### list action ###

describe_list() {
    echo "List available gitlabhq symlink targets"
}

do_list() {
    local i targets=( $(find_targets) )

    write_list_start "Available gitlabhq symlink targets:"
    for (( i = 0; i < ${#targets[@]}; i++ )); do
        # highlight the target where the symlink is pointing to
        [[ ${targets[i]} = \
            $(basename "$(canonicalise "${EROOT}/opt/gitlabhq")") ]] \
            && targets[i]=$(highlight_marker "${targets[i]}")
    done
    write_numbered_list -m "(none found)" "${targets[@]}"
}

### set action ###

describe_set() {
    echo "Set a new gitlabhq symlink target"
}

describe_set_parameters() {
    echo "<target>"
}

describe_set_options() {
    echo "target : Target name or number (from 'list' action)"
}

do_set() {
    [[ -z $1 ]] && die -q "You didn't tell me what to set the symlink to"
    [[ $# -gt 1 ]] && die -q "Too many parameters"

    if [[ -L ${EROOT}/opt/gitlabhq ]]; then
        # existing symlink
        remove_symlink || die -q "Couldn't remove existing symlink"
        set_symlink "$1" || die -q "Couldn't set a new symlink"
    elif [[ -e ${EROOT}/opt/gitlabhq ]]; then
        # we have something strange
        die -q "${EROOT}/opt/gitlabhq exists but is not a symlink"
    else
        set_symlink "$1" || die -q "Couldn't set a new symlink"
    fi
}

### unset action ###

describe_usset() {
    echo "Unset the gitlabhq symlink target"
}

do_unset() {
    if [[ -L ${EROOT}/opt/gitlabhq ]]; then
        remove_symlink
    elif [[ -e ${EROOT}/opt/gitlabhq ]]; then
        die -q "${EROOT}/opt/gitlabhq exists but is not a symlink"
    else
        echo "No active gitlabhq exists"
    fi
}
