
;;; emacs-libvterm site-lisp configuration

(add-to-list 'load-path "@SITELISP@")
(autoload 'vterm "vterm" "Create a new vterm" t)
(autoload 'vterm-other-window "vterm" "Create a new vterm" t)

