// This file was generated by ecm_setup_version(): DO NOT EDIT!

#ifndef KICONTHEMES_VERSION_H
#define KICONTHEMES_VERSION_H

#define KICONTHEMES_VERSION_STRING "5.70.0"
#define KICONTHEMES_VERSION_MAJOR 5
#define KICONTHEMES_VERSION_MINOR 70
#define KICONTHEMES_VERSION_PATCH 0
#define KICONTHEMES_VERSION ((5<<16)|(70<<8)|(0))

#endif
