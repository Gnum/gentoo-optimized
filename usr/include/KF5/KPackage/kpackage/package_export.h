
#ifndef KPACKAGE_EXPORT_H
#define KPACKAGE_EXPORT_H

#ifdef KPACKAGE_STATIC_DEFINE
#  define KPACKAGE_EXPORT
#  define KPACKAGE_NO_EXPORT
#else
#  ifndef KPACKAGE_EXPORT
#    ifdef KF5Package_EXPORTS
        /* We are building this library */
#      define KPACKAGE_EXPORT __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define KPACKAGE_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KPACKAGE_NO_EXPORT
#    define KPACKAGE_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KPACKAGE_DECL_DEPRECATED
#  define KPACKAGE_DECL_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KPACKAGE_DECL_DEPRECATED_EXPORT
#  define KPACKAGE_DECL_DEPRECATED_EXPORT KPACKAGE_EXPORT KPACKAGE_DECL_DEPRECATED
#endif

#ifndef KPACKAGE_DECL_DEPRECATED_NO_EXPORT
#  define KPACKAGE_DECL_DEPRECATED_NO_EXPORT KPACKAGE_NO_EXPORT KPACKAGE_DECL_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KPACKAGE_NO_DEPRECATED
#    define KPACKAGE_NO_DEPRECATED
#  endif
#endif

#define KPACKAGE_DECL_DEPRECATED_TEXT(text) __attribute__ ((__deprecated__(text)))

#define ECM_GENERATEEXPORTHEADER_VERSION_VALUE(major, minor, patch) ((major<<16)|(minor<<8)|(patch))

/* Take any defaults from group settings */
#if !defined(KPACKAGE_NO_DEPRECATED) && !defined(KPACKAGE_DISABLE_DEPRECATED_BEFORE_AND_AT)
#  ifdef KF_NO_DEPRECATED
#    define KPACKAGE_NO_DEPRECATED
#  elif defined(KF_DISABLE_DEPRECATED_BEFORE_AND_AT)
#    define KPACKAGE_DISABLE_DEPRECATED_BEFORE_AND_AT KF_DISABLE_DEPRECATED_BEFORE_AND_AT
#  endif
#endif
#if !defined(KPACKAGE_DISABLE_DEPRECATED_BEFORE_AND_AT) && defined(KF_DISABLE_DEPRECATED_BEFORE_AND_AT)
#  define KPACKAGE_DISABLE_DEPRECATED_BEFORE_AND_AT KF_DISABLE_DEPRECATED_BEFORE_AND_AT
#endif

#if !defined(KPACKAGE_NO_DEPRECATED_WARNINGS) && !defined(KPACKAGE_DEPRECATED_WARNINGS_SINCE)
#  ifdef KF_NO_DEPRECATED_WARNINGS
#    define KPACKAGE_NO_DEPRECATED_WARNINGS
#  elif defined(KF_DEPRECATED_WARNINGS_SINCE)
#    define KPACKAGE_DEPRECATED_WARNINGS_SINCE KF_DEPRECATED_WARNINGS_SINCE
#  endif
#endif
#if !defined(KPACKAGE_DEPRECATED_WARNINGS_SINCE) && defined(KF_DEPRECATED_WARNINGS_SINCE)
#  define KPACKAGE_DEPRECATED_WARNINGS_SINCE KF_DEPRECATED_WARNINGS_SINCE
#endif

#if defined(KPACKAGE_NO_DEPRECATED)
#  undef KPACKAGE_DEPRECATED
#  define KPACKAGE_DEPRECATED_EXPORT KPACKAGE_EXPORT
#  define KPACKAGE_DEPRECATED_NO_EXPORT KPACKAGE_NO_EXPORT
#elif defined(KPACKAGE_NO_DEPRECATED_WARNINGS)
#  define KPACKAGE_DEPRECATED
#  define KPACKAGE_DEPRECATED_EXPORT KPACKAGE_EXPORT
#  define KPACKAGE_DEPRECATED_NO_EXPORT KPACKAGE_NO_EXPORT
#else
#  define KPACKAGE_DEPRECATED KPACKAGE_DECL_DEPRECATED
#  define KPACKAGE_DEPRECATED_EXPORT KPACKAGE_DECL_DEPRECATED_EXPORT
#  define KPACKAGE_DEPRECATED_NO_EXPORT KPACKAGE_DECL_DEPRECATED_NO_EXPORT
#endif

/* No deprecated API had been removed from build */
#define KPACKAGE_EXCLUDE_DEPRECATED_BEFORE_AND_AT 0

#define KPACKAGE_BUILD_DEPRECATED_SINCE(major, minor) 1

#ifdef KPACKAGE_NO_DEPRECATED
#  define KPACKAGE_DISABLE_DEPRECATED_BEFORE_AND_AT 0x54600
#endif
#ifdef KPACKAGE_NO_DEPRECATED_WARNINGS
#  define KPACKAGE_DEPRECATED_WARNINGS_SINCE 0
#endif

#ifndef KPACKAGE_DEPRECATED_WARNINGS_SINCE
#  ifdef KPACKAGE_DISABLE_DEPRECATED_BEFORE_AND_AT
#    define KPACKAGE_DEPRECATED_WARNINGS_SINCE KPACKAGE_DISABLE_DEPRECATED_BEFORE_AND_AT
#  else
#    define KPACKAGE_DEPRECATED_WARNINGS_SINCE 0x54600
#  endif
#endif

#ifndef KPACKAGE_DISABLE_DEPRECATED_BEFORE_AND_AT
#  define KPACKAGE_DISABLE_DEPRECATED_BEFORE_AND_AT 0
#endif

#ifdef KPACKAGE_DEPRECATED
#  define KPACKAGE_ENABLE_DEPRECATED_SINCE(major, minor) (ECM_GENERATEEXPORTHEADER_VERSION_VALUE(major, minor, 0) > KPACKAGE_DISABLE_DEPRECATED_BEFORE_AND_AT)
#else
#  define KPACKAGE_ENABLE_DEPRECATED_SINCE(major, minor) 0
#endif

#if KPACKAGE_DEPRECATED_WARNINGS_SINCE >= 0x51500
#  define KPACKAGE_DEPRECATED_VERSION_5_21(text) KPACKAGE_DECL_DEPRECATED_TEXT(text)
#else
#  define KPACKAGE_DEPRECATED_VERSION_5_21(text)
#endif
#define KPACKAGE_DEPRECATED_VERSION_5(minor, text)      KPACKAGE_DEPRECATED_VERSION_5_##minor(text)
#define KPACKAGE_DEPRECATED_VERSION(major, minor, text) KPACKAGE_DEPRECATED_VERSION_##major(minor, "Since "#major"."#minor". " text)

#endif /* KPACKAGE_EXPORT_H */
