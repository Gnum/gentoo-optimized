#define MODULE_DIR "/usr/lib64/zsh/5.8"
#define SITESCRIPT_DIR "/usr/share/zsh/scripts"
#define SCRIPT_DIR "/usr/share/zsh/5.8/scripts"
#define SITEFPATH_DIR "/usr/share/zsh/site-functions"
#define FIXED_FPATH_DIR "/usr/local/share/zsh/site-functions"
#define FPATH_DIR "/usr/share/zsh/5.8/functions"
#define FPATH_SUBDIRS { "Calendar", "Chpwd", "Completion", "Completion/AIX", "Completion/BSD", "Completion/Base", "Completion/Cygwin", "Completion/Darwin", "Completion/Debian", "Completion/Linux", "Completion/Mandriva", "Completion/Redhat", "Completion/Solaris", "Completion/Unix", "Completion/X", "Completion/Zsh", "Completion/openSUSE", "Exceptions", "MIME", "Math", "Misc", "Newuser", "Prompts", "TCP", "VCS_Info", "VCS_Info/Backends", "Zftp", "Zle" }
