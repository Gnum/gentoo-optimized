#ifndef _ISL_INCLUDE_ISL_STDINT_H
#define _ISL_INCLUDE_ISL_STDINT_H 1
#ifndef _GENERATED_STDINT_H
#define _GENERATED_STDINT_H "isl 0.22.1"
/* generated using gnu compiler x86_64-pc-linux-gnu-gcc () 11.0.0 20200531 (experimental) */
#define _STDINT_HAVE_STDINT_H 1
#include <stdint.h>
#endif
#endif
